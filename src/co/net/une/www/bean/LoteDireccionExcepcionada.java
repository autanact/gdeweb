package co.net.une.www.bean;

/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: LoteDireccionExcepcionada.java
##	Contenido: Clase que implementa el objeto Lote direcci�n excepcionada, relacionado con la tabla ds_cd_lote_direxc_lt
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		04/01/2016 FMS Creaci�n de la clase
## 		11/02/2016 FMS Creaci�n de versi�n 1.0. Publicada en ambientes DESA/PRE/PROD  
## 		13/02/2016 FMS Actualizaci�n versi�n 2.0  incluye estado E como estado v�lido 
##**********************************************************************************************************************************
*/
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import co.net.une.www.util.GDEException;
import co.net.une.www.util.UtilGDE;

/**
 * Clase que implementa el objeto Lote direcci�n excepcionada, relacionado con la tabla ds_cd_lote_direxc_lt
 * @author Freddy Molina
 * @creado 12-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
* 		04/01/2016 FMS Creaci�n de la clase
 * 		11/02/2016 FMS Creaci�n de versi�n 1.0. Publicada en ambientes DESA/PRE/PROD  
 * 		13/02/2016 FMS Actualizaci�n versi�n 2.0  incluye estado E como estado v�lido 
 *
 */
public class LoteDireccionExcepcionada extends Servicios {
	
	private int id= 0;
	private String nombre_archivo = null;
	private String fecha_publicacion = null;
	private String fecha_recepcion = null;
	private String usuario = null;
	private String estado_publicacion_type = null;
	private String directorioRaiz =null;
	
	//archivo de configuraci�n
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.LoteDireccionExcepcionada", Locale.getDefault());
	
	//configuraciones
	private static final String DATASET = rb.getString("LoteDireccionExcepcionada.dataset"); 
	public static final String TABLE =  rb.getString("LoteDireccionExcepcionada.tabla");
	private static final String ALLFIELDS =  rb.getString("LoteDireccionExcepcionada.campos");
	//errores
	private static final String messageGSSException = rb.getString("error.messageGSSException");
	private static final String messageFileException = rb.getString("error.creandoArchivo");
	
	//mensajes
	private static final String sinTamanioLote = rb.getString("mensaje.publicar.sinRegistros");
	private static final String sinRegistros = rb.getString("mensaje.publicar.sinRegistros");

	
	
	public LoteDireccionExcepcionada() {
		super();
	}
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNombreArchivo() {
		return nombre_archivo;
	}



	public void setNombreArchivo(String nombre_archivo) {
		this.nombre_archivo = nombre_archivo;
	}



	public String getFechaPublicacion() {
		return fecha_publicacion;
	}



	public void setFechaPublicacion(String fecha_publicacion) {
		this.fecha_publicacion = fecha_publicacion;
	}



	public String getFechaRecepcion() {
		return fecha_recepcion;
	}



	public void setFechaRecepcion(String fecha_recepcion) {
		this.fecha_recepcion = fecha_recepcion;
	}



	public String getUsuario() {
		return usuario;
	}



	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}



	public String getEstadoPublicacionType() {
		return estado_publicacion_type;
	}



	public void setEstadoPublicacionType(String estado_publicacion_type) {
		this.estado_publicacion_type = estado_publicacion_type;
	}

	public String getDirectorioRaiz() {
		return directorioRaiz;
	}

	public void setDirectorioRaiz(String directorioRaiz) {
		this.directorioRaiz = directorioRaiz;
	}

	/**
	 * M�todo que crea un lote en BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void crearLote() throws GDEException{
		String fechaArchivo = null;
		try {
			
			// Se inserta el objeto en la tabla
			String campos = "nombre_archivo|estado_publicacion_type|fecha_publicacion|usuario";
			Date now = new Date();
			fechaArchivo =  new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss").format(now);
			String fechaPublicacion = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(now);
			String valores = "Default|PENDIENTE|"+fechaPublicacion+"|"+getUsuario();
			
			String id = ejecutarServicioInsertarElementoTabla(campos, valores);
			
			setId(Integer.parseInt(id));
			
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
		try {
			
			// Se actualiza con el nombre del archivo
			ejecutarServicioActualizarElementoTabla("id", getId()+"", "nombre_archivo", getId()+"_"+fechaArchivo+".biz");  
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
		try {
			
			// Se carga el objeto con los datos nuevos
			ejecutarServicioConsultaTabla(ALLFIELDS, "id", getId()+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
		
	}
	
	/**
	 * M�todo que crea el archivo de lotes a partir de un arreglo de registros
	 * @param archivoLote
	 * 			Listado de registros a ingresar
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void generarArchivo(List<String[]> archivoLote) throws GDEException {
		
		PrintWriter writer = null;
        try {
            //create a temporary file
        	
        	writer = new PrintWriter(getDirectorioRaiz()+getNombreArchivo(), "UTF-8");

            Iterator<String[]> loteIterator = archivoLote.iterator();
            writer.println("ID_LOTE: "+getId());
            writer.println("USUARIO: "+getUsuario());
            writer.println("FECHA_HORA_PUBLICACION: "+getFechaPublicacion());
            writer.println(" ");
    		while(loteIterator.hasNext()){
    			String[] regLote = loteIterator.next();
    			String linea = regLote[0]+"|"+regLote[1]+"|"+regLote[2]+"|"+regLote[3]+"|"+regLote[4]+"|"+regLote[5]+"|"+regLote[6]+"|"+regLote[7]+"|"+regLote[8];
    			writer.println(linea);
    		}
            
        } catch (Exception e) {
        	GDEException gdeException = new GDEException(MessageFormat.format(messageFileException,e.getMessage(),getDirectorioRaiz()+getNombreArchivo()));
			throw gdeException;
        } finally {
            try {
                writer.close();
            } catch (Exception e) { }
        }
		
	}

	@Override
	protected String obtenerDateSet() {
		return DATASET;
	}

	@Override
	protected String obtenerTabla() {
		return TABLE;
	}

	@Override
	protected void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registros.indexOf(";;")>=0){
					registros = registros.replaceAll(";;", ";##;");
				}
				if (registros.endsWith(";")) registros = registros+"##";
				
				String[] campos = registro[i].split(";");
				setId("##".equals(campos[0])?0:Integer.parseInt(campos[0]));
				setNombreArchivo("##".equals(campos[1])?null:campos[1]);
				setFechaPublicacion("##".equals(campos[2])?null:campos[2]);
				setFechaRecepcion("##".equals(campos[3])?null:campos[3]);
				setUsuario("##".equals(campos[4])?null:campos[4]);
				setEstadoPublicacionType("##".equals(campos[5])?null:campos[5]);
			}
		}
		
	}

	/**
	 * M�todo que obtiene un lote
	 * @param loteId
	 * 			Id del lote
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerLotePorIdLote(String loteId) throws GDEException {
		try {
			ejecutarServicioConsultaTabla(ALLFIELDS, "id", loteId);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

	@Override
	protected void obtenerObjeto(Object registros) throws GDEException {
		
	}
	
	/**
	 * M�todo que inserta un lote en la BD
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void insertarLote() throws GDEException {
		try {
			String nombreCampos = "";
			String valoresCampos = "";
			if (getNombreArchivo()!=null){
				nombreCampos+="nombre_archivo|";
				valoresCampos+=getNombreArchivo()+"|";
			}
			if (getEstadoPublicacionType()!=null){
				nombreCampos+="estado_publicacion_type|";
				valoresCampos+=getEstadoPublicacionType()+"|";
			}
			if (getFechaPublicacion()!=null){
				nombreCampos+="fecha_publicacion|";
				valoresCampos+=getFechaPublicacion()+"|";
			}
			if (getUsuario()!=null){
				nombreCampos+="usuario|";
				valoresCampos+=getUsuario()+"|";
			}
			nombreCampos=nombreCampos.endsWith("|")?nombreCampos.substring(0, nombreCampos.length() - 1) :nombreCampos;
			valoresCampos=valoresCampos.endsWith("|")?valoresCampos.substring(0, valoresCampos.length() - 1) :valoresCampos;
			
			setId(Integer.parseInt(ejecutarServicioInsertarElementoTabla(nombreCampos, valoresCampos)));
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
		try {
			ejecutarServicioActualizarElementoTabla("id", getId()+"", "nombre_archivo", getId()+"_"+getNombreArchivo());  
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}
		
		try {
			ejecutarServicioConsultaTabla(ALLFIELDS, "id", getId()+"");
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}


	/**
	 * M�todo que publica el lote en el archivo correspondiente
	 * @param usuario
	 * 			usuario que ejecuta la acci�n
	 * @param path
	 * 			ruta donde se va a guardar el lote
	 * @return
	 * 			Si el proceso se ejecuta sin problemas, no retorna nada. En caso de alguna falla lanza una excepci�n
	 * @throws GDEException
	 * 			Excepci�n con el error presentado
	 * 
	 */
	public static String [] publicarLote(String usuario, String path) throws GDEException {
		
		String[] respuesta = new String[5];
		respuesta[0]="K0";
		respuesta[1]="";
		respuesta[2]="";
		respuesta[3]="";
		respuesta[4]="";
		
		//obtengo la hora actual
		Date fecha_hora = new Date();
		
		//defino los formatos a utilizar de fecha a utilizar
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		SimpleDateFormat format_fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		
		//defino los archivos para escritura
		File archivo = null;
		BufferedWriter salArch = null;
		
		int contador_lineas = 0;
		int contador_registros = 0;
		int archivo_numero = 0;
		
		//obtengo la periocidad del lote
		PeridiocidadDirecciones periodicidad = new PeridiocidadDirecciones();
		periodicidad.obtenerPeriodicidadDirecciones();

		if (periodicidad.getTamanoLote() == 0) {
			GDEException e1 = new GDEException();
			e1.setMensage(sinTamanioLote);
			throw e1;

		}
			
		LoteDireccionExcepcionada loteDirExc = new LoteDireccionExcepcionada();
		String codSol = "";
		String mensajes ="";
		try {
			//Obtengo el listado de Ids de dirExc que cumplan el criterio
			DireccionExcepcionadaList dirExcList = new DireccionExcepcionadaList();
			dirExcList.obtenerListaIdDirExcSegunCriterio("estado_excepcion_type|estado_gestion_type|autorizada_gestion|ds_cd_lote_direxc_lt", "REPORTADA|PENDIENTE|true|_unset");
			
			//verifico que tenga direcciones excepcionadas que cumplan el criterio 
			if (!dirExcList.getDirExcList().isEmpty()){
				
				//existen direcciones a publicar
				Iterator<DireccionExcepcionada> dirExcIterator = dirExcList.getDirExcList().iterator();
				
				// Generar el nombre del archivo
				String nombreArchivo = format.format(fecha_hora) + ".biz"; // Aqui se le asigna el nombre al archivo
	
				// INSERTAR EN EL ARCHIVO POR LOTE registro en la tabla ds_cd_lote_direxc_lt
				loteDirExc.setNombreArchivo(nombreArchivo);
				loteDirExc.setEstadoPublicacionType("PENDIENTE");
				loteDirExc.setFechaPublicacion(format_fecha.format(fecha_hora));
				loteDirExc.setUsuario(usuario);
				loteDirExc.insertarLote();
				
				if (!path.endsWith("/") ) {
					if (!path.endsWith("\\"))
						path = path+"/";
				}
				loteDirExc.setDirectorioRaiz(path);
	
				System.out.println("El nombre completo del archivo es: " + loteDirExc.getDirectorioRaiz()+loteDirExc.getNombreArchivo());
				System.out.println("********** Vamos a trabajar con "+ dirExcList.totalDirecciones() + " registros. **********");
				while(dirExcIterator.hasNext()){ 
					
					try {
						//obtengo la direcci�n excepcionada
						DireccionExcepcionada dirExc = dirExcIterator.next();
						
						dirExc.obtenerDirExcPorId(dirExc.getId()+"");
						codSol= dirExc.getCodigoSolicitud();
						
						//Verifico si no hay repetidos por c�digo de solicitud
						DireccionExcepcionadaList dirExcListRep = new DireccionExcepcionadaList();
						dirExcListRep.obtenerListaIdDirExcSegunCriterio("codigo_solicitud", codSol);
						

						if (dirExcListRep.totalDirecciones()== 1) {
						
							//obtengo la direcci�n georrefenciada
							Direccion direccion = new Direccion();
							direccion.obtenerDireccionGeoreferenciada(dirExc.getPais(), dirExc.getDepartamento(), dirExc.getMunicipio(), dirExc.getDireccionTextoLibre());
							
							//verifico si el geor_type  la direcci�n georreferenciada es M o N
							if ( "MN".indexOf(direccion.getEstadoGeorType()) >=0) {
								
								//la direcci�n  georrefenciada es M o N
								//verifico si el geor_type  la direcci�n excepcionada NO es M o N
								if ( "MN".indexOf(dirExc.getEstadoGeorType()) < 0) {
									
									//la direcci�n excepcionada NO es M o N
									
									//Asigno los nuevos estatus Y o B de Geo
									dirExc.setEstadoGeorType(direccion.getEstadoGeorType());
									
									//ejecuto RespuestaDE
									invocarRespuestaDE(dirExc,direccion);
									
									dirExc.setEstadoGeorType(direccion.getEstadoGeorType());
									dirExc.setEstadoExcepcionType("EXACTA");
									
									//actualizo los datos en BD de la direcci�n excepcionada
									dirExc.actualizarDirExcVersionada("codigo_direccion|direccion_normalizada|estado_excepcion_type|estado_geor_type|estado_gestion_type|fecha_envio_proveedor|fecha_respuesta_proveedor|comentario_proveedor",
										direccion.getCodigoDireccion() + "|"+ direccion.getDireccionNormalizada()+ "|"+dirExc.getEstadoExcepcionType()+"|"+ direccion.getEstadoGeorType()+ "|"+ dirExc.getEstadoGestionType()+ "| | |Direcci�n resuelta por georreferenciaci�n",
										"");
									
									//Relaciono la direcci�n Excepcionada con la direccci�n
									dirExc.relacionarDireccionGeo();
								}
							}else {
								
								// Mejoro si es YBE
								if ("YBEC,F,G,J".indexOf(direccion.getEstadoGeorType()) >=0 ){
								
									if ( "YBE".indexOf(direccion.getEstadoGeorType()) >= 0) {
										
										String estOrg = dirExc.getEstadoGeorType();
										
										//Asigno los nuevos estatus diferentes a Y o B de Geo
										dirExc.setEstadoGeorType(direccion.getEstadoGeorType());
										
										//Si el estatus es Y o B env�o a CRM
										if (!estOrg.equals(direccion.getEstadoGeorType())) {
											invocarRespuestaDE(dirExc,direccion);
										}
										
										//Coloco en estatus pendiente
										dirExc.setEstadoGestionType("PENDIENTE");
										dirExc.setEstadoExcepcionType("REPORTADA");
										
									}else {
										
										dirExc.setEstadoExcepcionType("REPORTADA");
									}
									
									
									//Si el c�digo de direcci�n es vac�o en la direcci�n GEO toma la de la dirExec
									String codDireccion = ("C,F,G,J".indexOf(direccion.getEstadoGeorType())>=0?dirExc.getCodigoDireccion():direccion.getCodigoDireccion());
								
									//actualizo los datos en BD de la direcci�n excepcionada
									dirExc.actualizarDirExcVersionada("codigo_direccion|direccion_normalizada|estado_excepcion_type|estado_geor_type|estado_gestion_type|fecha_envio_proveedor|fecha_respuesta_proveedor|comentario_proveedor",
											codDireccion+ "|"+ direccion.getDireccionNormalizada()+ "|"+dirExc.getEstadoExcepcionType()+"|"+ dirExc.getEstadoGeorType()+ "|"+ dirExc.getEstadoGestionType()+ "| | | ",
											loteDirExc.getNombreArchivo());
		
									//Relaciono la direcci�n Excepcionada con la direccci�n
									dirExc.relacionarDireccionGeo(codDireccion);
									
									//Relaciono el lote
									dirExc.relacionarLote(loteDirExc.getId());
									
									// Grabar en el archivo
									if (contador_lineas == 0 || contador_registros==0 ) {
										// Incrementar el numero de Archivos
										archivo_numero++;
										
										// Agregar el numero de archivo al Path path_final = path.replace(".biz","_"+archivo_numero+".biz");
										String path_final = path+loteDirExc.getNombreArchivo();
										//System.out.println("********** Se relaciono el objeto de forma OK e ira al archivo: "+ path_final+ " **********");
										// Preparar archivo para escritura
										archivo = new File(path_final);
			
										
										salArch = new BufferedWriter(new FileWriter(archivo, true));
										
			
										// Encabezado
										salArch.write("ID_LOTE: " + loteDirExc.getId()+"");
										salArch.write("\r\n");
										salArch.write("USUARIO: " + usuario);
										salArch.write("\r\n");
										salArch.write("FECHA_HORA_PUBLICACION: "
												+ loteDirExc.getFechaPublicacion());
									}
									salArch.write("\r\n");
									salArch.write(dirExc.getCodigoSolicitud()+ "[|]1[|]"+ dirExc.getDireccionTextoLibre()+ "[|]" + dirExc.getPais() + "[|]"
											+ dirExc.getDepartamento() + "[|]"+ dirExc.getMunicipio() + "[|]"
											+ dirExc.getTelefonos() + "[|]"	+ dirExc.getContacto() + "[|]"
											+ dirExc.getEstadoGeorType() + "[|]"+ UtilGDE.colocarMascara(dirExc.getComentario()));
			
									contador_lineas++;
									contador_registros++;
			
									if (contador_lineas == dirExcList.totalDirecciones()) {
										contador_lineas = 0;
										// Cerrar e archivo
										salArch.close();
										salArch=null;
									}
								}
								
							}
						}else{
							System.out.println("Fall� Codigo Solucitud "+codSol+": Repetido");
							mensajes += "Fall� Codigo Solucitud "+codSol+": Repetido</br>";
						}
					}catch (GDEException e1){
						System.out.println("Fall� Codigo Solucitud "+codSol+": "+e1.getMensage());
						mensajes += "Fall� Codigo Solucitud "+codSol+": "+e1.getMensage()+"</br>";
						contador_lineas++;
					}
	
				}
				
				if (contador_lineas < periodicidad.getTamanoLote() && archivo_numero > 0) {
					// Cerrar e archivo
					if (salArch!=null) {
						salArch.close();
						salArch=null;
					}
				}
				
				respuesta[0]="OK";
				respuesta[1]=mensajes.equals("")?mensajes:"<br/><br/><b>Existen direcciones excepcionadas defectuosas</b><br/>"+mensajes;
				respuesta[2]=contador_registros+"";
				respuesta[3]=loteDirExc.getNombreArchivo();
				respuesta[4]=archivo_numero+"";
			}else{
				respuesta[0]="OK";
				respuesta[1]=(mensajes.equals("")?mensajes:"<br/><br/><b>Existen direcciones excepcionadas defectuosas</b><br/>"+mensajes);
				respuesta[2]=contador_registros+"";
				respuesta[3]=loteDirExc.getNombreArchivo();
				respuesta[4]=archivo_numero+"";			
			}
			
			// cierro el archivo
			if (salArch!=null) {
				salArch.close();
			}
			
		} catch (IOException e) {
			GDEException gdeException = new GDEException(MessageFormat.format(messageFileException,e.getMessage(),loteDirExc.getDirectorioRaiz()+loteDirExc.getNombreArchivo()));
			respuesta[1]=gdeException.getMensage();
			throw gdeException;
		} catch (Exception e) {
			e.printStackTrace();
			GDEException gdeException = new GDEException("Error General: "+e.getMessage());
			respuesta[1]=gdeException.getMensage();
			throw gdeException;
		}
		return respuesta;
	}

	/**
	 * M�todo que invoca el servicio de RespuestaDE
	 * @param dirExc
	 * 			Direcci�n Excepcionada
	 * @param direccion
	 * 			Direcci�n Georreferenciada
	 * @return
	 * @throws GDEException 
	 */
	private static String invocarRespuestaDE(DireccionExcepcionada dirExc, Direccion direccion) throws GDEException {
		
		String respCRM = direccion.ejecutarServicioRespuestaDE(direccion.getEstrato().toString(), dirExc.getCodigoSolicitud(), dirExc.getCodigoSistema(), 
				dirExc.getEstadoExcepcionType(), dirExc.getCodigoDireccion(), dirExc.getDireccionNormalizada(), direccion.getRural(), 
				dirExc.getEstadoGeorType(), direccion.getPais(), direccion.getMunicipio(), direccion.getDepartamento(),direccion.getLatitud(),direccion.getLongitud(),dirExc,direccion );

		if (!"".equals(respCRM)){
			
			if ("OK".equals(respCRM)){
				dirExc.setEstadoGestionType("PUBLICADA CRM");
			}else{
			
				dirExc.setEstadoGestionType("RESUELTA PROVEEDOR");
			}
		}
		
		return respCRM;
		
	}
	
	
}
