package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: Direccion.java
##	Contenido: Clase que implementa el objeto direcci�n, relacionado con la tabla detalle_direcci�n y con informaci�n de Georreferencia
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.math.BigInteger;
import java.util.Locale;
import java.util.ResourceBundle;

import co.net.une.www.svc.WSGeorreferenciarCRServiceStub;
import co.net.une.www.util.GDEException;

/**
 * Clase que implementa el objeto direcci�n, relacionado con la tabla detalle_direcci�n y con informaci�n de Georreferencia
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class Direccion extends Servicios {
	
	private String codigoDireccion = null;
	private String direccionNormalizada = null;
	private String estadoGeorType = null;
	private String agregado = null;
	private String codigoBarrio = null;
	private String codigoComuna = null;
	private String codigoDaneManzana = null;
	private String departamento = null;
	private String codigoDireccionProveevor = null;
	private String codigoLocalizacionTipo1 = null;	
	private String municipio = null;
	private String codigoPredio = null;
	private String coordenadaX = null;
	private String coordenadaY = null;
	private String direccionAnterior = null;
	private BigInteger estrato = null;
	private String latitud = null;
	private String longitud = null;
	private String nombreBarrio = null;
	private String nombreComuna = null;
	private String nombreLocalizacionTipo1 = null;
	private String placa = null;
	private String remanente = null;
	private String rural=null;
	private String tipoAgregacionNivel1 = null;
	private String pais = null;
	private String idProveedor = null;
	private String codigoUrbanizacion = null;
	private String ocupacionPuertos = null;
	private String direccionExcepcionada = null;
	private int id = 0;
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.Direccion", Locale.getDefault());
	private static final String messageGSSException = rb.getString("error.messageGSSException");
	
	//Configuraciones
	public static final String TABLE =  rb.getString("direccion.tabla");
	private static final String DATASET = rb.getString("direccion.dataset");
	private static final String ALLFIELDS =  rb.getString("direccion.campos");
	

	public Direccion() {
		super();
	}
	
	@Override
	public String obtenerDateSet() {
		return DATASET;
	}

	@Override
	public String obtenerTabla() {
		return TABLE;
	}

	public String getCodigoDireccion() {
		return codigoDireccion;
	}

	public void setCodigoDireccion(String codigoDireccion) {
		this.codigoDireccion = codigoDireccion;
	}

	public String getDireccionNormalizada() {
		return direccionNormalizada;
	}

	public void setDireccionNormalizada(String direccionNormalizada) {
		this.direccionNormalizada = direccionNormalizada;
	}

	public String getEstadoGeorType() {
		return estadoGeorType;
	}

	public void setEstadoGeorType(String estadoGeorType) {
		this.estadoGeorType = estadoGeorType;
	}

	public String getAgregado() {
		return agregado;
	}

	public void setAgregado(String agregado) {
		this.agregado = agregado;
	}

	public String getCodigoBarrio() {
		return codigoBarrio;
	}

	public void setCodigoBarrio(String codigoBarrio) {
		this.codigoBarrio = codigoBarrio;
	}

	public String getCodigoComuna() {
		return codigoComuna;
	}

	public void setCodigoComuna(String codigoComuna) {
		this.codigoComuna = codigoComuna;
	}

	public String getCodigoDaneManzana() {
		return codigoDaneManzana;
	}

	public void setCodigoDaneManzana(String codigoDaneManzana) {
		this.codigoDaneManzana = codigoDaneManzana;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getCodigoDireccionProveevor() {
		return codigoDireccionProveevor;
	}

	public void setCodigoDireccionProveevor(String codigoDireccionProveevor) {
		this.codigoDireccionProveevor = codigoDireccionProveevor;
	}

	public String getCodigoLocalizacionTipo1() {
		return codigoLocalizacionTipo1;
	}

	public void setCodigoLocalizacionTipo1(String codigoLocalizacionTipo1) {
		this.codigoLocalizacionTipo1 = codigoLocalizacionTipo1;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCodigoPredio() {
		return codigoPredio;
	}

	public void setCodigoPredio(String codigoPredio) {
		this.codigoPredio = codigoPredio;
	}

	public String getCoordenadaX() {
		return coordenadaX;
	}

	public void setCoordenadaX(String coordenadaX) {
		this.coordenadaX = coordenadaX;
	}

	public String getCoordenadaY() {
		return coordenadaY;
	}

	public void setCoordenadaY(String coordenadaY) {
		this.coordenadaY = coordenadaY;
	}

	public String getDireccionAnterior() {
		return direccionAnterior;
	}

	public void setDireccionAnterior(String direccionAnterior) {
		this.direccionAnterior = direccionAnterior;
	}

	public BigInteger getEstrato() {
		return estrato;
	}

	public void setEstrato(BigInteger estrato) {
		this.estrato = estrato;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getNombreBarrio() {
		return nombreBarrio;
	}

	public void setNombreBarrio(String nombreBarrio) {
		this.nombreBarrio = nombreBarrio;
	}

	public String getNombreComuna() {
		return nombreComuna;
	}

	public void setNombreComuna(String nombreComuna) {
		this.nombreComuna = nombreComuna;
	}

	public String getNombreLocalizacionTipo1() {
		return nombreLocalizacionTipo1;
	}

	public void setNombreLocalizacionTipo1(String nombreLocalizacionTipo1) {
		this.nombreLocalizacionTipo1 = nombreLocalizacionTipo1;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getRemanente() {
		return remanente;
	}

	public void setRemanente(String remanente) {
		this.remanente = remanente;
	}

	public String getRural() {
		return rural;
	}

	public void setRural(String rural) {
		this.rural = rural;
	}

	public String getTipoAgregacionNivel1() {
		return tipoAgregacionNivel1;
	}

	public void setTipoAgregacionNivel1(String tipoAgregacionNivel1) {
		this.tipoAgregacionNivel1 = tipoAgregacionNivel1;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getCodigoUrbanizacion() {
		return codigoUrbanizacion;
	}

	public void setCodigoUrbanizacion(String codigoUrbanizacion) {
		this.codigoUrbanizacion = codigoUrbanizacion;
	}

	/**
	 * M�todo que permite obtener una direccci�n georreferenciada
	 * @param pais
	 * 			c�digo del pa�s
	 * @param departamento
	 * 			c�digo del departamento
	 * @param municipio
	 * 			c�digo del municipio
	 * @param direccionNormalizada
	 * 			Direcci�n Normalizada
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerDireccionGeoreferenciada(String pais,String departamento, String municipio,String direccionNormalizada) throws GDEException {
		try {
			ejecutarServicioObtenerDirGeoreferenciada(pais, departamento, municipio, direccionNormalizada);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

	public String getOcupacionPuertos() {
		return ocupacionPuertos;
	}

	public void setOcupacionPuertos(String ocupacionPuertos) {
		this.ocupacionPuertos = ocupacionPuertos;
	}

	public String getDireccionExcepcionada() {
		return direccionExcepcionada;
	}

	public void setDireccionExcepcionada(String direccionExcepcionada) {
		this.direccionExcepcionada = direccionExcepcionada;
	}

	@Override
	protected void obtenerObjeto(Object registro) throws GDEException {
		WSGeorreferenciarCRServiceStub.GisCommonInfoDirType infoDirType = (WSGeorreferenciarCRServiceStub.GisCommonInfoDirType) registro;
		setCodigoDireccion(infoDirType.getCodigoDireccion());
		setDireccionNormalizada(infoDirType.getDireccionNormalizada());
		setEstadoGeorType(infoDirType.getEstadoGeoreferenciacion());
		setAgregado(infoDirType.getAgregado());
		setCodigoBarrio(infoDirType.getCodigoBarrio());
		setCodigoComuna(infoDirType.getCodigoComuna());
		setCodigoDaneManzana(infoDirType.getCodigoDaneManzana());
		setDepartamento(infoDirType.getCodigoDepartamento());
		setCodigoDireccionProveevor(infoDirType.getCodigoDireccionProveevor());
		setCodigoLocalizacionTipo1(infoDirType.getCodigoLocalizacionTipo1());
		setMunicipio(infoDirType.getCodigoMunicipio());
		setCodigoPredio(infoDirType.getCodigoPredio());
		setCoordenadaX(infoDirType.getCoordenadaX());
		setCoordenadaY(infoDirType.getCoordenadaY());
		setDireccionAnterior(infoDirType.getDireccionAnterior());
		setEstrato(infoDirType.getEstrato());
		setLatitud(infoDirType.getLatitud());
		setLongitud(infoDirType.getLongitud());
		setNombreBarrio(infoDirType.getNombreBarrio());
		setNombreComuna(infoDirType.getNombreComuna());
		setNombreLocalizacionTipo1(infoDirType.getNombreLocalizacionTipo1());
		setPlaca(infoDirType.getPlaca());
		setRemanente(infoDirType.getRemanente());
		setRural(infoDirType.getRural());
		setTipoAgregacionNivel1(infoDirType.getTipoAgregacionNivel1());
		setPais(infoDirType.getCodigoPais());
	}
	
	@Override
	protected void obtenerObjeto(String registros) throws GDEException {
		if (registros != null && !"0".equals(registros)){
			String[] registro = registros.split("\\|");
			for (int i=0; i < registro.length;i++){
				registro[i] = registro[i].replaceAll("[\\[\\]]", "");
				//Coloco ## para identificar los campos que vienen vac�os
				while(registro[i].indexOf(";;")>=0){
					registro[i] = registro[i].replaceAll(";;", ";##;");
				}
				if (registro[i].endsWith(";")) registro[i] = registro[i]+"##";
				
				String[] campos = registro[i].split(";");
				setCodigoDireccion(quitarMascaraTexto(campos[0]));
				setPais(quitarMascaraTexto(campos[1]));
				setDepartamento(quitarMascaraTexto(campos[2]));
				setMunicipio(quitarMascaraTexto(campos[3]));
				setCodigoComuna(quitarMascaraTexto(campos[4]));
				setCodigoBarrio(quitarMascaraTexto(campos[5]));
				setCodigoPredio(quitarMascaraTexto(campos[6]));
				setCodigoDireccionProveevor(quitarMascaraTexto(campos[7]));
				setDireccionAnterior(quitarMascaraTexto(campos[8]));
				setPlaca(quitarMascaraTexto(campos[9]));
				setDireccionNormalizada(quitarMascaraTexto(campos[10]));
				setRural(quitarMascaraTexto(campos[11]));
				setEstadoGeorType(quitarMascaraTexto(campos[12]));
				BigInteger estra = new BigInteger("0");
				 try {
					estra = new BigInteger(quitarMascaraTexto(campos[13]));
				} catch (Exception e) {}
				setEstrato(estra);
				
				setLatitud(quitarMascaraTexto(campos[14]));
				setLongitud(quitarMascaraTexto(campos[15]));
				setCodigoDaneManzana(quitarMascaraTexto(campos[16]));
				setAgregado(quitarMascaraTexto(campos[17]));
				setIdProveedor(quitarMascaraTexto(campos[18]));
				setCodigoUrbanizacion(quitarMascaraTexto(campos[19]));
				setNombreBarrio(quitarMascaraTexto(campos[20]));
				setNombreComuna(quitarMascaraTexto(campos[21]));
				setRemanente("##".equals(campos[22])||" ".endsWith(campos[22])?null:campos[22]);
				setOcupacionPuertos(registros);
				setDireccionExcepcionada(quitarMascaraTexto(campos[23]));

			}
		}
	}
	
	/**
	 * M�todo que permite obtener un detalle direcci�n
	 * @param codigoDireccion
	 * 			C�digo del detalle de direcci�n
	 * @throws GDEException
	 * 			En caso de una falla en el servicio se dispara una excepci�n
	 */
	public void obtenerDetalleDirPorCodigo(String codigoDireccion) throws GDEException {
		try {
			ejecutarServicioConsultaTabla(ALLFIELDS, "codigo", codigoDireccion);
		} catch (GDEException e) {
			e.setMensage(messageGSSException+" "+e.getMensage());
			throw e;
		}	
		
	}

}
