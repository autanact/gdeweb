package co.net.une.www.bean;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: HistoricoVersionDireccionExcepcionada.java
##	Contenido: Clase que implementa el objeto  de una lista ID de hist�rico versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_histo_version_direxc_lt 
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que implementa el objeto  de una lista ID de hist�rico versi�n direcci�n excepcionada, relacionado con la tabla ds_cd_histo_version_direxc_lt
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class HistoricoVersionDireccionExcepcionadaList {

	private List<HistoricoVersionDireccionExcepcionada> listVerDirExec = null;

	public HistoricoVersionDireccionExcepcionadaList() {
		super();
		listVerDirExec = new ArrayList<HistoricoVersionDireccionExcepcionada>();
	}

	public List<HistoricoVersionDireccionExcepcionada> getListVerDirExec() {
		return listVerDirExec;
	}

	public void setListVerDirExec(
			List<HistoricoVersionDireccionExcepcionada> listVerDirExec) {
		this.listVerDirExec = listVerDirExec;
	}
	
	
}
