package co.net.une.www.motores;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: MotorGeneradorLotes.java
##	Contenido: Clase que implementa el motor de generaci�n de lotes
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Creaci�n de la clase
##**********************************************************************************************************************************
*/
import java.io.File;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;



import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.bean.LoteDireccionExcepcionada;
import co.net.une.www.util.GDEException;

/**
 * Clase que implementa el motor de generaci�n de lotes
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class MotorGeneradorLotes {
	
	private static final ResourceBundle rb = ResourceBundle.getBundle("co.resources.MotorGeneradorLotes", Locale.getDefault());
	private static final String faltanParametros = rb.getString("error.parametros.faltanParametros");
	private static final String directorioNoExiste = rb.getString("error.archivo.directorioNoExiste");
	private static final String directorioErrorLectura = rb.getString("error.archivo.directorioErrorLectura");
	private static final String messageGDEError = rb.getString("error.messageGDEError");

	/**
	 * M�todo que permite ejecutar la motor desde un shell
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Inicia ejecuci�n de motor de Generaci�n de Lotes");
		
		try {
			
			//validaci�n de par�metros
			if ((args!= null)  && (args.length==1)){
				
				File directorio = new File(args[0]);
				
				if (!directorio.isDirectory()){
					GDEException gdeException = new GDEException(MessageFormat.format(directorioNoExiste, args[0]));
					throw gdeException;
				}
				
				if (!directorio.canWrite()){
					GDEException gdeException = new GDEException(MessageFormat.format(directorioErrorLectura, args[0]));
					throw gdeException;
				}
				
				// ejecuto el motor
				ejecutarMotor(args[0]);
			}else{
				System.out.println(messageGDEError+" "+faltanParametros);
			}
				
		} catch (GDEException e) {
			System.out.println(e.getMensage());
		}
		System.out.println("Termino el proceso");
	}

	/**
	 * M�todo que invoca la ejecuci�n del motor
	 * @param configPath
	 * 			direcci�n donde se van a guardar los lotes
	 */
	public static void ejecutarMotor(String configPath) {
	
		try {
			LoteDireccionExcepcionada.publicarLote(ServiciosUtil.getJobUsuario(), configPath);
		}catch (GDEException e) {
			System.out.println(e.getMensage());
		}
		System.out.println("Termino el proceso");
	}

}
