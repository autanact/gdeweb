/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: WSObtenerObjetosURNCallbackHandler.java
##	Contenido: Archivo que contiene la implementaci�n de la clase WSObtenerObjetosURNCallbackHandler del servicio WSObtenerObjetosURN
## 			   (ubica elementos de tablas de SW a partir de su identificador)
##	Autor: Freddy Molina
##  Fecha creaci�n: 25-02-2016
##	Fecha �ltima modificaci�n: 01-03-2016
##	Historial de cambios:  
##		01-03-2016 Creaci�n autom�tica
##**********************************************************************************************************************************
*/
/**
 * WSObtenerObjetosURNCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package co.net.une.www.svc;

    /**
     *  WSObtenerObjetosURNCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class WSObtenerObjetosURNCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public WSObtenerObjetosURNCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public WSObtenerObjetosURNCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for obtenerObjetosURN method
            * override this method for handling normal response from obtenerObjetosURN operation
            */
           public void receiveResultobtenerObjetosURN(
                    co.net.une.www.svc.WSObtenerObjetosURNStub.WSObtenerObjetosURNRS result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from obtenerObjetosURN operation
           */
            public void receiveErrorobtenerObjetosURN(java.lang.Exception e) {
            }
                


    }
    