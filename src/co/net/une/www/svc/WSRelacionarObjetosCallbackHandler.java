/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: WSRelacionarObjetosCallbackHandler.java
##	Contenido: Archivo que contiene la implementaci�n de la clase WSRelacionarObjetosCallbackHandler del servicio WSRelacionarObjetos
## 			   (relaciona elementos de tablas de SW)
##	Autor: Freddy Molina
##  Fecha creaci�n: 25-02-2016
##	Fecha �ltima modificaci�n: 01-03-2016
##	Historial de cambios:  
##		01-03-2016 Creaci�n autom�tica
##**********************************************************************************************************************************
*/
/**
 * WSRelacionarObjetosCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package co.net.une.www.svc;

    /**
     *  WSRelacionarObjetosCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class WSRelacionarObjetosCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public WSRelacionarObjetosCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public WSRelacionarObjetosCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for relacionarObjetos method
            * override this method for handling normal response from relacionarObjetos operation
            */
           public void receiveResultrelacionarObjetos(
                    co.net.une.www.svc.WSRelacionarObjetosStub.WSRelacionarObjetosRS result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from relacionarObjetos operation
           */
            public void receiveErrorrelacionarObjetos(java.lang.Exception e) {
            }
                


    }
    