/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: WSEliminarRelacionObjetoCallbackHandler.java
##	Contenido: Archivo que contiene la implementaci�n de la clase WSEliminarRelacionObjetoCallbackHandler del servicio WSEliminarRelacionObjeto
## 			   (elimina relaciones entre tablas de SW)
##	Autor: Freddy Molina
##  Fecha creaci�n: 25-02-2016
##	Fecha �ltima modificaci�n: 01-03-2016
##	Historial de cambios:  
##		01-03-2016 Creaci�n autom�tica
##**********************************************************************************************************************************
*/
/**
 * WSEliminarRelacionObjetoCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package co.net.une.www.svc;

    /**
     *  WSEliminarRelacionObjetoCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class WSEliminarRelacionObjetoCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public WSEliminarRelacionObjetoCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public WSEliminarRelacionObjetoCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for eliminarRelacionObjeto method
            * override this method for handling normal response from eliminarRelacionObjeto operation
            */
           public void receiveResulteliminarRelacionObjeto(
                    co.net.une.www.svc.WSEliminarRelacionObjetoStub.WSEliminarRelacionObjetoRS result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from eliminarRelacionObjeto operation
           */
            public void receiveErroreliminarRelacionObjeto(java.lang.Exception e) {
            }
                


    }
    