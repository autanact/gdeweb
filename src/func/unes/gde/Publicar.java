package func.unes.gde;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: enviarCRM.java
##	Contenido: Clase que implementa la publicaci�n de direcciones excepciondadas. Se basa en las clases de publicaci�n por Lotes de 
##				direcciones excepcionadas de  co.net.une.www.bean.LoteDireccionExcepcionada en el archivo MotoresGDE.jar
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.util.ResourceBundle;


import co.net.une.www.bean.LoteDireccionExcepcionada;
import co.net.une.www.bean.PeridiocidadDirecciones;
import co.net.une.www.util.GDEException;

/**
 * Clase que implementa la publicaci�n de direcciones excepciondadas. Se basa en las clases de publicaci�n por Lotes de 
 *			direcciones excepcionadas de  co.net.une.www.bean.LoteDireccionExcepcionada en el archivo MotoresGDE.jar
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class Publicar {

	// archivos de configuraciones
	private static final ResourceBundle rbMensajes = ResourceBundle.getBundle("properties.mensajes");

	// errores
	private static final String errorSerPeriocidad = rbMensajes.getString("error.pendientes.publicar.serperiocidad");


	// mensajes
	private static final String sinTamanioLote = rbMensajes.getString("mensaje.pendientes.publicar.sintamanioLote");

	//codigo de respuesta de proceso
	private String codigoRespuesta = "KO";
	//descripci�n del error
	private String descripcionError = "";
	//cantidad de registros publicados
	private int cantidadRegistros = 0;
	//nombre de archivo Biz
	private String nombrArchivoBiz = "";
	//n�mero de archivo Biz
	private int archivoNumeroBiz = 0;

	/**
	 * Constructor
	 */
	public Publicar() {
	}

	/**
	 * Funci�n que retorna el c�digo de respuesta del proceso
	 * @return
	 * 		el c�digo de respuesta del proceso
	 */
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	/**
	 * M�todo que asigna el c�digo de respuesta del proceso
	 * @param codigoRespuesta
	 * 			c�digo de respuesta del proceso
	 */
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	/**
	 * Funci�n que retorna la descripci�n del error
	 * @return
	 * 		descripci�n del error
	 */
	public String getDescripcionError() {
		return descripcionError;
	}

	/**
	 * M�todo que asigna la descripci�n del error
	 * @param descripcionError
	 * 			descripci�n del error
	 */
	public void setDescripcionError(String descripcionError) {
		this.descripcionError = descripcionError;
	}

	/**
	 * Funci�n que retorna la cantidad de registros publicados
	 * @return
	 * 		cantidad de registros publicados
	 */
	public int getCantidadRegistros() {
		return cantidadRegistros;
	}

	/**
	 * M�todo que asigna la cantidad de registros a publicar
	 * @param cantidadRegistros
	 * 			cantidad de registros a publicar
	 */
	public void setCantidadRegistros(int cantidadRegistros) {
		this.cantidadRegistros = cantidadRegistros;
	}

	/**
	 * Funci�n que retorna el nombre del archivo generado
	 * @return
	 * 		 nombre del archivo generado
	 */
	public String getNombrArchivoBiz() {
		return nombrArchivoBiz;
	}

	/**
	 * M�todo que asigna el nombre del archivo generado
	 * @param nombrArchivoBiz
	 * 			 nombre del archivo generado
	 */
	public void setNombrArchivoBiz(String nombrArchivoBiz) {
		this.nombrArchivoBiz = nombrArchivoBiz;
	}

	/**
	 * Funci�n que retorna el n�mero de archivo de bistalk
	 * @return
	 * 		n�mero de archivo de bistalk
	 */
	public int getArchivoNumeroBiz() {
		return archivoNumeroBiz;
	}

	/**
	 * M�todo que asiga el n�mero de archivo de bistalk
	 * @param archivoNumeroBiz
	 * 			n�mero de archivo de bistalk
	 */
	public void setArchivoNumeroBiz(int archivoNumeroBiz) {
		this.archivoNumeroBiz = archivoNumeroBiz;
	}

	/**
	 * M�todo que ejecuta la publicaci�n de direcciones excepcionadas en el lote
	 * @param usuario
	 * 			usario que ejecuta la publicaci�n
	 * @param path
	 * 			directorio donde se guarda la publicaci�n de los archivos de lotes
	 * @throws org.apache.axis2.AxisFault
	 * 			Si a nivel de Servicio interno se presenta un error se lanza una excepci�n de Axis
	 * @throws GDEException
	 * 			Si se presenta alg�n error controlable, se dispara la excepci�n
	 */
	public void publicarDirExc(String usuario, String path)	throws org.apache.axis2.AxisFault, GDEException {

		// variable que contiene la respuesta del proceso
		/*
		 * respuesta[0] = C�digo de respuesta del proceso OK|KO
		 * respuesta[1] = Descripci�n del error
		 * respuesta[2] = Cantidad de registros prublicados
		 * respuesta[3] = Nombre del archivo biz
		 * respuesta[4] = Cantidad de archivos publicados
		 */
		String[] respuesta = new String[5];

		//Se instancia la periodicidad
		PeridiocidadDirecciones periodicidad = new PeridiocidadDirecciones();
		try {
			// Se obtiene la pericidad de lotes
			periodicidad.obtenerPeriodicidadDirecciones();
		} catch (GDEException e1) {
			e1.setMensage(errorSerPeriocidad);
			setDescripcionError(errorSerPeriocidad);
			throw e1;
		}

		//Si el tama�o del lote es cero
		if (periodicidad.getTamanoLote() == 0) {
			//se lanza execpci�n informado que el tama�o de lote es cero
			setCodigoRespuesta("OK");
			setDescripcionError(sinTamanioLote);
			GDEException e1 = new GDEException();
			e1.setMensage(sinTamanioLote);
			throw e1;

		}

		try {
			
			//Ejecuto la publicaci�n del lote
			respuesta = LoteDireccionExcepcionada.publicarLote(usuario, path);
			
			//se obtiene y arma la respuesta de proceso
			setCodigoRespuesta(respuesta[0]);
			setDescripcionError(respuesta[1]);
			setCantidadRegistros(Integer.parseInt(respuesta[2]));
			setNombrArchivoBiz(respuesta[3]);
			setArchivoNumeroBiz(Integer.parseInt(respuesta[4]));

		} catch (GDEException e) {
			e.printStackTrace();
			setDescripcionError(e.getMessage());
			
		}catch (Exception e) {
			e.printStackTrace();
			setDescripcionError(e.getMessage());
		}

	}

}
