package func.unes.gde;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: Seguridad.java
##	Contenido: Clase que implementa la seguridad de los menues del GUI. Lee los atributos del archivo WebContent\xml\grupos.xml
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

/**
 * Clase que implementa la seguridad de los menues del GUI. Lee los atributos del archivo WebContent\xml\grupos.xml
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class Seguridad {
	
	/**
	 * Funci�n que lee la informaci�n de los grupos del archivo WebContent\xml\grupos.xml para obtener lo menues 
	 * disponibles seg�n los roles enviados
	 * @param a_roles
	 * 			Arreglo de roles
	 * @param usuario
	 * 			usuario
	 * @param path
	 * 			ubicaci�n del archivo grupos.xml
	 * @return
	 * 		Arreglo con los accesos seg�n el rol<br/>
	 * 		a_menu_acceso[0] = men� a mostrar<br/>
	 * 		a_menu_acceso[1] = nivel del men�
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	public static String[] menu_acceso ( ArrayList a_roles,  String usuario, String path ) throws ParserConfigurationException, SAXException, IOException {
		
		String[] a_menu_acceso =new String[2];
		int max_nivel = 10;
		
		// Leer Archivo GRUPOS.XML //
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();

		Document doc = db.parse(path);

		NodeList GrupoID = doc.getElementsByTagName("GrupoID");
		NodeList GrupoNivel = doc.getElementsByTagName("GrupoNivel");
		NodeList GrupoMenu = doc.getElementsByTagName("GrupoMenu");
		
		a_menu_acceso[0]="";
	    a_menu_acceso[1]="";
	     
		for (int j=0;j<a_roles.size();j++){ 
        	 String rol = a_roles.get(j).toString();
        
             for(int i=0;i<=GrupoID.getLength()-1;i++) { 
	               if (rol.equals(GrupoID.item(i).getFirstChild().getNodeValue()) ){
		            	 if (max_nivel >  Integer.parseInt(GrupoNivel.item(i).getFirstChild().getNodeValue()))  { 
			            	 
						     a_menu_acceso[0]=GrupoMenu.item(i).getFirstChild().getNodeValue();
						     a_menu_acceso[1]=GrupoNivel.item(i).getFirstChild().getNodeValue();
						     max_nivel = Integer.parseInt(GrupoNivel.item(i).getFirstChild().getNodeValue());
		                }
	               }
	         }
		}
		
		return  a_menu_acceso;
		
	}
	
	

}
