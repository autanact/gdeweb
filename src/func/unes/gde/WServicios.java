package func.unes.gde;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: WServicios.java
##	Contenido: Clase que implementa la comunicaci�n a trav�s de los servicios publicados para GDE
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.math.BigInteger;
import java.rmi.*;

import co.net.une.www.bean.Direccion;
import co.net.une.www.bean.DireccionExcepcionada;
import co.net.une.www.svc.*;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQ;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQType;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRS;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Clase que implementa la comunicaci�n a trav�s de los servicios publicados para GDE
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class WServicios {
	
	/**
	 * Funci�n que implementa el cliente del servicio WSConsultarTablasGDE, para consulta de elementos de una tabla en SW
	 * @param NombreTabla
	 * 			nombre de la tabla
	 * @param NombreDataset
	 * 			nombre del dataset
	 * @param NombresCamposConsulta
	 * 			listado de nombres de los campos a filtrar
	 * @param ValoresCamposConsulta
	 * 			listado de los valores a filtrar
	 * @param NombreCamposResultado
	 * 			listado de nombres de los campos a mostrar
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = Descripci�n del error<br/>
	 *    	respuesta_array[2] = cantidad de registros obtenidos
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] consultartabla_cadena(String NombreTabla, String NombreDataset, String NombresCamposConsulta, String ValoresCamposConsulta, String NombreCamposResultado ) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String respuesta ="";
		String codigoRespuesta = "";
		String descripcionError = "";
		String respuesta_array[] = new String[3];  	
		try {
    	  	
	    	// creamos el soporte y la peticion WS
			WSConsultarTablasGDEStub consultar = new WSConsultarTablasGDEStub();
			WSConsultarTablasGDEStub.WSConsultarTablasGDERQ  consultarRequest = new WSConsultarTablasGDEStub.WSConsultarTablasGDERQ();
			WSConsultarTablasGDEStub.WSConsultarTablasGDERQType  consultarRequestType = new WSConsultarTablasGDEStub.WSConsultarTablasGDERQType();
			WSConsultarTablasGDEStub.WSConsultarTablasGDERS consultarResponse = new WSConsultarTablasGDEStub.WSConsultarTablasGDERS();
	       
			// establecemos el parametro de la invocacion 
			consultarRequestType.setNombreTabla(NombreTabla);
			consultarRequestType.setNombreDataset(NombreDataset);
			consultarRequestType.setNombresCamposConsulta(NombresCamposConsulta);
			consultarRequestType.setValoresCamposConsulta(ValoresCamposConsulta);
			consultarRequestType.setNombreCamposResultado(NombreCamposResultado);
					
			// invocamos al web service
			consultarRequest.setWSConsultarTablasGDERQ(consultarRequestType);
			consultar._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			consultarResponse = consultar.consultarTablasGDE(consultarRequest);
			
			respuesta= consultarResponse.getWSConsultarTablasGDERS().getRegistros();
			
			codigoRespuesta = consultarResponse.getWSConsultarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta();			 
			
			descripcionError = consultarResponse.getWSConsultarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
		    
			// Formateamos la respuesta
			respuesta_array[0]=codigoRespuesta;
			respuesta_array[1]=descripcionError;
			respuesta_array[2]=respuesta;
						
			return respuesta_array;
			
		} catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= errorWS;
		respuesta_array[2]= "";
		
        return respuesta_array;
    }

	
	/**
	 * Funci�n que transforma un string de resultados de una consulta del servicio WSConsultarTablasGDE en un arreglo 
	 * donde cada posici�n es un elemento de la consulta
	 * @param tira
	 * 			String con los registros de la consulta
	 * @return
	 * 		Arreglo con elementos del string de resultado
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList lista_registros ( String tira ) {
		ArrayList arrayrespuesta = new ArrayList();
		
		// Se eliminan los []
		tira = tira.replace("[", "");
		tira = tira.replace("]", "");
		
		// realiza la transformaci[on
		String[] parts = tira.split("\\|", -1);
		
		// Se ingrisan los elementos en los arreglos
	    for (int p=0; p<(parts.length); p++ ) {
	    	
	    	arrayrespuesta.add(parts[p]);
	   
	    }
	    
	    // Se retorna el arreglo
		return arrayrespuesta;
	}
	

	/**
	 * Funci�n que implementa el cliente del servicio WSInsertarTablasGDE, para insertar elementos dentro de una tabla en SW
	 * @param NombreTabla
	 * 			nombre de la tabla
	 * @param NombreDataset
	 * 			nombre del dataset
	 * @param NombresCampos
	 * 			listado de nombres de los campos a insertar
	 * @param setValoresCampos
	 * 			listado de los valores a filtrar
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = Descripci�n del error		
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] insertartabla(String NombreTabla, String NombreDataset, String NombresCampos, String setValoresCampos) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String codigoRespuesta = "";
		String descripcionError = "";
		String respuesta_array[] = new String[2];  	
		try {
    	  	
	    	// creamos el soporte y la peticion WS 
			WSInsertarTablasGDEStub insertar = new WSInsertarTablasGDEStub();
		 	WSInsertarTablasGDEStub.WSInsertarTablasGDERQ  insertarRequest = new WSInsertarTablasGDEStub.WSInsertarTablasGDERQ();
		 	WSInsertarTablasGDEStub.WSInsertarTablasGDERQType  insertarRequestType = new WSInsertarTablasGDEStub.WSInsertarTablasGDERQType();
		 	WSInsertarTablasGDEStub.WSInsertarTablasGDERS insertarResponse = new WSInsertarTablasGDEStub.WSInsertarTablasGDERS();
		 	
		 // establecemos el parametro de la invocacion 
			insertarRequestType.setNombreTabla(NombreTabla);
			insertarRequestType.setNombreDataset(NombreDataset);
			insertarRequestType.setNombresCampos(NombresCampos);
			insertarRequestType.setValoresCampos(setValoresCampos);
							
			// invocamos al web service
			insertarRequest.setWSInsertarTablasGDERQ(insertarRequestType);
			
			insertar._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);                                 
			insertarResponse = insertar.insertarTablasGDE(insertarRequest);
			
			codigoRespuesta = insertarResponse.getWSInsertarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta();			 
			descripcionError = insertarResponse.getWSInsertarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
			
			respuesta_array[0]=codigoRespuesta;
			respuesta_array[1]=descripcionError;
			
			return respuesta_array;
			
		} catch (RemoteException e) {
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= errorWS;
  
        return respuesta_array;
    }
	
	
	/**
	 * Funci�n que implementa el cliente del servicio WSActualizarTablasGDE, para actualiza elementos de una tabla en SW
	 * @param NombreTabla
	 * 			nombre de la tabla
	 * @param NombreDataset
	 * 			nombre del dataset
	 * @param NombreCampoClave
	 * 			listado de nombres de los campos a filtrar
	 * @param ValorCampoClave
	 * 			listado de los valores a filtrar
	 * @param NombresCamposAct
	 * 			listado de nombres de los campos a actualizar
	 * @param ValoresCamposAct
	 * 			listado de los valores a actualizar
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = Descripci�n del error
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] actualizartabla(String NombreTabla, String NombreDataset, String NombreCampoClave, String ValorCampoClave, String NombresCamposAct, String ValoresCamposAct) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String codigoRespuesta = "";
		String descripcionError = "";
		String respuesta_array[] = new String[2];  	
		try {
    	  	
	    	// creamos el soporte y la peticion WS 
			WSActualizarTablasGDEStub actualizar = new WSActualizarTablasGDEStub();
			WSActualizarTablasGDEStub.WSActualizarTablasGDERQ  actualizarRequest = new WSActualizarTablasGDEStub.WSActualizarTablasGDERQ();
			WSActualizarTablasGDEStub.WSActualizarTablasGDERQType  actualizarRequestType = new WSActualizarTablasGDEStub.WSActualizarTablasGDERQType();
			WSActualizarTablasGDEStub.WSActualizarTablasGDERS actualizarResponse = new WSActualizarTablasGDEStub.WSActualizarTablasGDERS();
		    
		 // establecemos el parametro de la invocacion 

			actualizarRequestType.setNombreTabla(NombreTabla);
			actualizarRequestType.setNombreDataset(NombreDataset);
			actualizarRequestType.setNombreCampoClave(NombreCampoClave);
			actualizarRequestType.setValorCampoClave(ValorCampoClave);
			actualizarRequestType.setNombresCamposAct(NombresCamposAct);
			actualizarRequestType.setValoresCamposAct(ValoresCamposAct);
							
			// invocamos al web service
			actualizarRequest.setWSActualizarTablasGDERQ(actualizarRequestType);
			
			actualizar._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			
			actualizarResponse = actualizar.actualizarTablasGDE(actualizarRequest);
						
			codigoRespuesta = actualizarResponse.getWSActualizarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta();			 
 			
			descripcionError = actualizarResponse.getWSActualizarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
			
			respuesta_array[0]=codigoRespuesta;
			respuesta_array[1]=descripcionError;
									
			return respuesta_array;
			
		} catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= errorWS;
  
        return respuesta_array;
    }

	
	
	/**
	 * Funci�n que implementa el cliente del servicio WSEliminarTablasGD, para eliminar elementos de una tabla en SW
	 * @param NombreTabla
	 * 			nombre de la tabla
	 * @param NombreDataset
	 * 			nombre del dataset
	 * @param NombreCampoClave
	 * 			listado de nombres de los campos a insertar
	 * @param ValorCampoClave
	 * 			listado de los valores a insertar
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = Descripci�n del error
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] eliminartabla(String NombreTabla, String NombreDataset, String NombreCampoClave, String ValorCampoClave) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String codigoRespuesta = "";
		String descripcionError = "";
		String respuesta_array[] = new String[2];  	
		try {
    	  	
	    	// creamos el soporte y la peticion WS 
			WSEliminarTablasGDEStub eliminar = new WSEliminarTablasGDEStub();
			WSEliminarTablasGDEStub.WSEliminarTablasGDERQ  eliminarRequest = new WSEliminarTablasGDEStub.WSEliminarTablasGDERQ();
			WSEliminarTablasGDEStub.WSEliminarTablasGDERQType eliminarRequestType= new WSEliminarTablasGDEStub.WSEliminarTablasGDERQType();
			WSEliminarTablasGDEStub.WSEliminarTablasGDERS eliminarResponse = new WSEliminarTablasGDEStub.WSEliminarTablasGDERS();
		    
			// establecemos el parametro de la invocacion 
			eliminarRequestType.setNombreTabla(NombreTabla);
			eliminarRequestType.setNombreDataset(NombreDataset);
			eliminarRequestType.setNombreCampoClave(NombreCampoClave);
			eliminarRequestType.setValorCampoClave(ValorCampoClave);
							
			// invocamos al web service
			eliminarRequest.setWSEliminarTablasGDERQ(eliminarRequestType);
			
			eliminar._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			
			eliminarResponse = eliminar.eliminarTablasGDE(eliminarRequest);
						
			codigoRespuesta = eliminarResponse.getWSEliminarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta();			 
 			
			descripcionError = eliminarResponse.getWSEliminarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
			
			respuesta_array[0]=codigoRespuesta;
			respuesta_array[1]=descripcionError;
									
			return respuesta_array;
			
		} catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= errorWS;
  
        return respuesta_array;
    }

	
	/**
	 * Funci�n que implementa el cliente del servicio WSRespuestaDE, para enviar los datos de una direcci�n excepcionada a CRM
	 * @param CodigoSolicitud
	 * 			C�digo de solicitud
	 * @param CodigoSistema
	 * 			C�digo de Sistema
	 * @param EstadoExcepcion
	 * 			Estado Excepci�n
	 * @param CodigoDireccion
	 * 			C�digo Direcci�n
	 * @param DireccionNormalizada
	 * 			Direcci�n Normalizada
	 * @param Estrato
	 * 			Estrato
	 * @param Rural
	 * 			Rural
	 * @param EstadoGeo
	 * 			Estado de Georreferencia
	 * @param Pais
	 * 			C�digo de pa�s
	 * @param Municipio
	 * 			C�digo de municipio
	 * @param Departamento
	 * 			C�digo de departamento
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = C�digo Error<br/>
	 *    	respuesta_array[2] = Descripci�n del error
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] respuestaDE(String CodigoSolicitud, String CodigoSistema, String EstadoExcepcion, String CodigoDireccion, String DireccionNormalizada, String Estrato, String Rural, String EstadoGeo, String Pais, String Municipio, String Departamento
			,String latitud, String longitud, DireccionExcepcionada dirExc, Direccion detDir) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String respuesta_array[] = new String[3];  	
		try {
    	  	
	    	// creamos el soporte y la peticion WS
			//asigno los par�metros de entrada
			WSRespuestaDEStub proxy = new WSRespuestaDEStub();
			WSRespuestaDEStub.WSRespuestaDERQ requestLocal = new WSRespuestaDEStub.WSRespuestaDERQ();
			requestLocal.setWSRespuestaDERQ(new WSRespuestaDEStub.WSRespuestaDERQType());
			WSRespuestaDEStub.WSRespuestaDERS response = null;
			WSRespuestaDEStub.BoundedString1 estadoGeo;
			estadoGeo = new WSRespuestaDEStub.BoundedString1();
			estadoGeo.setBoundedString1(EstadoGeo);
				
			WSRespuestaDEStub.BoundedString2 codDep,codPais,rural;
			codDep = new WSRespuestaDEStub.BoundedString2();
			codDep.setBoundedString2(Departamento);
			codPais = new WSRespuestaDEStub.BoundedString2();
			codPais.setBoundedString2(Pais);
			rural = new WSRespuestaDEStub.BoundedString2();
			rural.setBoundedString2(colocarMascara(Rural));
			
			WSRespuestaDEStub.BoundedString5 estPag;
			estPag = new WSRespuestaDEStub.BoundedString5();
			estPag.setBoundedString5("");
			
			
			WSRespuestaDEStub.BoundedString8 codMun;
			codMun = new WSRespuestaDEStub.BoundedString8();
			codMun.setBoundedString8(Municipio);
			
			
			WSRespuestaDEStub.BoundedString10 codigoBarrio,codigoComuna,codigoLocalizacionTipo1,placa;
			codigoBarrio = new WSRespuestaDEStub.BoundedString10();
			codigoBarrio.setBoundedString10(colocarMascara(detDir.getCodigoBarrio()));
			codigoComuna = new WSRespuestaDEStub.BoundedString10();
			codigoComuna.setBoundedString10(colocarMascara(detDir.getCodigoComuna()));
			codigoLocalizacionTipo1 = new WSRespuestaDEStub.BoundedString10();
			codigoLocalizacionTipo1.setBoundedString10(colocarMascara(detDir.getCodigoLocalizacionTipo1()));
			placa = new WSRespuestaDEStub.BoundedString10();
			placa.setBoundedString10(colocarMascara(detDir.getPlaca()));
					
			WSRespuestaDEStub.BoundedString15 csol = new WSRespuestaDEStub.BoundedString15();
			csol.setBoundedString15(CodigoSolicitud);
					
			WSRespuestaDEStub.BoundedString14 codigoDaneManzana;
			codigoDaneManzana = new WSRespuestaDEStub.BoundedString14();
			codigoDaneManzana.setBoundedString14(colocarMascara(detDir.getCodigoDaneManzana()));
				
			WSRespuestaDEStub.BoundedString17 codigoPredio;
			codigoPredio = new WSRespuestaDEStub.BoundedString17();
			codigoPredio.setBoundedString17(colocarMascara(detDir.getCodigoPredio()));
				
			WSRespuestaDEStub.BoundedString18 insta;
			insta = new WSRespuestaDEStub.BoundedString18();
			insta.setBoundedString18(colocarMascara(""));
			
			WSRespuestaDEStub.BoundedString20 tipoAgregacionNivel1;
			tipoAgregacionNivel1= new WSRespuestaDEStub.BoundedString20();
			tipoAgregacionNivel1.setBoundedString20(colocarMascara(detDir.getTipoAgregacionNivel1()));
				
			WSRespuestaDEStub.BoundedString30 csis = new WSRespuestaDEStub.BoundedString30();
			csis.setBoundedString30(CodigoSistema);
			
			WSRespuestaDEStub.BoundedString30 ee = new WSRespuestaDEStub.BoundedString30();
			ee.setBoundedString30(EstadoExcepcion);
					
			WSRespuestaDEStub.BoundedString100 cdir,agregado,codigoDireccionProveevor,nombreBarrio,nombreComuna,nombreLocalizacionTipo1,remanente;
			cdir = new WSRespuestaDEStub.BoundedString100();
			cdir.setBoundedString100(CodigoDireccion);
			codigoDireccionProveevor = new WSRespuestaDEStub.BoundedString100();
			codigoDireccionProveevor.setBoundedString100(colocarMascara(detDir.getCodigoDireccionProveevor()));
			agregado = new WSRespuestaDEStub.BoundedString100();
			agregado.setBoundedString100(colocarMascara(detDir.getAgregado()));
			nombreBarrio = new WSRespuestaDEStub.BoundedString100();
			nombreBarrio.setBoundedString100(colocarMascara(detDir.getNombreBarrio()));
			nombreComuna = new WSRespuestaDEStub.BoundedString100();
			nombreComuna.setBoundedString100(colocarMascara(detDir.getNombreComuna()));
			nombreLocalizacionTipo1 = new WSRespuestaDEStub.BoundedString100();
			nombreLocalizacionTipo1.setBoundedString100(colocarMascara(detDir.getNombreLocalizacionTipo1()));
			remanente = new WSRespuestaDEStub.BoundedString100();
			remanente.setBoundedString100(colocarMascara(detDir.getRemanente()));
			
			WSRespuestaDEStub.BoundedString250 direccionAnterior,dirEstandarNLectura,dirNorm;
			direccionAnterior = new WSRespuestaDEStub.BoundedString250();
			direccionAnterior.setBoundedString250(colocarMascara(detDir.getDireccionAnterior()));
			dirEstandarNLectura = new WSRespuestaDEStub.BoundedString250();
			dirEstandarNLectura.setBoundedString250(colocarMascara(dirExc.getDireccionEstandarNLectura()));
			dirNorm = new WSRespuestaDEStub.BoundedString250();
			dirNorm.setBoundedString250(colocarMascara(DireccionNormalizada));
				
			requestLocal.getWSRespuestaDERQ().setCodigoSolicitud(csol);
			requestLocal.getWSRespuestaDERQ().setCodigoSistema(csis);
			requestLocal.getWSRespuestaDERQ().setEstadoExcepcion(ee);
			requestLocal.getWSRespuestaDERQ().setCodigoDireccion(cdir);
				
			requestLocal.getWSRespuestaDERQ().setAgregado(agregado);
			requestLocal.getWSRespuestaDERQ().setCodigoBarrio(codigoBarrio);
			requestLocal.getWSRespuestaDERQ().setCodigoComuna(codigoComuna);
			requestLocal.getWSRespuestaDERQ().setCodigoDaneManzana(codigoDaneManzana);
			requestLocal.getWSRespuestaDERQ().setCodigoDepartamento(codDep);
			requestLocal.getWSRespuestaDERQ().setCodigoDireccionProveevor(codigoDireccionProveevor);
			requestLocal.getWSRespuestaDERQ().setCodigoLocalizacionTipo1(codigoLocalizacionTipo1);
			requestLocal.getWSRespuestaDERQ().setCodigoMunicipio(codMun);
			requestLocal.getWSRespuestaDERQ().setCodigoPais(codPais);
			requestLocal.getWSRespuestaDERQ().setCodigoPredio(codigoPredio);
			requestLocal.getWSRespuestaDERQ().setCoordenadaX(detDir.getCoordenadaX());
			requestLocal.getWSRespuestaDERQ().setCoordenadaY(detDir.getCoordenadaY());
			requestLocal.getWSRespuestaDERQ().setDireccionAnterior(direccionAnterior);
			requestLocal.getWSRespuestaDERQ().setDireccionEstandarNLectura(dirEstandarNLectura);
			requestLocal.getWSRespuestaDERQ().setDireccionNormalizada(dirNorm);
			requestLocal.getWSRespuestaDERQ().setEstadoGeoreferenciacion(estadoGeo);
			requestLocal.getWSRespuestaDERQ().setEstadoPagina(estPag);
			BigInteger estra = new BigInteger("0");
			
			 try {
				estra = new BigInteger(Estrato);
			} catch (Exception e) {}
			 
			requestLocal.getWSRespuestaDERQ().setEstrato(estra);
			requestLocal.getWSRespuestaDERQ().setInstalacion(insta);
			requestLocal.getWSRespuestaDERQ().setLatitud(latitud);
			requestLocal.getWSRespuestaDERQ().setLongitud(longitud);
			requestLocal.getWSRespuestaDERQ().setNombreBarrio(nombreBarrio);
			requestLocal.getWSRespuestaDERQ().setNombreComuna(nombreComuna);
			requestLocal.getWSRespuestaDERQ().setNombreLocalizacionTipo1(nombreLocalizacionTipo1);
			requestLocal.getWSRespuestaDERQ().setPlaca(placa);
			requestLocal.getWSRespuestaDERQ().setRemanente(remanente);
			requestLocal.getWSRespuestaDERQ().setRural(rural);
			requestLocal.getWSRespuestaDERQ().setTipoAgregacionNivel1(tipoAgregacionNivel1);
			
			// invocamos al web service
			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			response = proxy.respuestaDE(requestLocal);
	
			respuesta_array[0]=response.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoRespuesta();
			respuesta_array[1]=response.getWSRespuestaDERS().getGisRespuestaProceso().getCodigoError();
			respuesta_array[2]=response.getWSRespuestaDERS().getGisRespuestaProceso().getDescripcionError();
								
			return respuesta_array;
			
		} catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= "";
		respuesta_array[2]= errorWS;
		
        return respuesta_array;
    }

	
	/**
	 * Funci�n que implementa el cliente del servicio WSGeorreferenciarCRService, para Georreferenciar una direcci�n
	 * @param codigoDepartamento
	 * 			C�digo Departamento
	 * @param codigoMunicipio
	 * 			C�digo Municipio
	 * @param codigoPais
	 * 			C�digo Pa�s
	 * @param direccionNatural
	 * 			Direccion Natural
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = C�digo del error<br/>
	 *    	respuesta_array[2] = Descripci�n del error<br/>
	 * 		respuesta_array[3] = C�digo direcci�n<br/>
	 * 		respuesta_array[4] = Direcci�n Normalizada<br/>
	 *    	respuesta_array[5] = Estado de Georreferencia
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] GeorreferenciarCR(String codigoDepartamento, String codigoMunicipio, String codigoPais, String direccionNatural  ) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String respuesta_array[] = new String[6]; 

		try {
    					
			   WSGeorreferenciarCRServiceStub geoProxy = new WSGeorreferenciarCRServiceStub();        
			   WSGeorreferenciarCRRQ geoRequest = new WSGeorreferenciarCRRQ();
			   WSGeorreferenciarCRRQType geoRequestType = new WSGeorreferenciarCRRQType();
			   WSGeorreferenciarCRRS geoResponse = new WSGeorreferenciarCRRS();

			   geoRequestType.setCodigoDepartamento(codigoDepartamento);
			   geoRequestType.setCodigoMunicipio(codigoMunicipio);
			   geoRequestType.setCodigoPais(codigoPais);
			   geoRequestType.setDireccionNatural(direccionNatural);
			   //---- Agregados ----
			   geoRequestType.setLatitud("0");
			   geoRequestType.setLongitud("0");
			   geoRequestType.setUNE_Cobertura_Especial(0);
			   
			   geoRequest.setWSGeorreferenciarCRRQ(geoRequestType);
			   geoProxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			   
			   geoResponse = geoProxy.georeferenciarCR(geoRequest);

			   respuesta_array[0]=geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getCodigoRespuesta();
			   respuesta_array[1]=geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getCodigoError();
			   respuesta_array[2]=geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getDescripcionError();
			   respuesta_array[3]=geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getCodigoDireccion();
			   respuesta_array[4]=geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getDireccionNormalizada();
			   respuesta_array[5]=geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getEstadoGeoreferenciacion();
			   
			   return respuesta_array;
			
		} catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		} catch (Exception e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= "";
		respuesta_array[2]= errorWS;
		respuesta_array[3]= "";
		respuesta_array[4]= "";
		respuesta_array[5]= "";
		
		
        return respuesta_array;
    }
	
	
	/**
	 * Funci�n que permite ordenar de manera ascendente arreglos de elementos por los valones de una de sus columnas
	 * @param a
	 * 			Arreglo de registros a ordenar. Cada registro es una lista de elementos separados por ;
	 * @param col
	 * 			N�mero de la columna por la cual se va a ordenar
	 * @return
	 * 		Arreglo con los elemenetos ordenados de manera ascendente por los valores de una columna
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ArrayList ordenar_abc(ArrayList a, int col) {
		
		// Inicializo las variables de control
		ArrayList anew = new ArrayList();
		ArrayList aordenado = new ArrayList();
		ArrayList agrupo = new ArrayList();
			
		// De cada registro del arreglo 
		for (int j=0;j<a.size();j++){ 
				// Obtengo el registro
        	   String rows = a.get(j).toString();
        	   	// Separo por ";" y armo el arreglo
        	   String[]  values = rows.split("\\;", -1);
        	   // Se ingresa en un nuevo arreglo el elemento de la posici�n COL
        	   anew.add(values[col]);
		}
		
		// Ordeno el arreglo de manera ascendente
		Collections.sort(anew);
		
		String actual = "";
		
		// Por cada elemento del arreglo ordenado
		for (int j=0;j<anew.size();j++){ 
			
			// Si el elemento no est� en la lista "agrupo" lo inserto (elimino los repetidos)
			if (!actual.equals(anew.get(j).toString())) {
				agrupo.add(anew.get(j).toString());
				actual = anew.get(j).toString();
			}
			
		}
		
		// Por cada uno de los elementos sin repetir (Clave)
		for (int j=0;j<agrupo.size();j++){ 
     	   
			// Por cada uno de los registros del arreglo original
     	    for (int c=0;c<a.size();c++){ 
     	    	
     	    	// Si la clave se encuentra en el registro
	     	     if( a.get(c).toString().indexOf(agrupo.get(j).toString())>=0  ){
	     	    	 // Ingreso el registro en el nuevo arreglo
	     	    	aordenado.add( a.get(c).toString() );
				 } 
     	    } 
     	     
		}
		
		// Retorno el arreglo con los elementos ordenados
		return aordenado;
	}
	
	

	
	/**
	 * Funci�n que implementa el cliente del servicio WSRelacionarObjetos, para relacionar tablas en SW
	 * @param Dataset
	 * 			nombre de dataset
	 * @param TablaBase
	 * 			nombre de tabla base
	 * @param CodigoBase
	 * 			Valor del c�digo base
	 * @param TablaRelacionada
	 * 			nombre de tabla relacionada
	 * @param CodigoRelacionado
	 * 			valor de c�digo base relacionado
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = Descripci�n del error
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] relacionarObjetos(String Dataset, String TablaBase, String CodigoBase, String TablaRelacionada, String CodigoRelacionado) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String codigoRespuesta = "";
		String descripcionError = "";
		String respuesta_array[] = new String[2];  	
		try {
    	  	
	    	// creamos el soporte y la peticion WS 
			WSRelacionarObjetosStub relacionar = new WSRelacionarObjetosStub();
			WSRelacionarObjetosStub.WSRelacionarObjetosRQ  relacionarRequest = new WSRelacionarObjetosStub.WSRelacionarObjetosRQ();
			WSRelacionarObjetosStub.WSRelacionarObjetosRQType  relacionarRequestType = new WSRelacionarObjetosStub.WSRelacionarObjetosRQType();
			WSRelacionarObjetosStub.WSRelacionarObjetosRS relacionarResponse = new WSRelacionarObjetosStub.WSRelacionarObjetosRS();
		 
			// establecemos el parametro de la invocacion 
			relacionarRequestType.setDataset(Dataset);
			relacionarRequestType.setTablaBase(TablaBase);
			relacionarRequestType.setCodigoBase(CodigoBase);
			relacionarRequestType.setTablaRelacionada(TablaRelacionada);
			relacionarRequestType.setCodigoRelacionado(CodigoRelacionado);
									
			// invocamos al web service
			relacionarRequest.setWSRelacionarObjetosRQ(relacionarRequestType);
			
			relacionar._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			                                  
			relacionarResponse = relacionar.relacionarObjetos(relacionarRequest);
			
			codigoRespuesta = relacionarResponse.getWSRelacionarObjetosRS().getGisRespuestaProceso().getCodigoRespuesta();			 
 			
			descripcionError = relacionarResponse.getWSRelacionarObjetosRS().getGisRespuestaProceso().getDescripcionError();
			
			respuesta_array[0]=codigoRespuesta;
			respuesta_array[1]=descripcionError;
									
			return respuesta_array;
			
		} catch (RemoteException e) {
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= errorWS;
  
        return respuesta_array;
    }
	/*** fin relacionarObjetos***/
	
	

	/**
	 * Funci�n que implementa el cliente del servicio WSEliminarRelacionObjeto, para eliminar relaciones entre tablas en SW
	 * @param NombreDataset
	 * 			nombre del dataset
	 * @param NombreTabla
	 * 			nombre de la tabla
	 * @param ValorCampoClave
	 * 			valores a elimiar relaci�n
	 * @param NombresCamposAct
	 * 			nombres del campo clave
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = Descripci�n del error
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] eliminarRelacion( String NombreDataset, String NombreTabla, String ValorCampoClave, String NombresCamposAct ) throws org.apache.axis2.AxisFault {
		String errorWS = "";
		String respuesta_array[] = new String[2];
		
		try {
			
			// creamos el soporte y la peticion WS 
			WSEliminarRelacionObjetoStub quitar = new WSEliminarRelacionObjetoStub();
			WSEliminarRelacionObjetoStub.WSEliminarRelacionObjetoRQ  quitarRequest = new WSEliminarRelacionObjetoStub.WSEliminarRelacionObjetoRQ();
			WSEliminarRelacionObjetoStub.WSEliminarRelacionObjetoRQType  quitarRequestType = new WSEliminarRelacionObjetoStub.WSEliminarRelacionObjetoRQType();
			WSEliminarRelacionObjetoStub.WSEliminarRelacionObjetoRS quitarResponse = new WSEliminarRelacionObjetoStub.WSEliminarRelacionObjetoRS();
			
			// establecemos los parametros de la invocacion 
			quitarRequestType.setDataset(NombreDataset);
			quitarRequestType.setTabla(NombreTabla);
			quitarRequestType.setCodigoRegistro(ValorCampoClave);
			quitarRequestType.setRelacion(NombresCamposAct);
			
			quitarRequest.setWSEliminarRelacionObjetoRQ(quitarRequestType);
			
			quitar._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
			
			quitarResponse = quitar.eliminarRelacionObjeto(quitarRequest);
			
			respuesta_array[0] = quitarResponse.getWSEliminarRelacionObjetoRS().getCodigoRespuesta();
			respuesta_array[1] = quitarResponse.getWSEliminarRelacionObjetoRS().getDescripcionError();
			
			return respuesta_array;
			
		} catch (Exception e) {
			errorWS = e.getMessage();
		}
		
		respuesta_array[0]="KO";
		respuesta_array[1]= errorWS;
		
		return respuesta_array;
	
	}
	
	/*** fin eliminarRelacion ***/
	private static String colocarMascara(String valor) {
		return valor==null? "":valor;
	}
	
}   

