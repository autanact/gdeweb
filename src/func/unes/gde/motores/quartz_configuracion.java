/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: quartz_configuracion.java
##	Contenido: Clase que contiene la implementaci�n de la clase  quartz_configuracion
##            (Clase que es invocada por el Listener de Quartz para lanzar los motores de manera autom�tica)
##	Autor: Freddy Molina
##  Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
package func.unes.gde.motores;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;

import javax.servlet.ServletContext;

import org.quartz.SchedulerException;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.motores.AlmacenamientoHistoricoTask;
import co.net.une.www.motores.GeneradorLotesTimerTask;
import co.net.une.www.motores.GeoreferenciaMasivaTask;


/**
 * Clase que es invocada por el Listener de Quartz para lanzar los motores de manera autom�tica
 * @author FreddyMolina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class quartz_configuracion {
    
    //obtengo las configuraciones iniciales
    //indica si los motores se van a inicializar. 1 se activan / 0 desactivan
    private static final String  ejecutarJobs = ServiciosUtil.getJobActivos();


	/**
	 * M�todo que inicializa los motores tiempo despu�s de iniciado el Web Application Server
	 * @param segundos
	 * 			segundos de espera antes de iniciar los motores
	 * @param usuario
	 * 			usuario configurado para ejecutar los motores
	 * @param servletContext 
	 * 			contexto del servlet
	 * @throws SchedulerException 
	 * 			Si existe algun problema dentro de proceso, se dispara una excepci�n
	 */
	public static void inicializarMotores(int segundos, String usuario, ServletContext servletContext) throws SchedulerException {
		
		//timer de ejecuci�n
	    Timer timer = new Timer();
	    
	    //se elimina del contexto variable de control para el motor de generaci�n de lotes
	    servletContext.removeAttribute("motorLoteRep");
	    //se elimina del contexto variable de control para el motor de georreferencia autom�tica
	    servletContext.removeAttribute("motorGeoRep");
	  //se elimina del contexto variable de control para el motor de generaci�n de hist�rico
	    servletContext.removeAttribute("motorHisRep");
	    //se inicialoza en el contexto variable de control para el motor de generaci�n de lotes. 0 por defecto
    	servletContext.setAttribute("motorLoteRep", "0");
    	//se inicialoza en el contexto variable de control para el motor de georreferencia autom�tica. 0 por defecto
    	servletContext.setAttribute("motorGeoRep", "0");
    	//se inicialoza en el contexto variable de control para el motor de generaci�n de hist�rico. 0 por defecto
    	servletContext.setAttribute("motorHisRep", "0");
	    
	    //se crean las instancias de las tareas
	    AlmacenamientoHistoricoTask historicoTask = new AlmacenamientoHistoricoTask(servletContext);
	    GeoreferenciaMasivaTask geoTask = new GeoreferenciaMasivaTask(servletContext);
	    GeneradorLotesTimerTask lotesTask = new GeneradorLotesTimerTask(usuario,ServiciosUtil.getDirectorioGuardarLote(),servletContext );
	    
	    
	    //Obtengo la hora actual
	    Date now = new Date();
	    Calendar c = Calendar.getInstance();
	    c.setTime(now); 
	    
	    //Retraso la ejecuci�n del inicio de los jobs hasta que termine de levantar el JBOSS     
	    c.add(Calendar.SECOND,segundos);
	    
	    //Agendo el motor de Almacenamiento hist�rico
	    timer.schedule(historicoTask, c.getTime());
	    System.out.println("Configuro el motor de historico");
	    //Agendo el motor de georreferencia
	    System.out.println("Configuro el motor de georeferencia");
	    timer.schedule(geoTask, c.getTime());
	    //Agendo el motor de generaci�n de lotes
	    System.out.println("Configuro el motor de almacenamiento de lotes");
	    timer.schedule(lotesTask, c.getTime());
		
	}
	
	/**
	 * M�todo que agenda los motores en caso de estar activos
	 * @param segundosEspera
	 * 			Segundos que tienen que esperar los motores antes de ejecutarse la primera vez
	 * @param usuario
	 * 			Usuario que ejecuta los motores
	 * @param servletContext
	 * 			contexto del servlet
	 */
	public static void agendarJobs(int segundosEspera, String usuario, ServletContext servletContext){
		
		System.out.println("********** Programando el Job!... **********");
    	System.out.println("Ejecutar JOB:"+ejecutarJobs);
    	@SuppressWarnings("unused")
		AlmacenamientoHistoricoTask historicoTask = new AlmacenamientoHistoricoTask(servletContext);
    	System.out.println("**********");
    	//valido si los JOBS esta configurados. 1 se activan / 0 desactivan
    	if ("1".equals(ejecutarJobs)){
    		
    		//Los JOBS estan configurados y se tienen que ejecutar
    		
    		//Se toman las configuraciones 
    		try {
    			
    			//Se inicializan los JOBS
				inicializarMotores(segundosEspera,usuario,servletContext);
				
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (SchedulerException e) {
				e.printStackTrace();
			}
    		
    	}
    	
    	System.out.println("********** Termin� Configuraci�n de Jobs!... **********");
		
	}
    
}
