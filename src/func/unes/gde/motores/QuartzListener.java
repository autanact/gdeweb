package func.unes.gde.motores;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: QuartzListener.java
##	Contenido: Clase que implementa el listener que controla y configura los motores al inicio del servidor JBOSS
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;



import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import co.net.une.ejb43.util.ServiciosUtil;

/**
 * Clase que implementa el listener que controla y configura los motores al inicio del servidor JBOSS
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class QuartzListener implements ServletContextListener {
	
		//variable de configuraci�n de schedules
        Scheduler scheduler = null;
        
        
        //obtengo las configuraciones iniciales configuraci�n de JOBS
        
        private static final String  segEspera = ServiciosUtil.getTiempoEsperaActivarJobs();
        private static final String  jobUsuario = ServiciosUtil.getJobUsuario();
        
        
        
        @Override
        public void contextInitialized(ServletContextEvent servletContext) {
        	
        	//cuando se inicializa el Quarts, se incluyen los motores y se configura su ejecuci�n
        	quartz_configuracion.agendarJobs(Integer.parseInt(segEspera),jobUsuario,servletContext.getServletContext());
        }

        @Override
        public void contextDestroyed(ServletContextEvent servletContext) {
        	try {
        		// se apaga el scheduler
        		scheduler.shutdown();
        	}catch (SchedulerException e){
        		e.printStackTrace();
        	}
        }
}