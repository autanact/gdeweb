package func.unes.gde.motores;

/***********************************************************************************************************************************
 ##	Empresa: Autana CT
 ##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
 ##	Archivo: QuartzJob.java
 ##	Contenido: Clase que implementa la publicaci�n de lotes
 ##	Autor: Freddy Molina
 ## Fecha creaci�n: 02-02-2016
 ##	Fecha �ltima modificaci�n: 02-02-2016
 ##	Historial de cambios:  
 ##		02-02-2016 FMS Primera versi�n
 ##**********************************************************************************************************************************
 */
import org.apache.axis2.AxisFault;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import co.net.une.www.util.GDEException;
import func.unes.gde.Publicar;

/**
 * Clase implementa la publicaci�n de lotes
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class QuartzJob implements Job {

	public void execute(JobExecutionContext context) throws JobExecutionException {

		// Captura el pase de parametros hechos al construir el Job en programar_job
		JobDataMap jdMap = context.getJobDetail().getJobDataMap();
		String usuario = jdMap.get("usuario").toString();
		String path = jdMap.get("path").toString();

		try {
			// Ejecutar el Proceso de Publicacion
			Publicar p = new Publicar();
			p.publicarDirExc(usuario, path);

		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GDEException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
