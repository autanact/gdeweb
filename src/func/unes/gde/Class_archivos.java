package func.unes.gde;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: Class_archivos.java
##	Contenido: Clase que implementa el control de archivos CSV para un usuario en particular
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/


/**
 * Clase que implementa el control de archivos CSV para un usuario en particular
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class Class_archivos {
	

	/**
	 * Funci�n que retorna la cantidad de archivos de usuario por persona
	 * @param usuario
	 * 			usuario due�o de los archivos CSV
	 * @param directorio
	 * 			directorio donde se encuentran los archivos CSV
	 * @return
	 * 		retorna la cantidad de archivos de usuario por persona
	 */
	public static int csv_usuario ( String usuario, String directorio ) {

		//ubico el directorio
		java.io.File dir = new java.io.File(directorio);
		//inicializo el contador de archivos
		int cantidad_csv = 0;

		//obtengo la cantidad de archivos
		String[] list = dir.list();
		//Si hay archivos
		if (list.length > 0) {
			//Por cada archivo encontrado
			for (int i = 0; i < list.length; i++) {
				// IDENTIFICAR LOS ARCHIVOS CSV DE USURARIO ACTUAL
				if (list[i].indexOf("csv")>=0 ) {
					//Si el archivo encontrado pertenece al usuario
					if (list[i].indexOf(usuario)>=0 ) {
						//incremento el contador de archivos
						cantidad_csv++;
					}
				}

			}
		}
		//retorno la cantidad de archivos encontrados
		return cantidad_csv;
	}
	
	
}   

