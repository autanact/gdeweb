package func.unes.gde;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: Rurales.java
##	Contenido: Clase que implementa el procesamiento de las direcciones excepcionadas de tipo rural
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.net.une.www.bean.Direccion;
import co.net.une.www.bean.DireccionExcepcionada;
import co.net.une.www.svc.WSConsultaEspacialGDEStub;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQ;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRQType;
import co.net.une.www.svc.WSGeorreferenciarCRServiceStub.WSGeorreferenciarCRRS;

/**
 * Clase que implementa el procesamiento de las direcciones excepcionadas de tipo rural
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class Rurales {

	/**
	 * Funci�n que procesa las direcciones excepcionales de tipo rurales
	 * @param usuario
	 * 			usuario que ejecuta el procesamiento de las rurales
	 * @param ruta
	 * 			ruta donde se guarda el archivo procesado
	 * @return
	 * 		Verdadero si el proceso se ejecut� satisfactoriamente. Falso en otro caso
	 * @throws org.apache.axis2.AxisFault
	 */
	/**
	 * @param usuario
	 * @param ruta
	 * @return
	 * @throws org.apache.axis2.AxisFault
	 */
	@SuppressWarnings("unchecked")
	public static boolean Procesar(String usuario, String ruta)	throws org.apache.axis2.AxisFault {
		boolean valorRetorno = true;
		String rutaArchivo, linea;
		BufferedReader archivoLectura=null;
		ArrayList<String> respuesta = new ArrayList<String>();
		
		// variable que contiene los campos obtenidos de una l�nea del archivo .csv
		String[] campo = new String[8];
		String[] respuesta2 = new String[3];
		String[] respEspacial = new String[10];
		String[] respConsulta1 = new String[3];
		String[] respConsulta2 = new String[3];
		String[] respInsertar = new String[2];
		String[] respActualizar = new String[2];
		String[] respuestaGeo = new String[8];
		String[] direxc = new String[12];
		String[] values2 = null;

		int codigoRespuesta = 0;
		int registros = 2;
		int direccionNormalizada = 4;
		int estadoGeor = 5;
		int placa = 6;
		int agregado = 7;

		int codigosolicitud = 0;
		int departamento = 1;
		int municipio = 2;
		int direccionnatural = 3;
		int latitud = 6;
		int longitud = 7;

		int rural = 4;

		String nombreTabla = null;
		String nombreDataset = null;
		String nombresCampos = null;
		String nombreCamposResultado = null;
		String valoresCampos = null;
		String camposClave = null;

		// Cocatenar la ruta con el nombre del archivo m�s reciente existente
		rutaArchivo = ruta.concat("/" + obtenerArchivo(usuario, ruta));
		try {

			archivoLectura = new BufferedReader(new FileReader(rutaArchivo));
			//Leo el archivo existente
			while ((linea = archivoLectura.readLine()) != null) {
				try {

					if (!linea.startsWith("Codigo_Solicitud[|]Departamento")) {
						linea = linea.replaceAll("\"", "").replaceAll(";", ",");
						campo = linea.split("\\[\\|\\]", -1);
	
	
						// Validacion de los valores para las variables latitud y longitud.
						if (campo[latitud] == null	|| Float.parseFloat(campo[latitud]) < 1
								|| Float.parseFloat(campo[latitud]) > 12
								|| !hasLess16Decimals(Float.parseFloat(campo[latitud]))) {
						}
						if (campo[longitud] == null
								|| Float.parseFloat(campo[longitud]) < -78
								|| Float.parseFloat(campo[longitud]) > -72
								|| !hasLess16Decimals(Float.parseFloat(campo[longitud]))) {
						}
	
						nombreTabla = "direccion_excepcionada";
						nombreDataset = "une_st";
						nombresCampos = "codigo_solicitud";
						nombreCamposResultado = "codigo_solicitud|codigo_sistema|codigo_direccion|direccion_texto_libre|direccion_normalizada|estado_geor_type|fecha_ingreso_direccion|fecha_envio_proveedor|fecha_respuesta_proveedor|estado_excepcion_type|estado_gestion_type|comentario|comentario_proveedor|pais|municipio|id";
	
						//ejecuto consulta sobre direcci�n excepcionada
						respConsulta1 = WServicios.consultartabla_cadena(nombreTabla, nombreDataset, nombresCampos,	campo[codigosolicitud], nombreCamposResultado);
	
						//Si la consulta present� alg�n problema
						if ("KO".equals(respConsulta1[0])|| "".equals(respConsulta1[0])|| respConsulta1[0] == null) {
							// cierro el archivo e interrumpo el proceso
							archivoLectura.close();
							return false;
						}
	
						//Se depuran los datos de la consulta
						//Coloco ## para identificar los campos que vienen vac�os
						String camposDireccion=respConsulta1[registros];
						if (camposDireccion.indexOf("]|")> 0){
							String[] reg = camposDireccion.split("\\|");
							camposDireccion=reg[0];
						}
						while(camposDireccion.indexOf(";;")>=0){
							camposDireccion = camposDireccion.replaceAll(";;", "; ;");
						}
						if (camposDireccion.endsWith(";")) camposDireccion = camposDireccion+" ";
						direxc = camposDireccion.replace("[", "").replace("]", "").split(";", -1);
						DireccionExcepcionada dirExc = new DireccionExcepcionada();
	
						//se asignan los valores al objeto direcci�n excepcionada
						dirExc.setCodigoSolicitud(direxc[0]);
						dirExc.setCodigoSistema(direxc[1]);
						dirExc.setCodigoDireccion(direxc[2]);
						dirExc.setDireccionTextoLibre(direxc[3]);
						dirExc.setDireccionNormalizada(direxc[4]);
						dirExc.setEstadoGeorType(direxc[5]);
						dirExc.setFechaIngresoDireccion(direxc[6]);
						dirExc.setFechaEnvioProveedor(direxc[7]);
						dirExc.setFechaRespuestaProveedor(direxc[8]);
						dirExc.setEstadoExcepcionType(direxc[9]);
						dirExc.setEstadoGestionType(direxc[10]);
						dirExc.setComentario(direxc[11]);
						dirExc.setComentarioProveedor(direxc[12]);
						dirExc.setPais(direxc[13]);
						dirExc.setMunicipio(direxc[14]);
						dirExc.setId(Integer.parseInt(direxc[15]));
						dirExc.setLatitud(campo[latitud]);
						dirExc.setLongitud(campo[longitud]);
	
						// Se realiza la Consulta espacial.		
						respEspacial = consultaEspacial(campo[latitud],	campo[longitud]);
	
						// Se asignan los valores de la consulta espacial al objeto direcci�n
						Direccion dirEsp = new Direccion();
						dirEsp.setPais(respEspacial[2]);
						dirEsp.setDepartamento(respEspacial[3]);
						dirEsp.setMunicipio(respEspacial[4]);
						dirEsp.setCodigoComuna(respEspacial[5]);
						dirEsp.setCodigoBarrio(respEspacial[6]);
						dirEsp.setCodigoDaneManzana(respEspacial[7]);
						dirEsp.setCodigoPredio(respEspacial[8]);
						BigInteger estrat = new BigInteger("".equals(respEspacial[9].trim()) ? "0": respEspacial[9]);
						dirEsp.setEstrato(estrat);
	
						// Si falta el c�digo del municipio
						if (dirEsp.getMunicipio() == null) {
							//cierro el archivo y termino el proceso
							archivoLectura.close();
							return false;
						}
	
						// Si la direcci�n viene con municipio y pa�s
						if (dirEsp.getMunicipio() != null && !"".equals(dirEsp.getPais())) {
							
							
							// Si los municipios son diferentes env�o un error
							if (!dirEsp.getMunicipio().equals(dirExc.getMunicipio())){
								
								// Se actualiza la direccion excepcionada
								String nombreTablaDir = "direccion_excepcionada";
								String nombreDatasetDir = "une_st";
								String camposClaveDir = "codigo_solicitud";
								String nombresCamposDir  = "estado_geor_type|estado_excepcion_type|comentario_proveedor";
								String valoresCamposDir = "X|INEXISTENTE|Direccion Rural con campos Municipio, Departamento � Pa�s que no corresponden con ubicaci�n de las coordenadas";

								respActualizar = WServicios.actualizartabla(nombreTablaDir, nombreDatasetDir,	camposClaveDir, campo[codigosolicitud],nombresCamposDir, valoresCamposDir);

								// cierro el archivo termino el proceso
								archivoLectura.close();
								return false;
																
							}
							
							// Georreferencio la direcci�n
							respuestaGeo = GeorreferenciarCR(campo[departamento],campo[municipio], dirEsp.getPais(),campo[direccionnatural], campo[latitud],
									campo[longitud]);
	
							// Asigno los valores a la direcci�n Georreferenciada dirGeo
							Direccion dirGeo = new Direccion();
							dirGeo.setCodigoDireccion(respuestaGeo[3]);
							dirGeo.setDireccionNormalizada(respuestaGeo[4]);
							dirGeo.setEstadoGeorType(respuestaGeo[5]); 
							dirGeo.setPlaca(respuestaGeo[6]);
							dirGeo.setAgregado(respuestaGeo[7]); 
	
							// Si la consulta present� alg�n problema
							if ("KO".equals(respuestaGeo[0])|| "".equals(respuestaGeo[0])|| respuestaGeo[0] == null) {
								// cierro el archivo termino el proceso
								archivoLectura.close();
								return false;
							}
	
							// variables para pr�ximas consultas
							nombreTabla = "detalle_direccion";
							nombreDataset = "une_st";
							camposClave = "codigo";
							
							// obtengo el c�digo de direcci�n de la direcci�n georreferenciada
							String codDir = dirGeo.getCodigoDireccion();
	
							// Si estado de georrefencia es inv�lido tanto en la direcci�n georreferencia como en la direcci�n excepcionada
							if ("E,C,F,G,J".indexOf(respuestaGeo[estadoGeor]) >= 0	&& "E,C,F,G,J".indexOf(dirExc.getEstadoGeorType()) >= 0) {
	
								// Si la consulta espacial no tiene predio
								if ((dirEsp.getCodigoPredio() == null) || (dirEsp.getCodigoPredio() == "")) {
									// Se cambia el estado de georreferencia a la direcci�n georrefenciada a B
									respuestaGeo[estadoGeor] = "B";
								} else {
									// la consulta espacial tiene predio, se cambia el estado de georreferencia a la direcci�n georrefenciada a Y
									respuestaGeo[estadoGeor] = "Y";
								}
	
								// Consulta a la tabla detalle_direccion.
								valoresCampos = "codigo|estado_geor_type";
	
								// Si la direcci�n georreferenciada no tiene c�digo de direcci�n, se toma el c�digo de direcci�n de la direcci�n excepcionada
								codDir = "".equals(dirGeo.getCodigoDireccion())?dirExc.getCodigoDireccion():dirGeo.getCodigoDireccion();
								
								// Se consulta sobre la tabla detalle_direcci�n
								respConsulta2 = WServicios.consultartabla_cadena(nombreTabla, nombreDataset, camposClave,codDir,valoresCampos);
	
								// Si la consulta present� alg�n problema
								if ("KO".equals(respConsulta2[0])|| "".equals(respConsulta2[0])|| respConsulta2[0] == null) {
									// cierro el archivo termino el proceso
									archivoLectura.close();
									return false;
								}
	
								// Se obtienen y formatean los registros de la consulta de detalle_direcci�n
								respuesta = WServicios.lista_registros(respConsulta2[2]);
	
								// Si no existe la direcci�n en la tabla detalle_direcci�n
								if (respConsulta2[registros].equals("0")) {
									
									// Se inserta en la tabla detalle_direccion.
									nombresCampos = "latitud|longitud|codigo_pais|codigo_departamento|codigo_municipio|codigo_comuna|codigo_barrio|codigo_manzana|codigo_predio|codigo|estrato_type|direccion_anterior|cadena_normalizada|estado_geor_type|numero_placa|agregado_complemento|es_rural";
									valoresCampos = campo[latitud] + "|"+ campo[longitud] + "|"	+ dirEsp.getPais() + "|"+ dirEsp.getDepartamento() + "|"
											+ dirEsp.getMunicipio() + "|"+ colocarMascara(dirEsp.getCodigoComuna()) + "|"+ colocarMascara(dirEsp.getCodigoBarrio()) + "|"
											+ colocarMascara(dirEsp.getCodigoDaneManzana()) + "|"+ colocarMascara(dirEsp.getCodigoPredio()) + "|"+ codDir + "|"
											+ dirEsp.getEstrato().toString() + "|"+ dirExc.getDireccionTextoLibre() + "|"+ respuestaGeo[direccionNormalizada]
											+ "|" + respuestaGeo[estadoGeor] + "|"+ ("".equals(respuestaGeo[placa])?" ":respuestaGeo[placa]) + "|"+ ("".equals(respuestaGeo[agregado])?" ":respuestaGeo[agregado]) + "|SI";
	
									respInsertar = WServicios.insertartabla(nombreTabla, nombreDataset,	nombresCampos, valoresCampos);
	
									// Si se present� alg�n problema en la inserci�n
									if ("KO".equals(respInsertar[0])|| "".equals(respInsertar[0])|| respInsertar[0] == null) {
										// cierro el archivo termino el proceso
										archivoLectura.close();
										return false;
									}
	
									// Se actualiza la direccion excepcionada
									nombreTabla = "direccion_excepcionada";
									nombreDataset = "une_st";
									camposClave = "codigo_solicitud";
									nombresCampos = "codigo_direccion|direccion_normalizada|estado_excepcion_type|estado_geor_type|comentario";
									valoresCampos = codDir+ "|"+ respuestaGeo[direccionNormalizada]+ "|RURAL|"
											+ respuestaGeo[estadoGeor]+ "|Direcci�n rural procesada.";
	
									respActualizar = WServicios.actualizartabla(nombreTabla, nombreDataset,	camposClave, campo[codigosolicitud],nombresCampos, valoresCampos);
	
									// Si la consulta present� alg�n problema
									if ("KO".equals(respActualizar[0]) || "".equals(respActualizar[0])|| respActualizar[0] == null) {
										// cierro el archivo termino el proceso
										archivoLectura.close();
										return false;
									}
	
									// Relaciono con la tabla direcci�n
									WServicios.relacionarObjetos("une_st","direccion_excepcionada",	dirExc.getId() + "","detalle_direccion",codDir + "");
	
	
								} else {
									
									// Existe el registro en la tabla detalle_direcci�n
									
									// formateo los par�metos de la consulta
									String resp1 = (String)respuesta.get(0);
									String[] partes = resp1.split("\\;");
									
									// partes[1]: estado de georreferencia de la direcci�n encontrada
									// Si el estado de georrefencia es inv�lido
									if (("Y, B, M, N").indexOf(partes[1]) < 0) {
										// Se actualiza la tabla detalle_direccion.
										nombresCampos = "latitud|longitud|codigo_comuna|codigo_barrio|codigo_manzana|codigo_predio|estrato_type|direccion_anterior|cadena_normalizada|estado_geor_type|numero_placa|agregado_complemento|es_rural";
										valoresCampos = campo[latitud]+ "|"	+ campo[longitud]+ "|"+  colocarMascara(dirEsp.getCodigoComuna())
												+ "|"+ colocarMascara(dirEsp.getCodigoBarrio())	+ "|"+ colocarMascara(dirEsp.getCodigoDaneManzana())+ "|"
												+ colocarMascara(dirEsp.getCodigoPredio())+ "|"+ dirEsp.getEstrato().toString() + "|"+ dirExc.getDireccionTextoLibre()
												+ "|"+ respuestaGeo[direccionNormalizada]	+ "|" + respuestaGeo[estadoGeor]
												+ "|" + colocarMascara(respuestaGeo[placa]) + "|"+ colocarMascara(respuestaGeo[agregado]) + "|SI";
	
										respActualizar = WServicios.actualizartabla(nombreTabla,nombreDataset,	camposClave,dirGeo.getCodigoDireccion(),nombresCampos, valoresCampos);
	
										// Si fall� la actualizaci�n
										if ("KO".equals(respActualizar[0])|| "".equals(respActualizar[0]) || respActualizar[0] == null) {
											// cierro el archivo termino el proceso
											archivoLectura.close();
											return false;
										}
	
										// Actualizar direccion excepcionada
										nombreTabla = "direccion_excepcionada";
										nombreDataset = "une_st";
										camposClave = "codigo_solicitud";
										nombresCampos = "direccion_normalizada|estado_excepcion_type|estado_geor_type|comentario";
										valoresCampos = respuestaGeo[direccionNormalizada]+ "|RURAL|" + respuestaGeo[estadoGeor]	+ "|Direcci�n rural NO procesada.";
	
										respActualizar = WServicios.actualizartabla(nombreTabla,nombreDataset, camposClave,campo[codigosolicitud],nombresCampos,valoresCampos);
	
										// Si fall� la actualizaci�n
										if ("KO".equals(respActualizar[0])|| "".equals(respActualizar[0])|| respActualizar[0] == null) {
											// cierro el archivo termino el proceso
											archivoLectura.close();
											return false;
										}
										
										// Relaciono con la tabla direcci�n
										WServicios.relacionarObjetos("une_st","direccion_excepcionada",	dirExc.getId() + "","detalle_direccion",codDir);
										
									} // Fin Condicional ("Y, B, M, N").indexOf((String) respuesta.get(3)) < 0
								}
	
							} // "E,C,F,G,J".indexOf(respuestaGeo[estadoGeor]) >= 0	&& "E,C,F,G,J".indexOf(dirExc.getEstadoGeorType()) >= 0 
	
							// Obtengo los datos de la direcci�n de c�digo direcci�n "codDir"
							respuesta2 = WServicios.consultartabla_cadena("detalle_direccion","une_st", "codigo",codDir,
											"codigo_pais|codigo_departamento|codigo_municipio|estrato_type|es_rural");
							
							// Si el proceso funcion� correctamente
							if (respuesta2[codigoRespuesta].equals("OK")) {
								
								// formateo los datos
								respuesta2[registros] = respuesta2[registros].replace("[", "");
								respuesta2[registros] = respuesta2[registros].replace("]", "");
								values2 = respuesta2[registros].split("\\;", -1);
							} else {
								// La consulta present� alg�n problema
								// cierro el archivo termino el proceso
								archivoLectura.close();
								return false;
							}
							
							// Si estado de georrefencia de la direcci�n georrefenciada es inv�lido
							if ("E,C,F,G,J".indexOf(respuestaGeo[estadoGeor]) < 0){
								
								// Env�o a RespuestaDE (Biztalk/CRM)
								DireccionExcepcionada dirExcMod = new DireccionExcepcionada();
								dirExcMod.obtenerDirExcPorCodigoSolicitud(campo[codigosolicitud]);
								Direccion detDir = new Direccion();
								detDir.obtenerDetalleDirPorCodigo(codDir);
								
								String respCRM[] = WServicios.respuestaDE(campo[codigosolicitud],dirExcMod.getCodigoSistema(),	dirExcMod.getEstadoExcepcionType(),
												dirExcMod.getCodigoDireccion(),dirExcMod.getDireccionNormalizada(),dirExcMod.getEstrato()==null?"0":dirExcMod.getEstrato().toString(),
												colocarMascara(values2, rural),	dirExcMod.getEstadoGeorType(),	dirExcMod.getPais(),
												dirExcMod.getMunicipio(),	dirExcMod.getDepartamento(),campo[latitud],campo[longitud],dirExcMod,detDir);
								
								//Si respuesta de CRM es OK se coloca en estatus reportada
								if ("OK".equals(respCRM[0])){

									// Actualizar direccion_excepcionada
									nombreTabla = "direccion_excepcionada";
									nombreDataset = "une_st";
									camposClave = "codigo_solicitud";
									nombresCampos = "estado_excepcion_type";
									valoresCampos = "PUBLICADA CRM";
			
									respActualizar = WServicios.actualizartabla(nombreTabla, nombreDataset, camposClave,campo[codigosolicitud], nombresCampos,valoresCampos);

								}
							}
	
						} else {
							// la direcci�n no se pudo ubicar geoespacialmente
							
							// Actualizar direccion_excepcionada
							nombreTabla = "direccion_excepcionada";
							nombreDataset = "une_st";
							camposClave = "codigo_solicitud";
							nombresCampos = "estado_geor_type|estado_excepcion_type|comentario_proveedor";
							valoresCampos = "X|INEXISTENTE|Direccion Rural con campos Municipio, Departamento � Pa�s que no corresponden con ubicaci�n de las coordenadas";
	
							respActualizar = WServicios.actualizartabla(nombreTabla, nombreDataset, camposClave,campo[codigosolicitud], nombresCampos,valoresCampos);
	
							// Si fall� la actualizaci�n
							if ("KO".equals(respActualizar[0])|| "".equals(respActualizar[0])|| respActualizar[0] == null) {
								// cierro el archivo termino el proceso
								archivoLectura.close();
								return false;
							}
						}
					}
				}catch (Exception e){
					e.printStackTrace();
				}// Condicional para omitir la linea de cabecera.
			}// Ciclo para leer cada linea del archivo.

			// retorno la respuesta del proceso
			return valorRetorno;
		} catch (IOException e) {
			e.printStackTrace();
			valorRetorno = false;
		} catch (Exception e) {
			e.printStackTrace();
			valorRetorno = false;
		}finally {
			// Cierre del archivo
			try {
				archivoLectura.close();
			} catch (IOException e) {}

			// Borrado del archivo.
			if( borrarArchivo(rutaArchivo) ) {
				//System.out.println("***** Se ha eliminado el archivo de forma exitosa al procesar rurales. *****");
			} else {
				//System.out.println("***** ERROR: El archivo al procesar rurales NO se pudo eliminar. *****");
			}
		}
		return valorRetorno;
	}

	
	/**
	 * Funci�n que elimina el archivo cvs luego de ser procesado
	 * @param ruta
	 * 			archivo a borrar
	 * @return
	 * 		verdadero si pudo eliminar el archivo, falso en cualquier otro caso
	 */
	private static boolean borrarArchivo(String ruta) {
		File arch = new File(ruta);
		return arch.delete();
	}
	
	/**
	 * Funci�n que retorna el nombre del archivo m�s reciente, seg�n el directorio y el usuario enviado
	 * @param usuario
	 * 			usuario que ejecuta el procesamiento y due�o del archivo
	 * @param ruta
	 * 			directorio donde se va a guardar el archivo
	 * @return nombreArchivo
	 */
	public static String obtenerArchivo(final String usuario, String ruta) {
		// Definicion del filtro por extension y usuario.
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String fileName) {
				return (fileName.endsWith("csv") && fileName
						.startsWith(usuario));
			}
		};
		
		// aplico el filtro al directorio enviado por par�metro para obtener la lista de archivos v�lidos
		File f = new File(ruta);
		String[] fileList = f.list(filter);
		int indiceMayor = 0;
		
		SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
		Date fechaEvaluada, fechaUno;

		try {
			fechaUno = formateador.parse(fileList[0].substring(fileList[0].indexOf('_') + 1,fileList[0].lastIndexOf('_')));
			// Determina entre todos los archivos del directorio aquel que tenga la fecha mas reciente.
			for (int i = 1; i < fileList.length; i++) {
				fechaEvaluada = formateador.parse(fileList[i].substring(fileList[i].indexOf('_') + 1,fileList[i].lastIndexOf('_')));
				if (fechaEvaluada.compareTo(fechaUno) > 0)
					indiceMayor = i;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		// Retorna el nombre del archivo con fecha mas reciente
		return fileList[indiceMayor];
	}


	/**
	 * Funci�n que retorna un arreglo con los datos de la direcci�n georreferenciada
	 * @param codigoDepartamento
	 * 			c�digo del departamento
	 * @param codigoMunicipio
	 * 			c�digo del municipio
	 * @param codigoPais
	 * 			c�digo del pa�s
	 * @param direccionNatural
	 * 			direcci�n natural
	 * @param latitud
	 * 			latitud
	 * @param longitud
	 * 			longitud
	 * @return
	 * 		Arreglo con los datos de la direcci�n georreferenciada <br/>
	 * 		respuesta_array[0] = C�digo de respuesta del proceso <br/>
	 *		respuesta_array[1] = C�digo de Error del proceso <br/>
	 *		respuesta_array[2] = Descripci�n del Error <br/>
	 *		respuesta_array[3] = C�digo de Direccion <br/>
	 *		respuesta_array[4] = Direccion Normalizada <br/>
	 *		respuesta_array[5] = Estado de Georeferenciacion <br/>
	 *		respuesta_array[6] = Placa <br/>
	 *		respuesta_array[7] = Agregados 
	 * @throws org.apache.axis2.AxisFault
	 * 		Si falla la comunicaci�n a trav�s del servicio de Georreferencia, lanza una excepci�n
	 */
	public static String[] GeorreferenciarCR(String codigoDepartamento,	String codigoMunicipio, String codigoPais, String direccionNatural,
			String latitud, String longitud) throws org.apache.axis2.AxisFault {
		// variable de respuesta
		String[] respuesta_array = new String[8];
		try {
			
			//Configuro la invocaci�n del servicio de Georreferencia
			WSGeorreferenciarCRServiceStub geoProxy = new WSGeorreferenciarCRServiceStub();
			WSGeorreferenciarCRRQ geoRequest = new WSGeorreferenciarCRRQ();
			WSGeorreferenciarCRRQType geoRequestType = new WSGeorreferenciarCRRQType();
			WSGeorreferenciarCRRS geoResponse = new WSGeorreferenciarCRRS();

			// Asigno los valores
			geoRequestType.setCodigoDepartamento(codigoDepartamento);
			geoRequestType.setCodigoMunicipio(codigoMunicipio);
			geoRequestType.setCodigoPais(codigoPais);
			geoRequestType.setDireccionNatural(direccionNatural);
			geoRequestType.setLatitud(latitud);
			geoRequestType.setLongitud(longitud);
			geoRequestType.setUNE_Cobertura_Especial(0);

			geoRequest.setWSGeorreferenciarCRRQ(geoRequestType);
			
			// Me conecto al servicio de Georreferencia
			geoProxy._getServiceClient()
					.getOptions()
					.setProperty(
							org.apache.axis2.transport.http.HTTPConstants.CHUNKED,
							Boolean.FALSE);

			geoResponse = geoProxy.georeferenciarCR(geoRequest);

			// Obtengo los datos y armo la respuesta
			respuesta_array[0] = geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getCodigoRespuesta();
			respuesta_array[1] = geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getCodigoError();
			respuesta_array[2] = geoResponse.getWSGeorreferenciarCRRS().getGisRespuestaProceso().getDescripcionError();
			respuesta_array[3] = geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getCodigoDireccion();
			respuesta_array[4] = geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getDireccionNormalizada();
			respuesta_array[5] = geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getEstadoGeoreferenciacion();
			respuesta_array[6] = geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getPlaca();
			respuesta_array[7] = geoResponse.getWSGeorreferenciarCRRS().getGisCommonInfoDir().getAgregado();

			// Retorno los datos
			return respuesta_array;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return respuesta_array;
	}


	/**
	 * Funci�n que realiza una consulta espacial a partir de la latitud y longitud
	 * @param latitud
	 * 			latitud del objeto a ubicar
	 * @param longitud
	 * 			longitud del objeto a ubicar
	 * @return
	 * 		Arreglo con los datos de la consulta espacial <br/>
	 * 		respuestaServicio[0] = Codigo de Respuesta del proceso<br/>
	 *		respuestaServicio[1] = Descripcion del Error del proceso<br/>
	 *		respuestaServicio[2] = C�digo del Pa�s<br/>
	 *		respuestaServicio[3] = C�digo del Departamento<br/>
	 *		respuestaServicio[4] = C�digo del Municipio<br/>
	 *		respuestaServicio[5] = C�digo de la Comuna<br/>
	 *		respuestaServicio[6] = C�digo del Barrio<br/>
	 *		respuestaServicio[7] = C�digo de la Manzana<br/>
	 *		respuestaServicio[8] = C�digo Predio<br/>
	 *		respuestaServicio[9] = Estrato Type
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] consultaEspacial(String latitud, String longitud) throws org.apache.axis2.AxisFault {
		String errorWS = "";
		String[] respuestaServicio = new String[10];
		try {
			// Soporte y peticion del servicio web
			WSConsultaEspacialGDEStub consultaEsp = new WSConsultaEspacialGDEStub();
			WSConsultaEspacialGDEStub.WSConsultaEspacialGDERQ consultaEspRequest = new WSConsultaEspacialGDEStub.WSConsultaEspacialGDERQ();
			WSConsultaEspacialGDEStub.WSConsultaEspacialGDERQType consultaEspRequestType = new WSConsultaEspacialGDEStub.WSConsultaEspacialGDERQType();
			WSConsultaEspacialGDEStub.WSConsultaEspacialGDERS consultaEspResponse = new WSConsultaEspacialGDEStub.WSConsultaEspacialGDERS();

			// Se establecen los parametros de la invocacion
			consultaEspRequestType.setLatitud(latitud);
			consultaEspRequestType.setLongitud(longitud);

			// Invocamos al web service
			consultaEspRequest
					.setWSConsultaEspacialGDERQ(consultaEspRequestType);
			consultaEsp
					._getServiceClient()
					.getOptions()
					.setProperty(
							org.apache.axis2.transport.http.HTTPConstants.CHUNKED,
							Boolean.FALSE);
			consultaEspResponse = consultaEsp
					.consultaEspacial(consultaEspRequest);

			respuestaServicio[0] = consultaEspResponse.getWSConsultaEspacialGDERS().getGisRespuestaProceso().getCodigoRespuesta();
			respuestaServicio[1] = consultaEspResponse.getWSConsultaEspacialGDERS().getGisRespuestaProceso().getDescripcionError();
			respuestaServicio[2] = consultaEspResponse.getWSConsultaEspacialGDERS().getCodigoPais();
			respuestaServicio[3] = consultaEspResponse.getWSConsultaEspacialGDERS().getCodigoDepartamento();
			respuestaServicio[4] = consultaEspResponse.getWSConsultaEspacialGDERS().getCodigoMunicipio();
			respuestaServicio[5] = consultaEspResponse.getWSConsultaEspacialGDERS().getCodigoComuna();
			respuestaServicio[6] = consultaEspResponse.getWSConsultaEspacialGDERS().getCodigoBarrio();
			respuestaServicio[7] = consultaEspResponse.getWSConsultaEspacialGDERS().getCodigoManzana();
			respuestaServicio[8] = consultaEspResponse.getWSConsultaEspacialGDERS().getCodigoPredio();
			respuestaServicio[9] = consultaEspResponse.getWSConsultaEspacialGDERS().getEstratoType();

			return respuestaServicio;

		} catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
			respuestaServicio[0] = "KO";
			respuestaServicio[1] = errorWS;
		}

		return respuestaServicio;
	}


	/**
	 * Funci�n que valida que la latitud y la longitud sean de formato v�lido
	 * @param value
	 * 			latitud o longitud
	 * @return
	 * 		Verdadero si cumple con el formato, falso en otro caso
	 */
	private static boolean hasLess16Decimals(Float value) {
		
		// formato v�lido
		DecimalFormat df = new DecimalFormat("##.###############");
		try {
			df.format(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Funci�n que obtiene el valor del arreglo en la posici�n, en caso que la posici�n exceda la longitud, devuelve vaci�
	 * @param arreglo
	 * 			Arreglo de par�metros
	 * @param posicion
	 * 			posici�n del elemento a obtener
	 * @return
	 * 		valor del arreglo en la posici�n, en caso que la posici�n exceda la longitud, devuelve vaci�
	 */
	private static String colocarMascara(String[] arreglo, int posicion) {
		String resp = " ";
		try {
			resp = arreglo[posicion];
		} catch (Exception e) {
		}
		return resp;
	}
	
	/**
	 * Funci�n que coloca un espacio en blanco cuando el valor es vac�o
	 * @param campo
	 * 			valor a validar
	 * @return
	 * 		espacio en blanco cuando el valor es vac�o
	 */
	private static String colocarMascara(String campo) {
		String resp = " ";
		try {
			resp = "".equals(campo)?" ":campo;
		} catch (Exception e) {
		}
		return resp;
	}
}
