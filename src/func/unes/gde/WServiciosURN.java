package func.unes.gde;
/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: WServiciosURN.java
##	Contenido: Clase que implementa la comunicaci�n a trav�s de los servicios WSConsultarURNS para GDE
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/

import java.rmi.*;

import co.net.une.ejb43.util.ServiciosUtil;
import co.net.une.www.svc.*;

/**
 * Clase que implementa la comunicaci�n a trav�s de los servicios WSConsultarURNS para GDE
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class WServiciosURN {


	/**
	 * Funci�n que retorna arreglo de registros acotados por la cantidad de elementos a mostrar y el n�mero del bloque (p�gina) a mostrar
	 * @param tamano_pagina
	 * 			N�mero de registros a mostrar
	 * @param pagina
	 * 			N�mero de p�gina
	 * @param NombreTabla
	 * 			nombre de la tabla
	 * @param NombreDataset
	 * 			nombre del dataset
	 * @param NombresCamposConsulta
	 * 			listado de nombres de los campos a filtrar
	 * @param ValoresCamposConsulta
	 * 			listado de los valores a filtrar
	 * @param NombreCamposResultado
	 * 			listado de nombres de los campos a mostrar
	 * @return
	 * 		Arreglo con la respuesta del servicio<br/>
	 * 		respuesta_array[0] = C�digo Respuesta (OK|KO)<br/>
	 * 		respuesta_array[1] = Descripci�n del error<br/>
	 * 	 	respuesta_array[2] = Listado con los registros de la consulta<br/>
	 *    	respuesta_array[3] = cantidad de registros obtenidos
	 * @throws org.apache.axis2.AxisFault
	 */
	public static String[] consultartabla_URN(int tamano_pagina, int pagina, String NombreTabla, String NombreDataset,	String NombresCamposConsulta, 
			String ValoresCamposConsulta,String NombreCamposResultado) throws org.apache.axis2.AxisFault {
		String errorWS = "";
		String totalResultados = "0";
		String respuesta_array[] = new String[4];
		String registros_respuesta = "";
		long timeout = ((Integer.parseInt(ServiciosUtil.getTimeOutServicios()) * 60) * 1000);
		try {
			// Llamado al WS ConsultarURN
			WSConsultarURNStub proxy = new WSConsultarURNStub();
			WSConsultarURNStub.WSConsultarURNRQ request = new WSConsultarURNStub.WSConsultarURNRQ();
			WSConsultarURNStub.WSConsultarURNRS response = new WSConsultarURNStub.WSConsultarURNRS();

			request.setWSConsultarURNRQ(new WSConsultarURNStub.WSConsultarURNRQType());
			request.getWSConsultarURNRQ().setNombreDataset(NombreDataset);
			request.getWSConsultarURNRQ().setNombreTabla(NombreTabla);
			request.getWSConsultarURNRQ().setNombresCamposConsulta(NombresCamposConsulta);
			request.getWSConsultarURNRQ().setValoresCamposConsulta(ValoresCamposConsulta);

			proxy._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);

			proxy._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED,Boolean.FALSE);

			response = proxy.consultarURN(request);

			String resultados[] = response.getWSConsultarURNRS().getResultados();

			totalResultados = response.getWSConsultarURNRS().getTotalResultados();

			// Valida si existen registros
			if (!totalResultados.equals("0")) {
				// tamano_pagina=0 lista todos los resulatos, Si
				if (tamano_pagina == 0) {
					tamano_pagina = Integer.parseInt(totalResultados);
				}
				// Calcular el tamano del Array de URN
				int array_tamano = tamano_pagina;

				if (((resultados.length - tamano_pagina * pagina) + 1) < tamano_pagina) {
					array_tamano = (resultados.length - tamano_pagina * pagina);

				}
				// Inicializar el Array URN
				String[] array_urns = new String[array_tamano];

				// Carga el ARRAY URN con los valores de la Consulta de URN
				int c = 0;

				for (int p = tamano_pagina * pagina; p < (resultados.length); p++) {

					if (p > ((tamano_pagina * pagina) + tamano_pagina)) {
						break;
					}

					if ((p >= (tamano_pagina * pagina))	&& (p < (tamano_pagina * pagina) + tamano_pagina)) {

						array_urns[c] = resultados[p];
						c++;

					}

				}

				try {
					// Llamado al WS ObtenerObjetosURN

					WSObtenerObjetosURNStub obtener = new WSObtenerObjetosURNStub();
					WSObtenerObjetosURNStub.WSObtenerObjetosURNRQ obtenerRequest = new WSObtenerObjetosURNStub.WSObtenerObjetosURNRQ();
					WSObtenerObjetosURNStub.WSObtenerObjetosURNRS obtenerResponse = new WSObtenerObjetosURNStub.WSObtenerObjetosURNRS();
					WSObtenerObjetosURNStub.Urns urns = new WSObtenerObjetosURNStub.Urns();

					obtenerRequest.setWSObtenerObjetosURNRQ(new WSObtenerObjetosURNStub.WSObtenerObjetosURNRQType());

					urns.setUrn(array_urns);

					obtenerRequest.getWSObtenerObjetosURNRQ().setUrns(urns);

					obtenerRequest.getWSObtenerObjetosURNRQ().setNombreCamposResultado(NombreCamposResultado);

					obtener._getServiceClient().getOptions().setTimeOutInMilliSeconds(timeout);
					obtener._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED,	Boolean.FALSE);

					obtenerResponse = obtener.obtenerObjetosURN(obtenerRequest);

					WSObtenerObjetosURNStub.Resultados obtenerResultados = obtenerResponse.getWSObtenerObjetosURNRS().getResultados();

					if (obtenerResultados != null) {
						WSObtenerObjetosURNStub.Registros[] registros = obtenerResultados.getRegistro();

						String[] campos = NombreCamposResultado.split("\\|", -1);

						for (int i = 0; i < registros.length; i++) {

							WSObtenerObjetosURNStub.Dupla[] atributos = registros[i].getAtributo();
							
							// Recorre las Lista de NombreCamposResultado
							for (int k = 0; k < campos.length; k++) {

								for (int j = 0; j < atributos.length; j++) {

									// Para Mantener el order de
									// NombreCamposResultado
									if (campos[k].equals(atributos[j].getCampo())) {
										registros_respuesta = registros_respuesta+ atributos[j].getValor();
										registros_respuesta = registros_respuesta+ ";";
									}

								}
							}
							if (i < registros.length - 1) {
								registros_respuesta = registros_respuesta + "|";
							}

						}
					}
					// RESPUESTA CON LOS REGISTROS
					respuesta_array[0] = response.getWSConsultarURNRS().getGisRespuestaProceso().getCodigoRespuesta();
					respuesta_array[1] = response.getWSConsultarURNRS().getGisRespuestaProceso().getDescripcionError();

					//Coloco ## para identificar los campos que vienen vac�os
					while(registros_respuesta.indexOf(";;")>=0){
						registros_respuesta = registros_respuesta.replaceAll(";;", "; ;");
					}
					respuesta_array[2] = registros_respuesta;
					respuesta_array[3] = totalResultados;

					return respuesta_array;
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					errorWS = e.getMessage();
				}

			} else {
				// RESPUESTA CON ERRORES
				respuesta_array[0] = response.getWSConsultarURNRS().getGisRespuestaProceso().getCodigoRespuesta();
				respuesta_array[1] = response.getWSConsultarURNRS().getGisRespuestaProceso().getDescripcionError();
				respuesta_array[2] = "0";
				respuesta_array[3] = "0";

				return respuesta_array;
			}

		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorWS = e.getMessage();
		}
		respuesta_array[0] = "KO";
		respuesta_array[1] = errorWS;
		respuesta_array[2] = "0";
		respuesta_array[3] = "0";

		return respuesta_array;
	}



	/**
	 * Funci�n que retorna la cantidad de bloques (p�ginas) de tama�o fijo que se pueden obtener de un conjunto de registros
	 * @param totalResultados
	 * 			total cantidad de registros
	 * @param tamano_pagina
	 * 			tama�o del bloque (p�gina)
	 * @return
	 */
	public static int catidad_paginas(int totalResultados, int tamano_pagina) {
		int cantidad_paginas = 0;

		cantidad_paginas = Math.round(((totalResultados) / tamano_pagina));

		if ((cantidad_paginas * tamano_pagina) < (totalResultados)) {
			cantidad_paginas = cantidad_paginas + 1;
		}
		return cantidad_paginas;
	}

}
