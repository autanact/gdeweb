package func.unes.gde;

/***********************************************************************************************************************************
##	Empresa: Autana CT
##	Proyecto: Sistema de Gesti�n de direcciones excepcionadas (GDE)
##	Archivo: enviarCRM.java
##	Contenido: Clase que implementa la gesti�n de las direcciones excepcionadas que son enviadas a Biztalk/CRM
##	Autor: Freddy Molina
## Fecha creaci�n: 02-02-2016
##	Fecha �ltima modificaci�n: 02-02-2016
##	Historial de cambios:  
##		02-02-2016 FMS Primera versi�n
##**********************************************************************************************************************************
*/
import java.rmi.RemoteException;

import co.net.une.www.bean.Direccion;
import co.net.une.www.bean.DireccionExcepcionada;
import co.net.une.www.util.GDEException;

/**
 * Clase que implementa la gesti�n manual de las direcciones excepcionadas que son enviadas a Biztalk/CRM
 * @author Freddy Molina
 * @creado 02-02-2016
 * @ultimamodificacion 02-02-2016
 * @version 1.0
 * @historial
 * 			02-02-2016 FJM Primera versi�n
 */
public class enviarCRM {
	

	/**
	 * Funci�n que ejecuta la gesti�n de una direcci�n excepcionada de manera manual
	 * @param codigo_solicitud
	 * 			c�digo de solicitud de la direcci�n excepcionada
	 * @return
	 * 		Arreglo con el resultado de la gesti�n del env�o de CRM de la direcci�n excepcionada
	 * 		respuesta_array[0]: [OK|KO] dependiendo si el resultado de la gesti�n culmin� sin problemas
	 * 		respuesta_array[1]: mensaje de error en caso o �xito de la gesti�n
	 * @throws org.apache.axis2.AxisFault
	 * 				Excepci�n en caso de fallar la comunicaci�n con alg�n servicio
	 */
	public static String[] EnviarCRM_Getion_Manual(String codigo_solicitud) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String respuesta_array[] = new String[2];  
		
		
		// Almacena las respuesta de los servicios
	    String respuesta[] = new String[3];
	    String [] respuesta_WSrespuestaDE = new String[2];
	    String[] values = null;
	    
	    // Posicion de valores en respuesta
	    int codigoRespuesta  = 0;
	    int descripcionError = 1;
	    int registros        = 2;
	    int mensaje        	 = 1;
	    
	    String [] respuesta_actualizar = new String[2];
	    	   
	    // Posicion de Valores en Respuesta
	    int codigo_sistema = 0;
	    int estado_gestion_type = 1;
	    int estado_excepcion_type = 2;
	    int codigo_direccion = 3;
	    int direccion_normalizada = 4;
	    int estado_geor_type = 5;
	    int codigo_pais = 6;
	    int codigo_departamento = 7;
	    int codigo_municipio = 8;
	    
	    
		try {
			// Busca los datos requeridos de la direcci�n excepcionada
			respuesta = WServicios.consultartabla_cadena("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"codigo_sistema|estado_gestion_type|estado_excepcion_type|codigo_direccion|direccion_normalizada|estado_geor_type|pais|departamento|municipio");
			System.out.println("Se procesa la direcci�n excepcionada cod_solicitud: "+codigo_solicitud);
			//si no se presenta ning�n error en la consulta
	        if (!respuesta[codigoRespuesta].equals("KO")) {
			
	        	//limpio la informaci�n enviada
	        	respuesta[registros] = respuesta[registros].replace("[", "");
				respuesta[registros] = respuesta[registros].replace("]", "");
			    values = respuesta[registros].split("\\;", -1);
			   
			    // 11.	El bot�n Enviar CRM s�lo debe activarse cuando se seleccione una direcci�n con estado_excepcion_type = �INEXISTENTE� y cuando el campo c�digo_sistema tiene asignado un valor diferente a �MIG�.
		    	// estado_excepcion_type = estado_excepcion_type
			     
			    if (values[estado_excepcion_type].equals("INEXISTENTE") && values[estado_gestion_type].equals("RESUELTA PROVEEDOR")  && !values[codigo_sistema].equals("MIG")  ) {
			   		
			    	System.out.println("La direcci�n es INEXISTENTE, RESUELTA PROVEEDOR y MIG");
			    	
			    	//si la direcci�n tiene estado geo YoB
			    	if ( "YB".indexOf(values[estado_geor_type]) >= 0 ) {
			    		// se actualizar el estado geor type en el detalle direcci�n
			    		System.out.println("Se actualiza detalle_direccion por ser YoB");
			    		respuesta_actualizar =  WServicios.actualizartabla("detalle_direccion","une_st","codigo",values[codigo_direccion],"estado_geor_type","X");
			    	}
			   
			    	// se actualizar el estado geor type en la direcci�n excepcionada
			    	 respuesta_actualizar =  WServicios.actualizartabla("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"estado_geor_type","X");
			   		 
			    	 // si falla alguna de las actualizaciones se interrumpe el proceso
			   		 if ( respuesta_actualizar[codigoRespuesta].equals("KO")) {
			   			 
			   			 //se env�an los mensajes de error
			   			respuesta_array[codigoRespuesta]=respuesta_actualizar[codigoRespuesta];
						respuesta_array[mensaje]=respuesta_actualizar[descripcionError];
						return respuesta_array;
			   		 } else {
			   			 
			   			System.out.println("Se env�a a CRM");
			   			
			   			//obtengo los datos del detalle direcci�n
			   			String respuesta2[] = new String[3];
			   			String values2[] = new String[2];
			   			respuesta2 = WServicios.consultartabla_cadena("detalle_direccion","une_st","codigo",values[codigo_direccion],"latitud|longitud");
					   
			   			//si no falla el proceso limpio el resultado
					    if (respuesta2[codigoRespuesta].equals("OK")) {
					    	respuesta2[registros] = respuesta2[registros].replace("[", "");
							respuesta2[registros] = respuesta2[registros].replace("]", "");
						    values2 = respuesta2[registros].split("\\;", -1);
					    } else {
					    	
					    	//se presenta una falla, se interrumpe el proceso
					    	respuesta_array[codigoRespuesta]="KO";
							respuesta_array[mensaje]="El proceso de Enviar a CRM gener� un error";
							return respuesta_array;
					    }
			   			
					    Direccion detDir = new Direccion();
					    DireccionExcepcionada dirExc = new DireccionExcepcionada();
					    detDir.obtenerDetalleDirPorCodigo(values[codigo_direccion]);
					    dirExc.obtenerDirExcPorCodigoSolicitud(codigo_solicitud);
					    
			   		     // Se debe llamar al Servicio WsRespuestaDE
			   		     respuesta_WSrespuestaDE= WServicios.respuestaDE(codigo_solicitud, values[codigo_sistema], values[estado_excepcion_type], values[codigo_direccion], values[direccion_normalizada], "0", "NO", "X", values[codigo_pais], values[codigo_municipio], values[codigo_departamento]
			   		    		,"0.0","0.0",dirExc,detDir);
						
			   		     //si se present� alguna falla
						 if ( respuesta_WSrespuestaDE[codigoRespuesta].equals("KO")) {
							 
							 System.out.println("CRM respondi� KO");
							 
							 // se verifica el estado_geor_type
							 if ( "YB".indexOf(values[estado_geor_type]) >= 0 ){
								 System.out.println("Se actualiza detalle_direccion por ser YoB");
								 
								 // se actualiza el estado_geor_type en detalle_direcci�n
								 respuesta_actualizar =  WServicios.actualizartabla("detalle_direccion","une_st","codigo",values[codigo_direccion],"estado_geor_type",values[estado_geor_type]);
							 }	
							 System.out.println("Se actualiza la direcci�n execionada");
							 respuesta_actualizar =  WServicios.actualizartabla("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"estado_geor_type",values[estado_geor_type]);
							 
							 // si alguna actualizaci�n fall�, se interrumpe el proceso
							 if ( respuesta_actualizar[codigoRespuesta].equals("KO")) {
								 
								//se env�an los mensajes de error
								 respuesta_array[codigoRespuesta]=respuesta_actualizar[codigoRespuesta];
								 respuesta_array[mensaje]= "El proceso Enviar a CRM NO fue realizado de forma exitosa :".concat(respuesta_actualizar[descripcionError]);
								 return respuesta_array;
							 } else {
								 
								 // el proceso se complet� sat�stactoriamente
								 respuesta_array[codigoRespuesta]="OK";
								 respuesta_array[mensaje]="El proceso Enviar a CRM NO fue realizado de forma exitosa :</br></br><b>Nota:</b> Se asigno el estado �RESUELTA PROVEEDOR� para que pueda ser enviado posteriormente de forma manual desde la pesta�a Resueltas Proveedor";
								 return respuesta_array;
							 }
						 
						 } else {
							 
							 //El env�o a CRM fue saf�sfactorio. Se recibi� OK
							 System.out.println("CRM respondi� OK");
							 
							 respuesta_actualizar =  WServicios.actualizartabla("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"estado_gestion_type","PUBLICADA CRM");
							 
							// si alguna actualizaci�n fall�, se interrumpe el proceso
							 if ( respuesta_actualizar[codigoRespuesta].equals("KO")) {
								 
								//se env�an los mensajes de error
								 respuesta_array[codigoRespuesta]=respuesta_actualizar[codigoRespuesta];
								 respuesta_array[mensaje]=respuesta_actualizar[descripcionError];
								 return respuesta_array;
							 } else {
								// el proceso se complet� sat�stactoriamente
								 respuesta_array[codigoRespuesta]="OK";
								 respuesta_array[mensaje]="El proceso Enviar CRM fue realizado de forma exitosa";
								 return respuesta_array;
							 }
							 
						 }
			   			 
			   		 }
			   	} else {
			   		// la direcci�n excepcionada no cumple con INEXISTENTE, RESUELTA PROVEEDOR y MIG
			   		respuesta_array[codigoRespuesta]="KO";
					respuesta_array[mensaje]="<b>Operacion no Exitosa</b>, El estado excepcion debe ser 'INEXISTENTE', el estado gestion debe ser 'RESUELTA PROVEEDOR' y el codigo del sistema debe ser diferente a 'MIG'";
					return respuesta_array;
			   	}
		    } 
		    
			respuesta_array[codigoRespuesta]=respuesta[codigoRespuesta];
			respuesta_array[mensaje]=respuesta[descripcionError];
			return respuesta_array;
			
		} catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		} catch (GDEException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorWS = e.getMensage();
		}
		
		respuesta_array[codigoRespuesta]="KO";
		respuesta_array[mensaje]= errorWS;
        return respuesta_array;
    }
	
	/**
	 * Funci�n que ejecuta la gesti�n de una direcci�n excepcionada cuando son resuelta proveedor
	 * @param codigo_solicitud
	 * 			c�digo de solicitud de la direcci�n excepcionada
	 * @return
	 * Arreglo con el resultado de la gesti�n del env�o de CRM de la direcci�n excepcionada
	 * 		respuesta_array[0]: [OK|KO] dependiendo si el resultado de la gesti�n culmin� sin problemas
	 * 		respuesta_array[1]: mensaje de error en caso o �xito de la gesti�n
	 * @throws org.apache.axis2.AxisFault
	 * 		Excepci�n en caso de fallar la comunicaci�n con alg�n servicio
	 */
	public static String[] EnviarCRM_Resuletas_Proveedor(String codigo_solicitud) throws org.apache.axis2.AxisFault{
		String errorWS = "";
		String respuesta_array[] = new String[2];  
		
		
		// Almacena las respuesta de los servicios
	    String respuesta[] = new String[3];
	    String respuesta2[] = new String[3];
	    String [] respuesta_WSrespuestaDE = new String[2];
	    String[] values = null;
	    String[] values2 = null;
	    
	    // Posicion de valores en respuesta
	    int codigoRespuesta  = 0;
	    int descripcionError = 1;
	    int registros        = 2;
	    int mensaje        	 = 1;
	    
	    String [] respuesta_actualizar = new String[2];
	    	   
	    // Posicion de Valores en Respuesta
	    int codigo_sistema = 1;
	    int direccion_normalizada = 3;
	    int estado_geor_type = 4;
	    int estado_excepcion_type = 8;
	    int codigo_direccion = 12;

	    		
	    int codigo_pais = 0;
	    int codigo_departamento = 1;
	    int codigo_municipio = 2;
	    int estrato_type = 3;
	    int rural = 4;
	    int latitud =5;
	    int longitud = 6;
	    
	    
	    try {
	    	
	    	// Buscar el registro actual a ENVIAR CRM
	        respuesta = WServicios.consultartabla_cadena("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"codigo_solicitud|codigo_sistema|direccion_texto_libre|direccion_normalizada|estado_geor_type|fecha_ingreso_direccion|fecha_envio_proveedor|fecha_respuesta_proveedor|estado_excepcion_type|estado_gestion_type|comentario|comentario_proveedor|codigo_direccion");
	        
	        System.out.println("Se procesa la direcci�n excepcionada cod_solicitud: "+codigo_solicitud);
	        
	        //si la consulta no present� problemas
	        if (!respuesta[codigoRespuesta].equals("KO")) {
			    
	        	//limpio los registros
			    String registro =respuesta[registros].replaceAll("[\\[\\]]", "");
				while(registro.indexOf(";;")>=0){
					registro = registro.replaceAll(";;", "; ;");
				}
				if (registro.endsWith(";")) registro = registro+" ";
				
				values = registro.split(";");
				
				//obtengo los datos del detalle direcci�n
			    respuesta2 = WServicios.consultartabla_cadena("detalle_direccion","une_st","codigo",values[codigo_direccion],"codigo_pais|codigo_departamento|codigo_municipio|estrato_type|es_rural|latitud|longitud");
			    
			    //si no falla el proceso limpio el resultado
			    if (respuesta2[codigoRespuesta].equals("OK")) {
			    	respuesta2[registros] = respuesta2[registros].replace("[", "");
					respuesta2[registros] = respuesta2[registros].replace("]", "");
				    values2 = respuesta2[registros].split("\\;", -1);
			    } else {
			    	
			    	//se presenta una falla, se interrumpe el proceso
			    	respuesta_array[codigoRespuesta]="KO";
					respuesta_array[mensaje]="El proceso de Enviar a CRM gener� un error";
					return respuesta_array;
			    }
	        	
			    // Si el codigo de sistema es diferente de MIG
	        	if (!values[codigo_sistema].equals("MIG")  ) {

	        		 // Se debe llamar al Servicio WsRespuestaDE
				    Direccion detDir = new Direccion();
				    DireccionExcepcionada dirExc = new DireccionExcepcionada();
				    detDir.obtenerDetalleDirPorCodigo(values[codigo_direccion]);
				    dirExc.obtenerDirExcPorCodigoSolicitud(codigo_solicitud);
	        		
	        		 System.out.println("El c�digo de sistema es diferente de MIG, env�o a CRM");
	     		    respuesta_WSrespuestaDE= WServicios.respuestaDE(codigo_solicitud, values[codigo_sistema], values[estado_excepcion_type], values[codigo_direccion], values[direccion_normalizada], colocarMascara(values2,estrato_type), colocarMascara(values2,rural), values[estado_geor_type], colocarMascara(values2,codigo_pais), colocarMascara(values2,codigo_municipio), colocarMascara(values2,codigo_departamento)
	     		    		,"".equals(values2[latitud])?"0.0":values2[latitud],"".equals(values2[longitud])?"0.0":values2[longitud],dirExc,detDir);

	     		   System.out.println("CRM respondi�: "+ respuesta_WSrespuestaDE[codigoRespuesta]);
	     		   
	     		    //si se present� alguna falla
					 if ( respuesta_WSrespuestaDE[codigoRespuesta].equals("KO")) {
						 
						 // se actualiza la direcci�n excepcionada
						 respuesta_actualizar =  WServicios.actualizartabla("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"estado_gestion_type","RESUELTA PROVEEDOR");
						 
						 // si el proceso de actualizaci�n falla
						 if ( respuesta_actualizar[codigoRespuesta].equals("KO")) {
							 
							 //se interrumpe el proceso y se env�a mensaje de error
							 respuesta_array[codigoRespuesta]=respuesta_actualizar[codigoRespuesta];
							 respuesta_array[mensaje]= "El proceso Enviar a CRM NO fue realizado de forma exitosa: ".concat(respuesta_actualizar[descripcionError]);
							 return respuesta_array;
						 } else {
							 
							 // el proceso de actualizaci�n pero fal� el env�o a CRM
							 respuesta_array[codigoRespuesta]="OK";
							 respuesta_array[mensaje]="El proceso Enviar a CRM NO fue realizado de forma exitosa</br></br><b>Nota:</b> Se asigno el estado �RESUELTA PROVEEDOR� para que pueda ser enviado posteriormente de forma manual desde la pesta�a Resueltas Proveedor";
							 return respuesta_array;
						 }
						 
					 } else {
						 
						 //el env�o a CRM fue satisfactorio
						 
						 //Se acualiza la direcci�n excepcionada
						 respuesta_actualizar =  WServicios.actualizartabla("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"estado_gestion_type","PUBLICADA CRM");
						 
						 //si se presenta una falla en la actualizaci�n
						 if ( respuesta_actualizar[codigoRespuesta].equals("KO")) {
							 
							 //se interrumpe el proceso y se env�a mensaje de error
							 respuesta_array[codigoRespuesta]=respuesta_actualizar[codigoRespuesta];
							 respuesta_array[mensaje]=respuesta_actualizar[descripcionError];
							 return respuesta_array;
						 } else {
							 
							 // la actualizaci�n se realiz� satisfactotiamente
							 respuesta_array[codigoRespuesta]="OK";
							 respuesta_array[mensaje]="El proceso Enviar CRM fue realizado de forma exitosa";
							 return respuesta_array;
						 }
						 
					 }

	        	} else {
	        		
	        		//El c�digo de sistema igual a MIG, se interrumpe el proceso
			   		respuesta_array[codigoRespuesta]="KO";
					respuesta_array[mensaje]="<b>Operacion NO Exitosa</b>, el codigo del sistema debe ser diferente a 'MIG'";
					return respuesta_array;
			   	}
	        
	        }
	    	
	        respuesta_array[codigoRespuesta]=respuesta[codigoRespuesta];
			respuesta_array[mensaje]=respuesta[descripcionError];
			return respuesta_array;

	    } catch (RemoteException e) {
			e.printStackTrace();
			errorWS = e.getMessage();
		} catch (GDEException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			errorWS = e.getMensage();
		}
			
		respuesta_array[codigoRespuesta]="KO";
		respuesta_array[mensaje]= errorWS;
        return respuesta_array;
	}
	
	/**
	 * Funci�n que evita los errores de ArrayIndexofBound, en caso que falte un par�metro, lo coloca por defecto como vac�o
	 * @param arreglo
	 * 			arreglo de par�metros
	 * @param posicion
	 * 			posici�n en el arreglo
	 * @return
	 * 		el valor de la posici�n en el arreglo. Si la posici�n est� fuera del arreglo devuelve vac�o.
	 */
	private static String colocarMascara(String[] arreglo, int posicion){
		String resp = " ";
		try {
			resp = arreglo[posicion];
		} catch (Exception e) {}
		return resp;
	}

}
