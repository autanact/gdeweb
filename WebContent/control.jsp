<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String Ctrl = request.getParameter("ctrl");
String Usuario = request.getRemoteUser();

%>
<head>
	<ms:setBundle basename="properties.mensajes" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<!-- Hoja de estilos del Gestor -->
	<link href="./Estilo/Index.css" rel="stylesheet" type="text/css" />
	<!-- Javascripts para manejo del Gestor -->
	<script type="text/javascript" src="./Script/Index.js"></script>
</head>

<body>
<p>Control: <%=Ctrl%></p> 
<p>Usuario: <%=Usuario%></p> 
</body>
</html>