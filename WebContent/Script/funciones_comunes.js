
function isDate(val) {
    var d = new Date(val);
    return !isNaN(d.valueOf());
}


function isValidDate(date) {
    var matches = /^(\d{2})[-\/](\d{2})[-\/](\d{4})$/.exec(date);
    if (matches == null) return false;
    var d = matches[1];
    var m = matches[2] - 1;
    var y = matches[3];
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
            composedDate.getMonth() == m &&
            composedDate.getFullYear() == y;
}


function alert_dialogo( mensaje ) {
	$('<div title="Error"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+mensaje+'</p></div>').dialog({
		  height: 150,
		  width: 500,
		  modal: true,
		  buttons: {
			"Aceptar":function() {
		        $( this ).dialog( "close" );
		    }
		  },
		  show: {
			effect: "blind",
			duration: 500
		  },
		  hide: {
			effect: "explode",
			duration: 500
		  }
		});
}
