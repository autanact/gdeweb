/** Javascripts del Gestor PNI GDirExecConf1
 * 
 */


$(document).ready(function() {
    $('#selectall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.codigo_solicitud').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.codigo_solicitud').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });

});

/* Funciones para Evviar CRM */

function enviar() {

	var codigo_solicitud=""
    var c=0;
			
		$('.codigo_solicitud').each(function() { //loop through each checkbox
	           // this.checked = false; //deselect all checkboxes with class "checkbox1" 
			    if (this.checked==true) {
			    	codigo_solicitud =  $(this).val() ;
			    	c=c+1;
			    }
	            
	    });
		
		if (c!=1) {
			alert_dialogo('Debe seleccionar un unico Registro a ser Enviado CRM');
			return false;
		}
		
	
	    $( '<div id="dialogo-enviar" title="Enviar CRM"><p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Esta seguro desea continuar con el proceso Enviar CRM?</p></div>' ).dialog({
	        autoOpen: true,
	        resizable: false,
	        height: 150,
	        width: 500,
	        modal: true,
	        buttons: {
	          "Si": function() {
	        		window.open("enviar_CRM_gestion.jsp?codigo_solicitud="+codigo_solicitud,"_self")
	          },
	          "No": function() {
	              $( this ).dialog( "close" );
	            }
	          
	        },
	        show: {
	      		effect: "blind",
	      		duration: 500
	        },
	        hide: {
	      		effect: "explode",
	      		duration: 500
	        }
	      });
}


$( "#enviar" ).click(function() {
	enviar();
});

/* Funciones para Editar Direccion */

function editar() {
	var direccion_texto_libre= document.getElementById("direccion_texto_libre").value;
	var contacto = document.getElementById("contacto").value;
	var telefonos = document.getElementById("telefonos").value;
	var comentario = document.getElementById("comentario").value;
	
	if(direccion_texto_libre=='') {
    	alert_dialogo( 'Dirección Natural es un campo requerido, por favor introduzca un valor valido.' );
    	return;
    }
	if(contacto=='') {
    	alert_dialogo( 'Contacto es un campo requerido, por favor introduzca un valor valido.' );
    	return;
    }
	if(telefonos=='') {
    	alert_dialogo( 'Teléfono Contacto es un campo requerido, por favor introduzca un valor valido.' );
    	return;
    }
	if(comentario=='') {
		document.getElementById("comentario").value="Dirección procesada en Gestión Manual";
    }
	$( "#dialogo-guardar" ).dialog( "open" );
}


$(function() {
    $( "#dialogo-guardar" ).dialog({
      autoOpen: false,
      resizable: false,
      height: 150,
      width: 500,
      modal: true,
      buttons: {
        "Si": function() {
           $('form#form_editar').submit();
           $( this ).dialog( "close" );
        },
        "No": function() {
            $( this ).dialog( "close" );
            $( "#dialogo-editar"  ).dialog( "close" );
        }
        
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });

$(function() {
    $( "#dialogo-editar" ).dialog({
      autoOpen: false,
      resizable: false,
      height: 380,
      width: 500,
      modal: true,
      buttons: {
        "Guardar Cambios": function() {
        	editar();      
        },
        "Cancelar": function() {
            $( this ).dialog( "close" );
          }
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });

$( "#editar" ).click(function() {
	var codigo_solicitud=""
	var c=0;
		
	$('.codigo_solicitud').each(function() { //loop through each checkbox
		    if (this.checked==true) {
		    	codigo_solicitud =  $(this).val() ;
		    	c=c+1;
		    }
    });
	
	if (c!=1) {
		alert_dialogo('Debe seleccionar un unico Registro a ser editado');
		return false;
	}
	
    $("#jspformulario").load( "editar_formulario.jsp?codigo_solicitud="+codigo_solicitud);
	$("#dialogo-editar").dialog( "open" );
});


/* Funciones para filtrar */

function seleccionar_filtro() {
	var x = document.getElementById("tipo_filtro").value;
	if (parseInt(x) =='0') {
		$('#filtro_codigo').show(); 
		$('#filtro_fecha').hide();
	} 
	
	if (parseInt(x) =='1' || parseInt(x) =='2' || parseInt(x) =='3' || parseInt(x) =='4'  ) {
		$('#filtro_codigo').hide(); 
		$('#filtro_fecha').show();
	} 
	
}


function filtrar() {
	
	var x = document.getElementById("tipo_filtro").value;
	var c = document.getElementById("codigo").value;
	var d = document.getElementById("desde").value;
	var h = document.getElementById("hasta").value;

	if (parseInt(x) =='0') {
        if(c=='') {
        	alert_dialogo( 'El Codigo es un campo requerido, por favor introduzca un valor valido.' );
        	return;
        }
	} 
	
	if (parseInt(x) =='1' || parseInt(x) =='2' || parseInt(x) =='3' || parseInt(x) =='4'  ) {
	    if( !isValidDate(d) || !isValidDate(h) ) {
			alert_dialogo( 'Algunas de las fechas no es validad. Por favor verifique.' );
			return;
		} else {
			var inicio = d.split("/");
			var fin = h.split("/");
			
			var fechaDesde = new Date(inicio[2],(inicio[1]-1),inicio[0]);
			var fechaHasta = new Date(fin[2],(fin[1]-1),fin[0]);
			
			if( fechaDesde > fechaHasta ) {
				alert_dialogo( 'La fecha DESDE no puede ser mayor que la fecha HASTA, por favor introduzca un valor valido.' );
				return;
			}
		}
	}
	$( 'form#form_filtro' ).submit();
	$( "#dialogo_filtrar" ).dialog( "close" );
}


$(function() {
$( "#dialogo_filtrar" ).dialog({
  autoOpen: false,
  height: 270,
  width: 500,
  modal: true,
  buttons: {
	"Ejecutar filtro":function() {
		filtrar();

    }
  },
  show: {
	effect: "blind",
	duration: 500
  },
  hide: {
	effect: "explode",
	duration: 500
  }
});

$( "#filtro" ).click(function() {
  $( "#dialogo_filtrar" ).dialog( "open" );
});

/// Configuracion regional para el Calendario
$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '<Ant',
		 nextText: 'Sig>',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
		 };
		 $.datepicker.setDefaults($.datepicker.regional['es']);

$( "#desde" ).datepicker({
    showOn: "button",
    buttonImage: "../Imagen/Calendario18P.png",
    buttonImageOnly: true,
    buttonText: "Select date"
  });

$( "#hasta" ).datepicker({
    showOn: "button",
    buttonImage: "../Imagen/Calendario18P.png",
    buttonImageOnly: true,
    buttonText: "Select date"
  });
});


$(function() {

$( "#actualizar" ).click(function() {
	window.open("GDirExec4.jsp","_self")
});
});
