/** Javascripts del Gestor PNI GDirExecConf1
 * 
 */

$(document).ready(function() {
    $('#selectall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.codigo_sistema').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
            $('.codigo_sistema').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });        
        }
    });


});

/* Funciones para Procesar */

function procesar() {
	
	window.open("procesar.jsp","_self")
}

$(function() {
    $( "#dialogo-procesar" ).dialog({
      autoOpen: false,
      resizable: false,
      height: 150,
      width: 500,
      modal: true,
      buttons: {
        "Si": function() {
           procesar();
           $( this ).dialog( "close" );
        },
        "No": function() {
            $( this ).dialog( "close" );
          }
        
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });

$( "#procesar" ).click(function() {
	  $( "#dialogo-procesar" ).dialog( "open" );
});

/* Funciones para Importar */

function importar() {
	
 $('form#form_importar').submit();

}

$(function() {
    $( "#dialogo-importar" ).dialog({
      autoOpen: false,
      resizable: false,
      height: 210,
      width: 500,
      modal: true,
      buttons: {
        "Continuar": function() {
            importar();
            $( this ).dialog( "close" );
        },
        "No": function() {
            $( this ).dialog( "close" );
          }
        
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });

$( "#importar" ).click(function() {
	  $( "#dialogo-importar" ).dialog( "open" );
});


/* Funciones para Exportar */

function exportar() {
	
	window.open("exportar.jsp","_self")
	
}

$(function() {
    $( "#dialogo-exportar" ).dialog({
      autoOpen: false,
      resizable: false,
      height: 150,
      width: 500,
      modal: true,
      buttons: {
        "Si": function() {
           exportar();
           $( this ).dialog( "close" );
        },
        "No": function() {
            $( this ).dialog( "close" );
          }
        
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });

$( "#exportar" ).click(function() {
	  $( "#dialogo-exportar" ).dialog( "open" );
});



/* Funciones para filtrar */

function seleccionar_filtro() {
	var x = document.getElementById("tipo_filtro").value;
	if (parseInt(x) =='0' || parseInt(x) =='1' || parseInt(x) =='2') {
		$('#filtro_codigo').show(); 
		$('#filtro_fecha').hide();
	} 
	
	if (parseInt(x) =='3' || parseInt(x) =='4' || parseInt(x) =='5') {
		$('#filtro_codigo').hide(); 
		$('#filtro_fecha').show();
	} 
	
}


function filtrar() {
	
	var x = document.getElementById("tipo_filtro").value;
	var c = document.getElementById("codigo").value;
	var d = document.getElementById("desde").value;
	var h = document.getElementById("hasta").value;

	if (parseInt(x) =='0' || parseInt(x) =='1' || parseInt(x) =='2') {
        if(c=='') {
        	alert_dialogo( 'El Codigo es un campo requerido, por favor introduzca un valor valido.' );
        	return;
        }
	} 
	
	if (parseInt(x) =='3' || parseInt(x) =='4' || parseInt(x) =='5') {
		if( !isValidDate(d) || !isValidDate(h) ) {
			alert_dialogo( 'Algunas de las fechas no es validad. Por favor verifique.' );
			return;

		} else {
			var inicio = d.split("/");
			var fin = h.split("/");
			
			var fechaDesde = new Date(inicio[2],(inicio[1]-1),inicio[0]);
			var fechaHasta = new Date(fin[2],(fin[1]-1),fin[0]);
			
			if( fechaDesde > fechaHasta ) {
				alert_dialogo( 'La fecha DESDE no puede ser mayor que la fecha HASTA, por favor introduzca un valor valido.' );
				return;
			}
		}
	}
	
	$( 'form#form_filtro' ).submit();
	$( "#dialogo_filtrar" ).dialog( "close" );
}


$(function() {
$( "#dialogo_filtrar" ).dialog({
  autoOpen: false,
  height: 270,
  width: 500,
  modal: true,
  buttons: {
	"Ejecutar filtro":function() {
        filtrar();
      
    }
  },
  show: {
	effect: "blind",
	duration: 500
  },
  hide: {
	effect: "explode",
	duration: 500
  }
});

$( "#filtro" ).click(function() {
  $( "#dialogo_filtrar" ).dialog( "open" );
});

/// Configuracion regional para el Calendario
$.datepicker.regional['es'] = {
		 closeText: 'Cerrar',
		 prevText: '<Ant',
		 nextText: 'Sig>',
		 currentText: 'Hoy',
		 monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		 monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		 dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		 dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		 dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		 weekHeader: 'Sm',
		 dateFormat: 'dd/mm/yy',
		 firstDay: 1,
		 isRTL: false,
		 showMonthAfterYear: false,
		 yearSuffix: ''
		 };
		 $.datepicker.setDefaults($.datepicker.regional['es']);

$( "#desde" ).datepicker({
    showOn: "button",
    buttonImage: "../Imagen/Calendario18P.png",
    buttonImageOnly: true,
    buttonText: "Select date"
  });

$( "#hasta" ).datepicker({
    showOn: "button",
    buttonImage: "../Imagen/Calendario18P.png",
    buttonImageOnly: true,
    buttonText: "Select date"
  });
});


/* Funciones para filtrar */
$(function() {

$( "#actualizar" ).click(function() {
	window.open("GDirExec2.jsp","_self")
});
});


