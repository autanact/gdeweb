/** Javascripts del Gestor PNI
 * 
 */
var Agestor = ["¬","¬","¬","¬","¬","¬","¬","¬","¬","¬"]; // Arreglo que indica el orden de las ventanas
var AGmenu= [-1,-1,-1,-1,-1,-1]; // Arreglo que controla las opciones de Menu activas de un gestor, donde el Gestor es la posición dentro del arreglo
var AImenu = ["¬","¬","¬","¬","¬","¬"]; // Arreglo que indica el orden de las ventanas
var Vgestoractivo= -1; //Variable que controla el Gestor activo

var sPathJSP ='';

//Función para salir del Gestor
//function salir(){
//	if (confirm("Está seguro en salir de la página?")){top.location="index.jsp";}
//} 

function salir(){

	window.open("index.jsp","_self")
} 

//Función que activa la funcionalidad de la actividad de configuración
// Gestor= Parametro de entrada que tiene la nomenclatura del Gestor a activar
function ejecutaVent(Gestor,v1,v2 ,PathJSP){
    // alert('modulo: '+Gestor + 'ruta:'+PathJSP);
    sPathJSP=PathJSP;
	activarVentana(Gestor); // Coloca la ventana de Configuración adelante
	document.getElementById("CcierraVent").style.visibility="visible";
}

// Función que coloca la ventana activa adelante de las demas ventanas
// Vgestor= Parametro de entrada que identifica la ventana del Gestor a colocar adelante
function activarVentana(Vgestor){
	if (Agestor[0] != Vgestor){
		ajustarArreglo(Vgestor);
		if (Agestor[1] != "¬"){
			document.getElementById("TapaVent").style.visibility="visible";	
		}
		colocarTitulos();
	}
}

//Función que cierra la Ventana de configuración
function cerrarVent(){
	var Vgestor = Agestor[0];
	var tit2 = buscaTituloVentana(Vgestor);
	if (confirm("Está seguro en querer salir de "+ tit2)){
		document.getElementById("C" + Vgestor).style.zIndex="1";
		document.getElementById("C" + Vgestor).style.visibility="hidden";
		Agestor[0] = Agestor[1];
		ajustarArreglo(Agestor[1]);
		if (Agestor[1] == "¬"){
			document.getElementById("TapaVent").style.visibility="hidden";	
		}
		if (Agestor[0] == "¬"){
			document.getElementById("CcierraVent").style.visibility="hidden";	
		}
		colocarTitulos();
	}
}


// Función que ajusta el arreglo de gestores activos 
//Igestor= Parametro de entrada que identifica la ventana del Gestor a colocar adelante o elimina
function ajustarArreglo(Igestor){	
	var Atemp= ["¬","¬","¬","¬","¬","¬","¬","¬","¬","¬"];
	Atemp[0] = Igestor;
	if (Igestor != "¬"){
		document.getElementById("C" + Igestor).style.zIndex="850";
		document.getElementById("C" + Igestor).style.visibility="visible";
	}
	var p = 1;
	for (var i=0; i < 10; i++){
		if (Agestor[i] != Igestor){
			Atemp[p] = Agestor[i];
			var t = 750 - p;
			p++;
			if (Agestor[i] != "¬"){
				document.getElementById("C" + Agestor[i]).style.zIndex = t;					
			}
		}
	}
	Agestor = Atemp;
}

// Función que muestra el titulo del Gestor Activo
function colocarTitulos(){
	var tit1 = "Gestor Smallworld";
	if (Agestor[0] != "¬"){
		var tit2 = buscaTituloVentana(Agestor[0]);
		document.getElementById("Ccierra").title= "Cerrar Ventana de " + tit2;
		tit1 = tit1 + " - " + tit2
	}
	document.getElementById("Tpagina").innerHTML= tit1;

}

//Función que busca el titulo del Gestor Activo
function buscaTituloVentana(Vgestor){
	var tit;
	if (Vgestor == "GNov"){tit = "Novedades PNI - Fénix"}
	else {
		if (Vgestor == "GNovConf"){tit = "Configuración de las Novedades PNI - Fénix"}
		else {
			if (Vgestor == "GDirExec"){tit = "Direcciones Excepcionadas"}
			else {
				if (Vgestor == "GDirExecConf"){tit = "Configuración de direcciones excepcionadas"}
				else {
					if (Vgestor == "GBi"){tit = "BI"}
					else {
						tit = "Configuración BI"
					}
				}
			}
		}
	}
	return tit;
}

// Función que activa un gestor
// vgestor= Parametro de entrada que identifica el valor del gestor que se esta activando
function FejecutarGestor(Vgestor){
	if (Vgestoractivo != Vgestor){
		if (Vgestoractivo != -1){
			document.getElementById("Mgestor" + Vgestoractivo).className="MGestor";
		}
		document.getElementById("Mgestor" + Vgestor).className="MGestorP";
		Vgestoractivo = Vgestor;
	}
}

// Función que activa la funcionalidad de la actividad escogida en el menu
// Vid= Identificador de la opción
// pos= Parametro de entrada que indica la Posición dentro del arreglo AGmenuConf[pos]
function Fejecutar(Vid,pos,opc){
	var Vmenu = AImenu[pos];
	if (Vid != Vmenu){
		if (Vmenu != "¬"){document.getElementById(Vmenu).className="MOpcion";}
		document.getElementById(Vid).className="MOpcionP";
		AImenu[pos] = Vid;
		//var ruta="./Novedades/" + Vid + ".html";
		var ruta="./"+sPathJSP + "/" + Vid + ".jsp";
		
		//alert('ruta:'+ ruta +' Iframe:'+"E" + opc);
		document.getElementById("E" + opc).src=ruta;
	}
}

function cerrar_ventana_frame() {
	
	var Vgestor = Agestor[0];
	var tit2 = buscaTituloVentana(Vgestor);
	
		document.getElementById("C" + Vgestor).style.zIndex="1";
		document.getElementById("C" + Vgestor).style.visibility="hidden";
		Agestor[0] = Agestor[1];
		ajustarArreglo(Agestor[1]);
		if (Agestor[1] == "¬"){
			document.getElementById("TapaVent").style.visibility="hidden";	
		}
		if (Agestor[0] == "¬"){
			document.getElementById("CcierraVent").style.visibility="hidden";	
		}
		colocarTitulos();

}

$(function() {
$( "#dialogo-confirmar" ).dialog({
  autoOpen: false,
  height: 180,
  width: 400,
  modal: true,
  buttons: {
	  "Si": function() {
		  cerrar_ventana_frame();
          $( this ).dialog( "close" );
           
        },
        "No": function() {
            $( this ).dialog( "close" );
          }
  },
  show: {
	effect: "blind",
	duration: 500
  },
  hide: {
	effect: "explode",
	duration: 500
  }
});

	$( "#Ccierra" ).click(function() {
	  $( "#dialogo-confirmar" ).dialog( "open" );
	});
});

$(function() {
	$( "#dialogo-salir" ).dialog({
	  autoOpen: false,
	  height: 180,
	  width: 400,
	  modal: true,
	  buttons: {
		  "Si": function() {
			  salir();
	          $( this ).dialog( "close" );
	           
	        },
	        "No": function() {
	            $( this ).dialog( "close" );
	          }
	  },
	  show: {
		effect: "blind",
		duration: 500
	  },
	  hide: {
		effect: "explode",
		duration: 500
	  }
	});

	});


function salir_del_sistema() {
	$( "#dialogo-salir" ).dialog( "open" );
}