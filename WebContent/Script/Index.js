// Javascripts de la Página Index.html

//Valida que una entrada de datos no sea vacía
// q = Cadena a validar
// true = No esta vacía
// false = Viene vacía
function vacio(q) {
	for ( i = 0; i < q.length; i++ ) {
		if ( q.charAt(i) != " " ) {
			return true
        }
	}
	return false
}

//Función que confirma las credenciales del usuario
function ConfirmarCred(){
	var Usuario = vacio(document.getElementById("Tusuario").value);
	var Clave = vacio(document.getElementById("Tclave").value);
	if (Usuario && Clave){
		document.getElementById("fichaUsuario").submit();
	}
	else {
		if (Usuario || Clave){
			document.getElementById("Merror").style.visibility="visible";
		}
	}
}

//Funcion que esconde el aviso de Error en las credenciales
function Cerror(ve){
	if (ve == "1") {document.getElementById("Merror").style.visibility="visible";}
	else {document.getElementById("Merror").style.visibility="hidden";}
}