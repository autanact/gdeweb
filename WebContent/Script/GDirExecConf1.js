/** Javascripts del Gestor PNI GDirExecConf1
 * 
 */

var shora_minuto   = "ninguno" ;
var shora_minuto_p = "ninguno" ;
var nMaximo=59 ;
var nMaximo_p=59 ;
var nMinimo_p=00 ;
function incrementar(){

	if (shora_minuto=="ninguno")  {
		alert("Por favor seleccione un campo valido (HH,MM) para incrementar");
		return false;
	}
	if (parseInt(document.getElementById(shora_minuto).value)< nMaximo) {
		document.getElementById(shora_minuto).value= pad(parseInt(document.getElementById(shora_minuto).value)+1,2);
	}

}



function decrementar(){

	if (shora_minuto=="ninguno")  {
		alert("Por favor seleccione un campo valido (HH,MM) para decrementar");
		return false;
	}
	if (parseInt(document.getElementById(shora_minuto).value)> 0) {
		document.getElementById(shora_minuto).value= pad(parseInt(document.getElementById(shora_minuto).value)-1,2);
	}

}




function campo_activo(sActivo,nMax) {
	shora_minuto=sActivo;
	nMaximo=nMax;
}

function soloIntervalos(obj) {
  
	if (parseInt(obj.value)<00 || parseInt(obj.value)>nMaximo) {
		alert("El valor ingresado debe estar en el rango de 0 a "+pad(nMaximo,2));
		obj.value=pad(nMaximo,2);
		obj.focus();
	}

}
// Funciones Sobre Periocidad //


function campo_activo_p(sActivo) {
	
	shora_minuto_p=sActivo;
	nMinimo_p=0;
	nMaximo_p=0;
	if (shora_minuto_p=="hora_periocidad") {
		
		nMaximo_p = parseInt(document.getElementById('hora_final').value) - parseInt(document.getElementById('hora_inicial').value);
		
	}
	if (shora_minuto_p=="min_periocidad") {
		nMaximo_p = parseInt(document.getElementById('min_final').value) - parseInt(document.getElementById('min_inicial').value);
		
		if( parseInt(document.getElementById('hora_periocidad').value) > 0 ) { 
			nMinimo_p=0;
		} else {
			nMinimo_p=30;
			document.getElementById('min_periocidad').value =30;
		}
		if (nMaximo_p < nMinimo_p ) {
		//	alert("El tiempo mínimo permitido para la periodicidad es 30 minutos, actualmente el maximo es "+pad(nMaximo_p,2));
		}
	}
}

function soloIntervalos_p(obj) {
	
	if (shora_minuto_p=="min_periocidad") {
		
		if (nMaximo_p < nMinimo_p ) {
		//	alert("El tiempo mínimo permitido para la periodicidad es 30 minutos, actualmente el maximo es "+pad(nMaximo_p,2));
			return false;
		}
	}

	if (parseInt(obj.value)< nMinimo_p || parseInt(obj.value)>nMaximo_p) {
		alert("El valor ingresado debe estar en el rango de "+pad(nMinimo_p,2)+" a "+pad(nMaximo_p,2));
		obj.value=pad(nMaximo_p,2);
		obj.focus();
	}

}
function incrementar_p(){

	if (shora_minuto_p=="ninguno")  {
		alert("Por favor seleccione un campo valido (HH,MM) para incrementar");
		return false;
	}
	if (parseInt(document.getElementById(shora_minuto_p).value)< nMaximo_p) {
		document.getElementById(shora_minuto_p).value= pad(parseInt(document.getElementById(shora_minuto_p).value)+1,2);
	}

}

function decrementar_p(){

	if (shora_minuto_p=="ninguno")  {
		alert("Por favor seleccione un campo valido (HH,MM) para incrementar");
		return false;
	}
	if (parseInt(document.getElementById(shora_minuto_p).value)> nMinimo_p) {
		document.getElementById(shora_minuto_p).value= pad(parseInt(document.getElementById(shora_minuto_p).value)-1,2);
	}

}
// Funciones Direcciones por Lote
function soloIntervalos_l(obj) {
  
	if (parseInt(obj.value)<1 || parseInt(obj.value)>500) {
		alert("El valor ingresado debe estar en el rango de 1 a 500");
		obj.value="500";
		obj.focus();
	}

}


function incrementar_l(){

	
	if (parseInt(document.getElementById("tamanno_lote").value)< 500) {
		document.getElementById("tamanno_lote").value= pad(parseInt(document.getElementById("tamanno_lote").value)+1,3);
	}

}



function decrementar_l(){


	if (parseInt(document.getElementById("tamanno_lote").value)> 1) {
		document.getElementById("tamanno_lote").value= pad(parseInt(document.getElementById("tamanno_lote").value)-1,3);
	}

}

// Funciones Numero de Dias
function soloIntervalos_d(obj) {
  
	if (parseInt(obj.value)<1 || parseInt(obj.value)>365) {
		alert("El valor ingresado debe estar en el rango de 1 a 365");
		obj.value="365";
		obj.focus();
	}

}


function incrementar_d(){

	
	if (parseInt(document.getElementById("dias_historial").value)< 365) {
		document.getElementById("dias_historial").value= pad(parseInt(document.getElementById("dias_historial").value)+1,3);
	}

}



function decrementar_d(){


	if (parseInt(document.getElementById("dias_historial").value)> 1) {
		document.getElementById("dias_historial").value= pad(parseInt(document.getElementById("dias_historial").value)-1,3);
	}

}

// Funciones Comunes

function pad(n, width, z) {
	  z = z || '0';
	  n = n + '';
	  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function soloNumeros(e)
{
var keynum = window.event ? window.event.keyCode : e.which;
if ((keynum == 8) || (keynum == 46))
	return true;	
	
return /\d/.test(String.fromCharCode(keynum));
}


// Confirmar Configuracion //
function ConfirmarCon(){
	
	var lista_dias="";
	// Validad dias
	// Validad que se haya seleccionado un Dia en el Checkbox 
	if ($("input[name='dias']:checked").length == 0) {
		
		alert('Debe seleccionar uno o varios dias de la semana para la gestion de direcciones.');
		return false;
    }
	// Cargar los valores en una variable
	$("input[name='dias']:checked").each(function(){
		lista_dias = lista_dias + $(this).val() + ',';
	});
	
	//alert( lista_dias);
	//Validar Hora Final
	if (parseInt(document.getElementById("hora_final").value+document.getElementById("min_final").value) <= parseInt(document.getElementById("hora_inicial").value+document.getElementById("min_inicial").value)) {
		alert('La Hora Final no puede ser menor o igual que la Hora Inicial.');
		return false;
	}
	
	// Validar de Direcciones por Lote está fuera del rango permitido
	if (parseInt(document.getElementById("tamanno_lote").value)<1 || parseInt(document.getElementById("tamanno_lote").value)>500) {
		alert('El valor ingresado en el campo # de Direcciones por Lote está fuera del rango permitido (1 a 500).');
	}
	
	// Validar Hora Inicial
	if (parseInt(document.getElementById("hora_inicial").value)<0 || parseInt(document.getElementById("hora_inicial").value)>23) {
		alert('El valor del campo Hora Inicial no es un valor válido (00 a 23).');
	}
	
	// Validar Hora Final
	if (parseInt(document.getElementById("hora_final").value)< parseInt(document.getElementById("hora_inicial").value) || parseInt(document.getElementById("hora_final").value)>23) {
		alert('El valor del campo Hora Final no es un valor válido ('+pad(document.getElementById("hora_inicial").value,2)+' a 23).');
	}
	

	$(document).ready(function() {
		$('form#FichaPub').submit();
	});
}


//Confirmar Guardar //
function Guardar(){
	
	// Validar de Direcciones por Lote está fuera del rango permitido
	if (parseInt(document.getElementById("dias_historial").value)<1 || parseInt(document.getElementById("dias_historial").value)>365) {
		alert('El valor del campo Dias Historial no es una valor válido (1 a 365).');
	}
	
		
	$(document).ready(function() {
		$('form#FichaHis').submit();
	});
}


$(function() {
    $( "#dialogo-informacion" ).dialog({
      autoOpen: true,
      resizable: false,
      height: 180,
      width: 500,
      modal: true,
      buttons: {
 
        "Aceptar": function() {
            $( this ).dialog( "close" );
          }
        
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });



