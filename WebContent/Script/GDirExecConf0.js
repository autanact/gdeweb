/** Javascripts del Gestor PNI GDirExecConf1
 * 
 */

$('#id_localidad').on('change', function() {
	  var value = $(this).val();
	  if (value=="0") {
	   $("#id_localidad option").prop("selected",true);
	  }
});

$('#codigo_municipio').on('change', function() {
	  var value = $(this).val();
	  if (value=="0") {
	   $("#codigo_municipio option").prop("selected",true);
	  }
});


$(document).ready(function() {
   
	$("#id_localidad").click(function() {
		var multi = document.getElementById("#id_localidad".id);
		
	  //	if (multi.options[0] ) {
	  //		alert("holaa");
	  //	}
	  //for (var i = 0; i < multi.options.length; i++) {
      //      multi.options[i].selected = true;
      //  }
	  //$("#id_localidad option").prop("selected",true);
		
    }) ;
});

// Validacion y accion para Agregar una localidad
function AgregaLocalidad(){
	var codigo_departamento=$('#codigo_departamento').val();
	var lista_codigo_municipio="";
	// Si existe un municipio o varios seleccionados , validamos que SELECCIONAR TODOS se deseleccione 
	if ($('#codigo_municipio option:selected').length > 1) {
	
		$('#codigo_municipio option:selected').each(function() {
			if($(this).val()==0) {
				$('#codigo_municipio>option:eq(0)').prop('selected', false);
			};
		});
		
    }
	var codigo_municipio_valores = $('#codigo_municipio').val();
    lista_codigo_municipio = codigo_municipio_valores.join(", ");
    
	//alert ('Departamento :'+codigo_departamento+'\n'+'Municipios :'+ lista_codigo_municipio );
	
	
	$(document).ready(function() {
		$('form#FichaCarga').submit();
	});

}

$('#codigo_departamento').on('change', function() {
	  var value = $(this).val();
	  var res = value.split("|",2); 
	  window.open("GDirExecConf0.jsp?codigo_departamento_aux="+res[0],"_self")
});

function cambiar_municipios(codigo_departamento){ 
	
	window.open("GDirExecConf0.jsp?codigo_departamento_aux="+codigo_departamento,"_self")
}

//Eliminar localidad
function EliminarLocalidad(){
	
	var lista_id_localidad="";
	// Si existe una localidad o varias seleccionadas , validamos que SELECCIONAR TODOS se deseleccione 
	if ($('#id_localidad option:selected').length > 1) {
	
		$('#id_localidad option:selected').each(function() {
			if($(this).val()==0) {
				$('#id_localidad>option:eq(0)').prop('selected', false);
			};
		});
		
    }
	var id_localidad_valores = $('#id_localidad').val();
	lista_id_localidad = id_localidad_valores.join(", ");
    
	//alert ('Localidades :'+ lista_id_localidad );
	
	/*if (confirm("Desea continuar con la eliminacion?")){
		$(document).ready(function() {
			$('form#FichaEliminar').submit();
		});
	}*/
	
	$( "#dialogo-eliminar" ).dialog( "open" );
	
}


function eliminar() {
	
	$('form#FichaEliminar').submit();
}


$(function() {
    $( "#dialogo-eliminar" ).dialog({
      autoOpen: false,
      resizable: false,
      height: 150,
      width: 500,
      modal: true,
      buttons: {
        "Si": function() {
          eliminar();
          $( this ).dialog( "close" );
           
        },
        "No": function() {
            $( this ).dialog( "close" );
          }
        
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });

$(function() {
    $( "#dialogo-existentes" ).dialog({
      autoOpen: true,
      resizable: false,
      height: 250,
      width: 500,
      modal: true,
      buttons: {
 
        "Aceptar": function() {
            $( this ).dialog( "close" );
          }
        
      },
      show: {
    		effect: "blind",
    		duration: 500
      },
      hide: {
    		effect: "explode",
    		duration: 500
      }
    });
 });

   

