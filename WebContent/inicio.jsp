<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>
<%@ page import="func.unes.gde.*" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.servlet.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%

// Ruta Archivos XML
String path_xml = getServletContext().getRealPath("xml/grupos.xml");
// Variable de Sesion Usuario Actual 
session.setAttribute("session_usuario", request.getRemoteUser().toString());
String session_usuario = (String)session.getAttribute("session_usuario");
// Gestion para determinar el grupo.
String session_privilege = "administrador";
if(request.isUserInRole("GG-T-SMW_GDE_Consultas")) {
	session_privilege = "consultas";
}
session.setAttribute("session_privilege", session_privilege);

// Leer Archivo GRUPOS.XML //
DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
DocumentBuilder db = dbf.newDocumentBuilder();
Document doc = db.parse(path_xml);
NodeList GrupoID = doc.getElementsByTagName("GrupoID");
NodeList GrupoDes = doc.getElementsByTagName("GrupoDes");
NodeList GrupoMenu = doc.getElementsByTagName("GrupoMenu");

ArrayList a_roles = new ArrayList();

for(int i=0;i<=GrupoID.getLength()-1;i++) { 
    if (request.isUserInRole(GrupoID.item(i).getFirstChild().getNodeValue()) ){
    	a_roles.add(GrupoID.item(i).getFirstChild().getNodeValue());
    }
}

// Llamar a Clase Menu Acceso que determina los Permisos del Usuario
String[] a_menu_acceso = Seguridad.menu_acceso(a_roles, session_usuario, path_xml );

// Guardar el  Nivel de Seguridad en variable sesion
session.setAttribute("session_nivel", a_menu_acceso[1]);

%>
<head>
<ms:setBundle basename="properties.mensajes" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="cache-control" content="max-age=0" /> 
<meta http-equiv="cache-control" content="no-cache" /> 
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<title>Gestor Smallworld</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="./Estilo/Inicio.css" rel="stylesheet" type="text/css" />
<!-- Javascripts para manejo del Gestor -->
<script src="./Script/jquery.js"></script>
<script src="./Script/jquery-ui.js"></script>
<script type="text/javascript" src="./Script/inicio2.js"></script>
<link href="./Estilo/jquery-ui_2.css" rel="stylesheet">
</head>
<body>

<!-- Caja del Encabezado de la Página de arranque del Gestor de PNI -->

<div id="Encabezado">
	<span id="Tpagina">Gestor Smallworld</span>
	<span id="Nusuario">Usuario: <b><%=session_usuario %></b></span>
</div>
<!-- Menú Principal de la página de arranque del Gestor PNI -->
<ul id="MenuIdPrin" class="Menu">
	<li class="MOpcionPrin">
		<span id="LogoMenu" title="Menú principal"></span>
		<ul>
		
	       <!-- Inicio Deslegar el Menu Principal-->
	       <%= a_menu_acceso[0] %>
	       <!-- Fin Deslegar el Menu Principal--> 
				
			<!-- Opción para abrir página de Ayuda -->
			<li id="Graya" class="SubMOpcionPrin" style="margin-left: 0px;">
				<span>Ayuda</span>
			</li>
			<!-- Opción para salir del Gestor -->
			<li id="Graya" class="SubMOpcionPrin"  style="margin-left: 0px;">
				<span onclick="salir_del_sistema()">Salir</span>
			</li>
		</ul>
	</li>
</ul>
<!-- Caja que delinea area de ventanas de ejecución  -->
<div id="remarca"></div>
<!-- Tapa de las ventanas de ejecución  -->
<div id="TapaVent"></div>
<!-- Caja contenedora de los menus de opciones y ejecuciones de las funcionalidades y configuraciones -->
<div id="cajaFunConf">
	<!-- Caja contenedora del menu y ventanas de ejecucion de las Funcionalidades del Gestor de Novedades -->
	<div id="CGNov" class="cajaMenuIfra">
		<!-- Menú de las actividades que puede realizar en el Gestor de Novedades -->
		<div id="MenuGNov" class="CMenu">
		    <span id="GNovPenPub" onclick="Fejecutar(this.id,0,'GNov')" class="MOpcion">Pendientes por publicar</span>   
		    <span id="GNovPenPro" onclick="Fejecutar(this.id,0,'GNov')" class="MOpcion">Pendientes por procesar</span>    
		    <span id="GNovErr" onclick="Fejecutar(this.id,0,'GNov')" class="MOpcion">Errores</span>    
		    <span id="GNovPro" onclick="Fejecutar(this.id,0,'GNov')" class="MOpcion">Procesadas</span>
		</div>
		<!-- Caja donde se ejecutan las funcionalidades del Gestor de Novedades -->
		<div class="CEjecutar">
			<iframe id="EGNov" class="Ciframe" scrolling="auto"></iframe>
		</div>
	</div>
	<!-- Caja contenedora del menu y ventanas de ejecucion de las Configuraciones del Gestor de Novedades -->
	<div id="CGNovConf" class="cajaMenuIfra">
		<!-- Menú de las Configuraciones que puede realizar en el Gestor de Novedades -->
		<div id="MenuGNovConf" class="CMenu">
		    <span id="GNovConfLocTec" onclick="Fejecutar(this.id,1,'GNovConf')" class="MOpcion">Localidad/Tecnología</span>   
		    <span id="GNovConfPubRec" onclick="Fejecutar(this.id,1,'GNovConf')" class="MOpcion">Publicación/Recepción</span>    
		</div>
		<!-- Caja donde se ejecutan las funcionalidades del Gestor de Novedades -->
		<div class="CEjecutar">
			<iframe id="EGNovConf" class="Ciframe" scrolling="auto"></iframe>
		</div>
	</div>
	<!-- Caja contenedora del menu y ventanas de ejecucion de las Funcionalidades del Gestor de Direcciones Excepcionadas -->
	<div id="CGDirExec" class="cajaMenuIfra">
		<!-- Menú de las actividades que puede realizar en el Gestor de Direcciones Excepcionadas -->
		<div id="MenuGDirExec" class="CMenu">
		    <span id="GDirExec0" onclick="Fejecutar(this.id,2,'GDirExec')" class="MOpcion">Pendientes</span>   
		    <span id="GDirExec1" onclick="Fejecutar(this.id,2,'GDirExec')" class="MOpcion">Publicados Proveedor</span>    
		    <span id="GDirExec2" onclick="Fejecutar(this.id,2,'GDirExec')" class="MOpcion">Rurales</span>   
		    <span id="GDirExec3" onclick="Fejecutar(this.id,2,'GDirExec')" class="MOpcion">Parcialmente Resueltas</span>
		    <span id="GDirExec4" onclick="Fejecutar(this.id,2,'GDirExec')" class="MOpcion">Gestión Manual</span>   
		    <span id="GDirExec5" onclick="Fejecutar(this.id,2,'GDirExec')" class="MOpcion">Resueltas Proveedor</span>
		</div>
		<!-- Caja donde se ejecutan las funcionalidades del Gestor de Direcciones Excepcionadas -->
		<div class="CEjecutar">
			<iframe id="EGDirExec" class="Ciframe" scrolling="auto"></iframe>
		</div>
	</div>
	<!-- Caja contenedora del menu y ventanas de ejecucion de las Configuraciones del Gestor de Direcciones Excepcionadas -->
	<div id="CGDirExecConf" class="cajaMenuIfra">
		<!-- Menú de las actividades que puede realizar en el Gestor de Direcciones Excepcionadas -->
		<div id="MenuGDirExecConf" class="CMenu">
		    <span id="GDirExecConf0" onclick="Fejecutar(this.id,3,'GDirExecConf')" class="MOpcion">Localidad</span>   
		    <span id="GDirExecConf1" onclick="Fejecutar(this.id,3,'GDirExecConf')" class="MOpcion">Publicacion/Historial</span>    
		</div>
		<!-- Caja donde se ejecutan las funcionalidades del Gestor de Direcciones Excepcionadas -->
		<div class="CEjecutar">
			<iframe id="EGDirExecConf" class="Ciframe" scrolling="auto"></iframe>
		</div>
	</div>
	<!-- Caja contenedora del menu y ventanas de ejecucion de las Funcionalidades del Gestor de BI -->
	<div id="CGBi" class="cajaMenuIfra">
		<!-- Menú de las actividades que puede realizar en el Gestor de BI -->
		<div id="MenuGBi" class="CMenu">
		    <span id="GBi0" onclick="Fejecutar(this.id,4,'GBi')" class="MOpcion">BI 1</span>   
		    <span id="GBi1" onclick="Fejecutar(this.id,4,'GBi')" class="MOpcion">BI 2</span>    
		</div>
		<!-- Caja donde se ejecutan las funcionalidades del Gestor de BI -->
		<div class="CEjecutar">
			<iframe id="EGBi" class="Ciframe" scrolling="auto"></iframe>
		</div>
	</div>
	<!-- Caja contenedora del menu y ventanas de ejecucion de las Configuraciones del Gestor de BI -->
	<div id="CGBiConf" class="cajaMenuIfra">
		<!-- Menú de las actividades que puede realizar en el Gestor de BI -->
		<div id="MenuGBiConf" class="CMenu">
		    <span id="GBiConf0" onclick="Fejecutar(this.id,5,'GBiConf')" class="MOpcion">BI Conf 1</span>   
		    <span id="GBiConf1" onclick="Fejecutar(this.id,5,'GBiConf')" class="MOpcion">BI Conf 2</span>    
		</div>
		<!-- Caja donde se ejecutan las funcionalidades del Gestor de BI -->
		<div class="CEjecutar">
			<iframe id="EGBiConf" class="Ciframe" scrolling="auto"></iframe>
		</div>
	</div>
</div>
<!-- Caja contenedora de los controles de las ventanas de configuracion de los gestores -->
<div id="CcierraVent">
	<span id="Ccierra" ></span>
 <!-- <span id="Ccierra" onclick="cerrarVent()"></span>   -->
</div>
<div id="TapaMensajes"></div>

<div id="dialogo-confirmar" title="Confirmacion">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Está seguro de cerrar la ventana?</p>
</div>

<div id="dialogo-salir" title="Salir">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Está seguro en salir de la página?</p>
</div>

</body>
</html>