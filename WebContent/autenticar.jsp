<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String error = request.getParameter("err");
%>
<head>
	<ms:setBundle basename="properties.mensajes" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<!-- Hoja de estilos del Gestor -->
	<link href="./Estilo/Index.css" rel="stylesheet" type="text/css" />
	<!-- Javascripts para manejo del Gestor -->
	<script type="text/javascript" src="./Script/Index.js"></script>
</head>
<body onload="Cerror('<%= error%>')">
<!-- Caja del Encabezado de la Página de arranque del Gestor de PNI -->
<div id="Encabezado">
	<span id="Tpagina"><ms:message key="Titulo_Sistema" /></span>
</div>
<!-- Menú de la Configuración de la página de arranque del Gestor PNI -->
<ul id="MenuIdPrin" class="Menu">
	<li class="MOpcionPrin">
		<span id="LogoMenu" title='<ms:message key="Titulo_MenuP" />'></span>
		<ul>
			<li class="SubMOpcionPrin" style="margin-left: 0px;">
				<span><ms:message key="Ayuda" /></span>
			</li>
		</ul>
	</li>
</ul>



<!-- Formulario para la carga de las credenciales -->
<form method="post" id="fichaUsuario" name="j_security_check" action="j_security_check">
	<h2 style="text-align: center"><ms:message key="Titulo_FormInicio" /></h2>
	<span>		
		<label style="margin-left: 15px;"><ms:message key="Etiqueta_Usuario" /></label>
		<input style="margin-left: 61px;" type="text" id="Tusuario" name="j_username" onfocus="Cerror('0')" size="20" maxlength="32" title='<ms:message key="Title_Usuario" />' />
	</span>
	<span>
		<label style="margin-left: 15px;"><ms:message key="Etiqueta_Clave" /></label>
		<input style="margin-left: 39px;" type="password" id="Tclave" name="j_password" onfocus="Cerror('0')" size="20" maxlength="32" title='<ms:message key="Title_Clave" />' />
	</span>
	<span style="padding: 10px 80px 10px 124px">		
		<input type="button" id="Bingresa" value='<ms:message key="Boton_Confirmar" />' onclick="ConfirmarCred()" title='<ms:message key="Title_Boton_Inicio" />' />
	</span>
	<p id="Merror"><ms:message key="Aviso_UsuCla_NoValida" /></p>
</form>

</body>
</html>