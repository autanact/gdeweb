<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="org.jboss.axis.AxisFault" %>
<%@ page import="co.net.une.www.svc.WSConsultarTablasGDEStub" %>
<%@ page import="co.net.une.www.svc.WSInsertarTablasGDEStub" %>
<%@ page import="co.net.une.www.svc.WSActualizarTablasGDEStub" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>

<%@page import="java.io.File"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>

<%@ page import="func.unes.gde.*" %>

<%


int codigosolicitud  = 0;
int departamento 	 = 1;
int municipio 		 = 2;
int direccionnatural = 3;
int nombrecontacto 	 = 4;
int telefonocontacto = 5;
int latitud 		 = 6;
int longitud 		 = 7;

//Array que almacenara la respuesta del WS       
ArrayList arrayrespuesta = new ArrayList();
String respuesta = "";
String getCodigoRespuesta="";
String  getDescripcionError="";

String root=session.getServletContext().getRealPath("/archivos_importados/");
java.io.File file;
java.io.File dir = new java.io.File(root);

String[] list = dir.list();

	if (list.length > 0) {

	for (int i = 0; i < list.length; i++) {
	  file = new java.io.File(root  + list[i]);
	  	  
	  // LEER ARCHIVO TEXTO EN UN DIRECTORIO
	  String jspPath = session.getServletContext().getRealPath("/archivos_importados");
	  String txtFilePath = jspPath+ '/'+list[i];
	  BufferedReader reader = new BufferedReader(new FileReader(txtFilePath));
	  StringBuilder sb = new StringBuilder();
	  String line;
	  
      // creamos el soporte y la peticion WS direxcepcionada
	  WSConsultarTablasGDEStub direxcepcionada = new WSConsultarTablasGDEStub();
	  WSConsultarTablasGDEStub.WSConsultarTablasGDERQ  direxcepcionadaRequest = new WSConsultarTablasGDEStub.WSConsultarTablasGDERQ();
	  WSConsultarTablasGDEStub.WSConsultarTablasGDERQType  direxcepcionadaRequestType = new WSConsultarTablasGDEStub.WSConsultarTablasGDERQType();
	  WSConsultarTablasGDEStub.WSConsultarTablasGDERS direxcepcionadaResponse = new WSConsultarTablasGDEStub.WSConsultarTablasGDERS();
	
	  while((line = reader.readLine())!= null) {
		  if(!line.startsWith("Codigo_Solicitud[|]Departamento")){
		    line = line.replaceAll("\"", "");
		    //System.out.println("********** Linea del archivo **********");
		  	//System.out.println("linea: " + line);

		    String[] campo = line.split("\\[\\|\\]", -1);
		    
		    //System.out.println("CodigoSolicitud: " + campo[codigosolicitud]);
		    //System.out.println("Departamento   : " + campo[departamento]);
		    //System.out.println("Municipio      : " + campo[municipio]);
		    //System.out.println("DireccionNatura: " + campo[direccionnatural]);
		    //System.out.println("NombreContacto : " + campo[nombrecontacto]);
		    //System.out.println("TelefonoContact: " + campo[telefonocontacto]);
		    //System.out.println("Latitud        : " + campo[latitud]);
		    //System.out.println("Longitud       : " + campo[longitud]);
		    
   			// establecemos el parametro de la invocacion 
			direxcepcionadaRequestType.setNombreTabla("direccion_excepcionada");
			direxcepcionadaRequestType.setNombreDataset("une_st");
			direxcepcionadaRequestType.setNombresCamposConsulta("codigo_solicitud");
			direxcepcionadaRequestType.setValoresCamposConsulta( campo[codigosolicitud] );
			direxcepcionadaRequestType.setNombreCamposResultado("codigo_solicitud|codigo_sistema|direccion_texto_libre|direccion_normalizada|estado_geor_type|fecha_ingreso_direccion|fecha_envio_proveedor|fecha_respuesta_proveedor|estado_excepcion_type|estado_gestion_type|comentario|comentario_proveedor");
					
			// invocamos al web service
			direxcepcionadaRequest.setWSConsultarTablasGDERQ(direxcepcionadaRequestType);
			
			direxcepcionada._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
		                                              
		    direxcepcionadaResponse = direxcepcionada.consultarTablasGDE(direxcepcionadaRequest);
		    
			respuesta = direxcepcionadaResponse.getWSConsultarTablasGDERS().getRegistros();
			getCodigoRespuesta = direxcepcionadaResponse.getWSConsultarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta();			 
			getDescripcionError = direxcepcionadaResponse.getWSConsultarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
			
			if( "OK".equals(getCodigoRespuesta) ) {
				
				WSActualizarTablasGDEStub actualizar = new WSActualizarTablasGDEStub();
				WSActualizarTablasGDEStub.WSActualizarTablasGDERQ  actualizarRequest = new WSActualizarTablasGDEStub.WSActualizarTablasGDERQ();
				WSActualizarTablasGDEStub.WSActualizarTablasGDERQType  actualizarRequestType = new WSActualizarTablasGDEStub.WSActualizarTablasGDERQType();
				WSActualizarTablasGDEStub.WSActualizarTablasGDERS actualizarResponse = new WSActualizarTablasGDEStub.WSActualizarTablasGDERS();
				
				actualizarRequestType.setNombreTabla("direccion_excepcionada");
				actualizarRequestType.setNombreDataset("une_st");
				actualizarRequestType.setNombreCampoClave("codigo_solicitud");
				actualizarRequestType.setValorCampoClave(campo[codigosolicitud]);
				actualizarRequestType.setNombresCamposAct("departamento|municipio|direccion_texto_libre|usuario_solicitud|telefonos|latitud|longitud");
				actualizarRequestType.setValoresCamposAct(campo[departamento]+"|"+campo[municipio]+"|"+campo[direccionnatural]+"|"+campo[nombrecontacto]+"|"+campo[telefonocontacto]+"|"+campo[latitud]+"|"+campo[longitud]);
				
				actualizarRequest.setWSActualizarTablasGDERQ(actualizarRequestType);
				
				actualizar._getServiceClient().getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.CHUNKED, Boolean.FALSE);
				
				actualizarResponse = actualizar.actualizarTablasGDE(actualizarRequest);
				
				getCodigoRespuesta = actualizarResponse.getWSActualizarTablasGDERS().getGisRespuestaProceso().getCodigoRespuesta();
				getDescripcionError = actualizarResponse.getWSActualizarTablasGDERS().getGisRespuestaProceso().getDescripcionError();
			} //Fin del condicional de CodigoRespuesta.
	     } //Fin del condicional de la linea del archivo.
	  }//Fin del Ciclo que lee el archivo.
	}
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Procesar</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
    <link href="../Estilo/jquery-ui.css" rel="stylesheet">
   
</head>
<body>

<div id="Lista" style="width: 1200px; height: 500px;">
<h1>PROCESAR</h1>
<p>&nbsp;</p>
<h2>El proceso fue generado de forma exitosa</h2>
<p>&nbsp;</p>
<a href="GDirExec2.jsp">Volver</a> 

</div>


<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>

</body>
</html>