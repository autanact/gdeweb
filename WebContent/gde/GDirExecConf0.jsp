<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.net.*"%>
<%@ page import="func.unes.gde.*" %>
<%@ page import="java.nio.charset.*" %>

<%

// Variales auxilares para la tabla localidades
int nombre_departamento=0;
int nombre_municipio=1;
int id=2;

String [] respuesta = new String[3];
String [] respuesta_m = new String[3];
String [] respuesta_loc = new String[3];

String [] respuesta_insertar = new String[2];
String [] respuesta_actualizar = new String[2];

int codigoRespuesta = 0;
int descripcionError = 1;
int registros = 2;

String existentes_loc = "";

request.setCharacterEncoding("UTF-8");

// Validar que el formulario FichaCargar  sido enviado //
if(request.getParameter("codigo_departamento") != null) {

	String[] values_codigo_municipio = request.getParameterValues("codigo_municipio");
	 	
	for (int v=0; v<(values_codigo_municipio.length); v++ ) {
    	 
		 try {
			 respuesta_loc = WServicios.consultartabla_cadena("ds_cd_localidad","une_st","codigo_departamento|nombre_departamento|codigo_municipio|nombre_municipio|gestionar_direcciones",request.getParameter("codigo_departamento")+"|"+values_codigo_municipio[v]+"|true","nombre_departamento|nombre_municipio|id|gestionar_direcciones");
			 if ( !respuesta_loc[registros].equals("0") ) {
				 respuesta_loc[registros] = respuesta_loc[registros].replace("[", "");
				 respuesta_loc[registros] = respuesta_loc[registros].replace("]", "");
				 String[] values_respuesta_loc = respuesta_loc[registros].split("\\;", -1);
				 existentes_loc = existentes_loc + "Localidad existente : <b>"+  values_respuesta_loc[0]+", " + values_respuesta_loc[1] + "</b><br>";
			 } else {
				 respuesta_insertar =  WServicios.insertartabla("ds_cd_localidad","une_st","codigo_departamento|nombre_departamento|codigo_municipio|nombre_municipio|gestionar_direcciones",request.getParameter("codigo_departamento")+"|"+values_codigo_municipio[v]+"|true") ;
			 }	
		 } catch (Exception e) {
			 System.err.println(e.toString());
		 }
	}
}

//Validar que el formulario FichaEliminar  sido enviado //
if(request.getParameter("id_localidad") != null) {
	
	String[] values_id_localidad = request.getParameterValues("id_localidad");
	
	for (int v=0; v<(values_id_localidad.length); v++ ) {
 	  
      try {   
			//se comenta la eliminacion logica	    
    	    //respuesta_actualizar =  WServicios.actualizartabla("ds_cd_localidad","une_st","id",values_id_localidad[v],"gestionar_direcciones","false");
			//se realiza eliminacion fisica
			respuesta_actualizar =  WServicios.eliminartabla("ds_cd_localidad","une_st","id",values_id_localidad[v]);			
    		
		 } catch (Exception e) {
			 System.err.println(e.toString());
		 }
	}
}
        // Array que almacenara la respuesta del WS para la Consulta Departamentos      
        ArrayList arrayrespuesta = new ArrayList();
    
        // Array que almacenara la respuesta del WS  WS para la Consulta Municipios 
        ArrayList arrayrespuesta_municipios = new ArrayList(); 
        
         // Array que almacenara la respuesta del WS  WS para la Consulta Localidades 
        ArrayList arrayrespuesta_localidades = new ArrayList(); 
         
        
		try {
	     	// creamos el soporte y la peticion WS Departamentos
			respuesta = WServicios.consultartabla_cadena("ds_carto_departamento","ds_cartografia","","","codigo_departamento|nombre");
			
	     	if (!respuesta[2].equals("0")) {
	     		arrayrespuesta = WServicios.ordenar_abc( WServicios.lista_registros( respuesta[2] ),1 );
			}	
			    
		   // creamos el soporte y la peticion WS Departamentos
		   // Verifica se cambio de departamento de la lista para crear el filtro de los municipios
			if(request.getParameter("codigo_departamento_aux") != null) {
				// Seleccionar los Municipios del Departamento seleccionado anteriormente
				respuesta_m = WServicios.consultartabla_cadena("ds_carto_municipio","ds_cartografia","ds_carto_departamento.codigo_departamento",request.getParameter("codigo_departamento_aux"),"codigo_municipio|nombre");
			} else {
				// Seleccionar los Municipios del Primer Departamento
				String rows = arrayrespuesta.get(0).toString();
	            String[]  values = rows.split("\\;", -1);
	        	respuesta_m = WServicios.consultartabla_cadena("ds_carto_municipio","ds_cartografia","ds_carto_departamento.codigo_departamento",values[0],"codigo_municipio|nombre");
			}
		   
		    if (!respuesta_m[2].equals("0")) {
				arrayrespuesta_municipios = WServicios.ordenar_abc( WServicios.lista_registros( respuesta_m[2] ),1);
		    }
			
			// creamos el soporte y la peticion WS localidades
			respuesta_loc = WServicios.consultartabla_cadena("ds_cd_localidad","une_st","gestionar_direcciones","true","nombre_departamento|nombre_municipio|id|gestionar_direcciones");
			if (!respuesta_loc[2].equals("0")) {
				arrayrespuesta_localidades = WServicios.ordenar_abc(WServicios.lista_registros( respuesta_loc[2] ),0);
			}
			
		 } catch (Exception e) {
			 System.err.println(e.toString());
		 }

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Localidad</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
    <link href="../Estilo/jquery-ui.css" rel="stylesheet">
</head>
<body>
<div id="Lista" style="height: 500px; width: 850px;">

<!-- Formulario para la carga de las credenciales -->
	<form method="post" id="FichaCarga" name="FichaCarga" style="top: 0px; width: 240px; margin: 5px;">
		<fieldset>
			<legend>Localidad</legend>
			<table style="margin: 5px;">
				<thead>
					<tr style="text-align: left">
						<th>Departamentos</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="vertical-align: top">
							<select name="codigo_departamento" id="codigo_departamento" >
								   						        
   						        <%  
   						     	if(!respuesta[2].equals("0")) {
	   						        for (int j=0;j<arrayrespuesta.size();j++){ 
	   						        	   String rows = arrayrespuesta.get(j).toString();
	   						        	   
	   						        	//   //System.out.println(arrayrespuesta.get(j).toString());
	   						        	   
	   						               String[]  values = rows.split("\\;", -1);
	   						               if(request.getParameter("codigo_departamento_aux") == null) { %>
	   						                <option value="<%=  values[0]+"|"+values[1]  %>" ><%  if(values[1].length()>20) { out.println(values[1].substring(0,20)); } else { out.println(values[1]); } %></option>
	   						           <%  } else { %>
		   						               <% if (request.getParameter("codigo_departamento_aux").equals(values[0]) ) {
		   						               %>
		   						                   <option  selected value="<%=  values[0]+"|"+values[1]  %>" ><%  if(values[1].length()>20) { out.println(values[1].substring(0,20)); } else { out.println(values[1]); } %></option> 
										       
										       <%  } else { %>
										           <option value="<%=  values[0]+"|"+values[1]  %>" ><%  if(values[1].length()>20) { out.println(values[1].substring(0,20)); } else { out.println(values[1]); } %></option> 
										       
										       <%  } %>
										<%  } %>
									<%  }
   						        }%>
								
							</select>
						</td>
					</tr>		
				</tbody>
			</table>
			<table>
				<thead>
					<tr style="text-align: left">
						<th>Municipios</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<select name="codigo_municipio" id="codigo_municipio" size=17 multiple="multiple">
							    <option value="0">----Seleccionar Todos----</option> 
								<%
								if (!respuesta_m[2].equals("0")) {	
									   for (int j=0;j<arrayrespuesta_municipios.size();j++){ 
	   						        	   String rows = arrayrespuesta_municipios.get(j).toString();
	   						               String[]  values = rows.split("\\;", -1);
	   						               %>
	                                       <option value="<%=  values[0]+"|"+values[1] %>"><%=  values[1] %></option> 
									<%  }
								} %>
								
							</select>
						</td>
					</tr>		
				</tbody>
			</table>
		</fieldset>
	
		<table>
			<tfoot>
				<tr>
					<td colspan=3 align="center">
					   <% if (respuesta_m.equals("0")) { %>
							<input type="button"  id="Bingresa" name="agregar_localidad" value="Agregar localidad" onclick="AgregaLocalidad()"  disabled />
					   <% } else { %>
					  		 <input type="button"  id="Bingresa" name="agregar_localidad" value="Agregar localidad" onclick="AgregaLocalidad()" />
					   <% }  %>
					</td>
				</tr>
			</tfoot>	
		</table>
	</form>
	
	
	<table id="Tabla" style="position: absolute; top: 5px; left: 280px;">
		<thead>
			<tr>
				<th colspan=3 id="tt">Localidades Activas</th>
			</tr>
			<tr style="text-align: left;">
				<th style="width: 160px">  Departamentos</th>
				<th style="width: 160px">  Municipios</th>

			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan=2>
				    <form method="post" id="FichaEliminar" name="FichaEliminar">
					<select size=10 multiple="multiple" class="Tabla codigo_departamento" style="width: 500px; height: 350px;" name="id_localidad" id="id_localidad">
						<option value="0" style="text-align: center;">----- Seleccionar todos -----</option>
						
						<% String nombre_departamento_actual=""; 
						  
						   if (!respuesta_loc[2].equals("0") )  {
						   
						   for (int j=0;j<arrayrespuesta_localidades.size();j++){ 
							   
				        	   String rows = arrayrespuesta_localidades.get(j).toString();
				               String[]  values = rows.split("\\;", -1);

				               %>

								<%  if ( !(values[nombre_departamento].equals(nombre_departamento_actual) ) ){ 
									      nombre_departamento_actual = values[nombre_departamento];%>
		   							      <optgroup class="optg" label="<%= values[nombre_departamento] %>" >
								          <option class="optgoptg1" value="<%= values[id] %>"><%= values[nombre_municipio] %></option>      
	   							<% } else { %>
								
								     <option class="optgoptg1" value="<%= values[id] %>"><%= values[nombre_municipio] %></option>
								
								<% } %>		
							    <% if ( j < (arrayrespuesta_localidades.size()-1) ) { 
							    	   String rows_aux = arrayrespuesta_localidades.get(j+1).toString();
						               String[]  values_aux = rows_aux.split("\\;", -1);
								    	if ( !( values_aux[nombre_departamento].equals(nombre_departamento_actual) ) ) { %>
								           </optgroup>
								        <% } %>
							    <% } else { %>	
							    	</optgroup>
							    <% } %>
							<% } 
						   
						   }%>	
					</select>
					</form>
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan=2 align="center">
				      <% if (respuesta_loc[2].equals("0")) { %>
							<input type="button"  id="Bingresa" value="Eliminar localidad" onclick="EliminarLocalidad()" disabled />
					   <% } else { %>
					  		<input type="button"  id="Bingresa" value="Eliminar localidad" onclick="EliminarLocalidad()"  />
					   <% }  %>
				</td>
			</tr>
		</tfoot>	
	</table>
	
</div>

<div id="dialogo-eliminar" title="Confirmacion">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Desea continuar con la eliminacion?</p>
</div>
<%if (!existentes_loc.equals("")) {%>
<div id="dialogo-existentes" title="Informacion">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><%= existentes_loc %></p>
</div>
<%}%>
</body>
<!-- Javascripts para manejo del Gestor -->
<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>
<script type="text/javascript" src="../Script/GDirExecConf0.js"></script>


</html>