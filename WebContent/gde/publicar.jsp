<%@page import="co.net.une.ejb43.util.ServiciosUtil"%>
<%@page import="co.net.une.www.util.GDEException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.io.*" %>

<%@ page import="func.unes.gde.*" %>

<%


String respuesta[] = new String[5];
int codigoRespuesta  	= 0;
int descripcionError 	= 1;
int contador_registros  = 2;
int nombre_archivo_biz  = 3;
int archivo_numero_biz  = 4 ;

String nombrearchivo = "";

String session_usuario = (String)session.getAttribute("session_usuario");
//System.out.println("La ruta del archivo es: " + rb.getString("file_path"));
//respuesta = publicar.Publicar(session_usuario, getServletContext().getRealPath("/archivos_importados/"));
//respuesta = publicar.Publicar(session_usuario, getServletContext().getRealPath("ARCHIVO_BIZ_NOMBRE_TMP.BIZ"));

	Publicar p = new Publicar();
try {


	p.publicarDirExc(session_usuario, ServiciosUtil.getDirectorioGuardarLote());
}catch (GDEException e){
	
}
////System.out.println( "*************** Respuesta es igual a: " + respuesta );
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Publicar</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
    <link href="../Estilo/jquery-ui.css" rel="stylesheet">

</head>
<body>

<div id="Lista" style="width: 1200px; height: 500px;">

<% if (p.getCodigoRespuesta().equals("KO")) { %>
   <h2>Error: <%= respuesta[descripcionError] %></h2>
<% } else { %>
	<% if ( p.getArchivoNumeroBiz() ==  0) { %>
					
				    <h1>PUBLICAR</h1>
					<p>&nbsp;</p>
					<h2>El lote de direcciones no genero ningun registro para ser procesado</h2>
					<p>&nbsp;</p>
					<a href="GDirExec0.jsp">Volver</a> 
	               
	<% } else {    %>
	                <h1>PUBLICAR</h1>
					<p>&nbsp;</p>
					<h2>El lote de direcciones fue generado de forma exitosa, cantidad de registros: <%= p.getCantidadRegistros() %></h2>
					<p>&nbsp;</p>
					<p>Hacer click en el siguiente link para descargar el archivo generado </p>
									
					<% for (int a=1;a<=p.getArchivoNumeroBiz() ;a++){ 
						    nombrearchivo = p.getNombrArchivoBiz();
							//nombrearchivo = respuesta[nombre_archivo_biz].replace(".", "_"+a+".") ; ;
					%>
					
					( <a href="../<%= nombrearchivo %>"><%= nombrearchivo %></a> ) </br>
					<% } %>
					<p>&nbsp;</p>
					<a href="GDirExec0.jsp">Volver</a> 
	               <%
	} 
} %> 
</div>

<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>

</body>
</html>