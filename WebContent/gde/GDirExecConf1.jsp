<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.util.Date" %>

<%@ page import="func.unes.gde.*" %>
<%@ page import="func.unes.gde.motores.*" %>

<%

 //    getServletContext().
  //   sc.getAttribute("attributeName");

%>

<%

String session_usuario = (String)session.getAttribute("session_usuario");

String respuesta_job = "" ;

String [] respuesta_actualizar = new String[2];

String [] respuesta_insertar = new String[2];


//Validar que el formulario FichaPub  sido enviado //
if(request.getParameter("tamanno_lote") != null) {
	
	String[] values_dias = request.getParameterValues("dias");
	String dias = "";
	
	for (int v=0; v<(values_dias.length); v++ ) {
 	    
 	   dias = dias+values_dias[v];
 	   if (v < values_dias.length-1) { dias = dias+ ","; }
 	}
	

		 try {  
			    String hora_inicial =  request.getParameter("hora_inicial")+request.getParameter("min_inicial");
			    String hora_final =  request.getParameter("hora_final")+request.getParameter("min_final");
			    String periodicidad =  request.getParameter("hora_periocidad")+request.getParameter("min_periocidad");
			    String tamanno_lote =  request.getParameter("tamanno_lote");
			    
			    respuesta_actualizar =  WServicios.actualizartabla("ds_cd_periodicidad_direcciones","une_st","id",request.getParameter("id"),"hora_inicial|hora_final|periodicidad|dias|tamanno_lote",hora_inicial+"|"+hora_final+"|"+periodicidad+"|"+dias+"|"+tamanno_lote);
	    	 
			    if (respuesta_actualizar[0].equals("OK"))   {
		
			    	quartz_configuracion.agendarJobs(0, session_usuario, getServletContext());
			    	respuesta_job =  "OK";
			    }

		 } catch (Exception e) {
			 //System.out.println("****** Respuesta del Job ******");
			 //System.out.println("respuesta_job: " + respuesta_job);
			 System.err.println(e.toString());
		 }
}



//Validar que el formulario FichaHis  sido enviado //
if(request.getParameter("dias_historial") != null) {
		 try {   
			 
			 respuesta_actualizar =  WServicios.actualizartabla("ds_cd_envio_direxc_historico","une_st","id",request.getParameter("id"),"dias",request.getParameter("dias_historial"));
		 
			quartz_configuracion.agendarJobs(0, session_usuario, getServletContext());
		 } catch (Exception e) {
			 System.err.println(e.toString());
		 }
}


String respuesta_p[] = new String[6];


int codigoRespuesta = 0;
int descripcionError = 1;
int registros = 2;

// Leer tabla  ds_cd_periodicidad_direcciones
try {  
	// creamos el soporte y la peticion WS ds_cd_periodicidad_direcciones
	respuesta_p = WServicios.consultartabla_cadena("ds_cd_periodicidad_direcciones","une_st","","","hora_inicial|hora_final|periodicidad|dias|tamanno_lote|id");
	
} catch (Exception e) {
	 System.err.println(e.toString());
}


if (respuesta_p[codigoRespuesta].equals("KO")) {
	respuesta_p[registros] = "[0630;1630;0030;LU,MA,MI,JU,VI;500;42]";
	%>
    <h2>Error: <%=  respuesta_p[descripcionError]  %></h2>
<% } else { 
	if (respuesta_p[registros].equals("0")) { 
		 try {  
			 
		    // Insertar el primer registro de la tabla ds_cd_periodicidad_direcciones
		 	respuesta_insertar =  WServicios.insertartabla("ds_cd_periodicidad_direcciones","une_st","hora_inicial|hora_final|periodicidad|dias|tamanno_lote","0630|1630|0000|LU,MA,MI,JU,VI|500") ;
				
			// Consultar el Primer Registro ds_cg_envio_direxc_historico
			respuesta_p = WServicios.consultartabla_cadena("ds_cd_periodicidad_direcciones","une_st","","","hora_inicial|hora_final|periodicidad|dias|tamanno_lote|id");

			
		 } catch (Exception e) {
			 System.err.println(e.toString());
		 }
	}
}


respuesta_p[registros] = respuesta_p[registros].replace("[", "");
respuesta_p[registros] = respuesta_p[registros].replace("]", "");


String[] values_periodicidad_direcciones = respuesta_p[registros].split("\\;", -1);

//Leer tabla  ds_cd_envio_direxc_historico
String respuesta_h[] = new String[3];

try {  
	
	    // creamos el soporte y la peticion WS ds_cd_envio_direxc_historico
	    respuesta_h = WServicios.consultartabla_cadena("ds_cd_envio_direxc_historico","une_st","","","id|dias|fecha_inicial_historico");

		
} catch (Exception e) {
	 System.err.println(e.toString());
}


respuesta_h[registros] = respuesta_h[registros].replace("[", "");
respuesta_h[registros] = respuesta_h[registros].replace("]", "");

String[] values_envio_direxc_historico = respuesta_h[registros].split("\\;", -1);

//// VALIDAR LA EXISTENCIA DE HISTORIAL 
if (respuesta_h[codigoRespuesta].equals("KO")) { %>
<h2>Error: <%= respuesta_h[descripcionError] %></h2>
<% } else { 
	  Date fecha_hora= new Date();
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	if (respuesta_h[registros].equals("0")) { 
		
		 try {  
		    // Insertar el primer registro de la tabla ds_cg_envio_direxc_historico
		  
			respuesta_insertar =  WServicios.insertartabla("ds_cd_envio_direxc_historico","une_st","dias|fecha_inicial_historico","365|"+format.format(fecha_hora)) ;
			
			// Consultar el Primer Registro ds_cg_envio_direxc_historico
			respuesta_h = WServicios.consultartabla_cadena("ds_cd_envio_direxc_historico","une_st","","","id|dias|fecha_inicial_historico");
         
		 } catch (Exception e) {
			 System.err.println(e.toString());
		 }	
	}else{
		respuesta_h = WServicios.actualizartabla("ds_cd_envio_direxc_historico","une_st", "id", values_envio_direxc_historico[0], "fecha_inicial_historico", format.format(fecha_hora));
		
	}
} 



%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Publicación/Historial</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
	<link href="../Estilo/jquery-ui.css" rel="stylesheet">

</head>
<body>
<!-- Formulario para la carga de las Configuraciones -->


<form method="post" name="FichaPub" id="FichaPub" style="width: 900px;">
	<fieldset>
	  <legend>Dias/Horario de Gestión
		</legend><table width="852" style="width: 100%; margin: 0px;">
			<thead>
				<tr>
					<th width="30" style="width: 30px"></th>
					<th width="28" style="width: 20px">L</th>
					<th width="28" style="width: 20px">M</th>
					<th width="28" style="width: 20px">M</th>
					<th width="28" style="width: 20px">J</th>
					<th width="28" style="width: 20px">V</th>
					<th width="28" style="width: 20px">S</th>
					<th width="28" style="width: 20px">D</th>
					<th width="104" style="width: 30px;  width: 100px;"></th>
					<th width="88" align="right" style="width: 80px;">HH: MM</th>
					<th style="text-align: left; width: 80px;">&nbsp;</th>
					<th width="85" style="text-align: left; padding-left: 40px">&nbsp;</th>
					<th width="68" align="right" style="width: 60px;">HH: MM</th>
					<th width="88" style="text-align: left; width: 80px;">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td rowspan=2>Dias</td>
					<td rowspan=2>
						<input type="checkbox" value="LU" name="dias" id="dias_lu" 
						<%
						if( values_periodicidad_direcciones[3].indexOf("LU")>=0 ){
							out.print("checked");
						} %>>
					</td>
					<td rowspan=2>
						<input type="checkbox" value="MA" name="dias" id="dias_ma" 
						<% if( values_periodicidad_direcciones[3].indexOf("MA")>=0 ){
							out.print("checked");
						} %>>
					</td>
					<td rowspan=2>
						<input type="checkbox" value="MI" name="dias" id="dias_mi" 
						<% if( values_periodicidad_direcciones[3].indexOf("MI")>=0 ){
							out.print("checked");
						} %>>
					</td>
					<td rowspan=2>
						<input type="checkbox" value="JU" name="dias" id="dias_ju"
						<% if( values_periodicidad_direcciones[3].indexOf("JU")>=0 ){
							out.print("checked");
						} %>>
					</td>
					<td rowspan=2>
						<input type="checkbox" value="VI" name="dias" id="dias_vi" 
						<% if( values_periodicidad_direcciones[3].indexOf("VI")>=0 ){
							out.print("checked");
						} %>>
					</td>
					<td rowspan=2>
						<input type="checkbox" value="SA" name="dias" id="dias_sa"
						<% if( values_periodicidad_direcciones[3].indexOf("SA")>=0 ){
							out.print("checked");
						} %>>
					</td>
					<td rowspan=2>
						<input type="checkbox" value="DO" name="dias" id="dias_do"
						<% if( values_periodicidad_direcciones[3].indexOf("DO")>=0 ){
							out.print("checked");
						} %>>
					</td>
					<td colspan="2" rowspan=2 align="right" style="padding-left: 30px;">
						Hora Inicial &nbsp;
						<input type="text" maxlength=2 size=2 value="<%= values_periodicidad_direcciones[0].substring(0,2) %>" style="width: 15px;"  onBlur="soloIntervalos(this)" onkeypress="return soloNumeros(event);" onfocus="campo_activo('hora_inicial',23)" name="hora_inicial" id="hora_inicial" >:
					<input type="text" maxlength=2 size=2 value="<%= values_periodicidad_direcciones[0].substring(2) %>" style="width: 15px;"  onBlur="soloIntervalos(this)" onkeypress="return soloNumeros(event);" onfocus="campo_activo('min_inicial',59)" name="min_inicial" id="min_inicial"></td>
					<td width="179" style="padding-left: 0px; padding-bottom: 0px;"><input type="button" class="Incrementa" onClick="incrementar();"></td>
					<td colspan="2" rowspan=2 align="right" style="padding-left: 30px;">Hora Final &nbsp;
					  <input type="text" maxlength=2 size=2 value="<%= values_periodicidad_direcciones[1].substring(0,2) %>" style="width: 15px;" onBlur="soloIntervalos(this)" onkeypress="return soloNumeros(event);" onfocus="campo_activo('hora_final',23)" name="hora_final" id="hora_final" >
					  :
                        <input type="text" maxlength=2 size=2 value="<%= values_periodicidad_direcciones[1].substring(2) %>" style="width: 15px;" onBlur="soloIntervalos(this)" onkeypress="return soloNumeros(event);" onfocus="campo_activo('min_final',59)"  name="min_final" id="min_final">			      </td>
				  <td style="padding-left: 0px; padding-bottom: 0px;"><input type="button" class="Incrementa" onClick="incrementar();"></td>
			    </tr>
				<tr>
				  <td style="padding-left: 0px;"><input type="button" class="Decrementa" onClick="decrementar();"></td>
					<td style="padding-left: 0px;"><input type="button" class="Decrementa" onClick="decrementar();"></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
	<fieldset>
		<legend>Periodicidad</legend>
		<table style="width: 780px; margin: 0px;">
			<thead>
				<tr>
					<th style="width: 150px"></th>
					<th align="right" style="width: 80px;">HH: MM</th>
				  <th style="width: 20px"></th>
					<th style="text-align: left; padding-left: 40px">&nbsp;</th>
					<th style="text-align: left; padding-left: 40px">&nbsp;</th>
					<th style="text-align: left; padding-left: 40px">&nbsp;</th>
					<th style="text-align: left; padding-left: 40px">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="2" rowspan=2 align="right" style="padding-left: 30px;">Tiempo de Gestión&nbsp; 
					<input type="text" maxlength=2 size=2 value="<%= values_periodicidad_direcciones[2].substring(0,2) %>" style="width: 15px;" name="hora_periocidad"
                        id="hora_periocidad" onBlur="soloIntervalos_p(this)" onkeypress="return soloNumeros(event);" onfocus="campo_activo_p('hora_periocidad')">:
					<input type="text" maxlength=2 size=2 value="<%= values_periodicidad_direcciones[2].substring(2) %>" style="width: 15px;" name="min_periocidad" 
                        id="min_periocidad"  onBlur="soloIntervalos_p(this)" onkeypress="return soloNumeros(event);" onfocus="campo_activo_p('min_periocidad')"></td>
					<td style="padding-left: 0px; padding-bottom: 0px;">
					   <input type="button" class="Incrementa" onClick="incrementar_p();">
					</td>
					<td rowspan=2 style="padding-left: 40px">&nbsp;</td>
					<td colspan="2" rowspan=2 align="right" style="padding-left: 40px"># Direcciones por lote &nbsp;
                    <input type="text" maxlength=3 size=5 value="<%= values_periodicidad_direcciones[4] %>" style="width: 35px;" name="tamanno_lote" id="tamanno_lote"
                    onBlur="soloIntervalos_l(this)" onkeypress="return soloNumeros(event);" >                    </td>
					<td style="padding-left: 0px; padding-bottom: 0px;">
					<input type="button" class="Incrementa" onClick="incrementar_l();"></td>
				</tr>
				<tr>
					<td style="padding-left: 0px;">
						<input type="button" class="Decrementa" onClick="decrementar_p();">
					</td>
					<td style="padding-left: 0px;">
                        <input type="button" class="Decrementa" onClick="decrementar_l();">
                    </td>
				</tr>
			</tbody>
		</table>
	</fieldset>
	<table style="width: 100%; margin: 0px;">
		<tfoot>
			<tr>
				<td width="720" style="text-align: right;">
					<input name="id" type="hidden" id="id" value="<%= values_periodicidad_direcciones[5] %>">
                    <input type="button"  id="confirmar_configuracion" value="Confirmar configuracion" onClick="javascript:ConfirmarCon();" title="Guardar la Configuracion" />
				</td>
			</tr>
		</tfoot>	
	</table>
    
    </form>
    
    <form method="post" name="FichaHis" id="FichaHis" style="width: 900px;">
    <fieldset>
    <legend>Historial</legend>
		<table style="width: 780px; margin: 0px;">
			<thead>
				<tr>
					<th style="width: 80px"></th>
					<th style="text-align: left; width: 50px;">&nbsp;</th>
					<th style="width: 20px"></th>
					<th style="text-align: left; padding-left: 40px">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="2" rowspan=2 align="right" style="padding-left: 30px;"># de dias
					  
			&nbsp;
			
			 <input type="hidden" value="<%= values_envio_direxc_historico[0] %>"  name="id" >
            
	         <input type="text" maxlength=3 size=5 value="<%= values_envio_direxc_historico[1] %>" style="width: 35px;" name="dias_historial" id="dias_historial"
                         onBlur="soloIntervalos_d(this)" onkeypress="return soloNumeros(event);">                    </td>
					<td style="padding-left: 0px; padding-bottom: 0px;">
						<input type="button" class="Incrementa" onClick="incrementar_d();">
					</td>
					<td rowspan=2 style="padding-left: 40px"><span style="padding-left: 0px;">
					  <input type="button"  id="guardar" value="Guardar" onClick="javascript:Guardar();" title="Guardar" />
					</span></td>
				</tr>
				<tr>
					<td style="padding-left: 0px;">
						<input type="button" class="Decrementa" onClick="decrementar_d();"></td>
				</tr>
			</tbody>
		</table>
	</fieldset>
	</form>


<!-- Javascripts para manejo del Gestor -->
<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>
<script type="text/javascript" src="../Script/GDirExecConf1.js"></script>

<%if (!respuesta_job.equals("")) {%>
<div id="dialogo-informacion" title="Informacion">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span><%= respuesta_job %></p>
</div>
<% }%>

 
</body>
</html>