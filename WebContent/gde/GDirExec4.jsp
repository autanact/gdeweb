<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="co.net.une.ejb43.util.ServiciosUtil"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="func.unes.gde.*" %>

<%

request.setCharacterEncoding("UTF-8");

String [] respuesta_actualizar = new String[2];
String [] respuesta_relacion = new String[2];
String [] consultaId = new String[3];
String idRegistro = null;

if(request.getParameter("direccion_texto_libre") != null) {
	try {
		 respuesta_actualizar =  WServicios.actualizartabla("direccion_excepcionada","une_st","codigo_solicitud",request.getParameter("codigo_solicitud"),"direccion_texto_libre|contacto|telefonos|comentario|estado_excepcion_type|estado_gestion_type",request.getParameter("direccion_texto_libre")+"|"+request.getParameter("contacto")+"|"+request.getParameter("telefonos")+"|"+request.getParameter("comentario")+"|"+request.getParameter("estado_excepcion_type")+"|"+request.getParameter("estado_gestion_type"));
		 consultaId = WServicios.consultartabla_cadena("direccion_excepcionada","une_st","codigo_solicitud",request.getParameter("codigo_solicitud"), "id");
		 idRegistro = consultaId[2].replace("[", "").replace("]", "");
		 respuesta_relacion = WServicios.eliminarRelacion("une_st", "direccion_excepcionada", idRegistro, "ds_cd_lote_direxc_lt");
 	} catch (Exception e) {
		 System.err.println(e.toString());
	}	
}

/// Manejo de Paginacion
int tamano_pagina=Integer.parseInt(ServiciosUtil.getTamanoPaginador()) ;
int pagina           = 0;
int cantidad_paginas = 0;
int bloque           = 0;
String filtro = null;
String codigo = null;
String desde = null;
String hasta = null;

if (request.getParameter("pagina") != null) { pagina=Integer.parseInt(request.getParameter("pagina")); }
if (request.getParameter("bloque") != null) { bloque=Integer.parseInt(request.getParameter("bloque")); }
if (request.getParameter("tipo_filtro") != null) { filtro=request.getParameter("tipo_filtro"); }
if (request.getParameter("codigo") != null) { codigo=request.getParameter("codigo"); }
if (request.getParameter("desde") != null) { desde=request.getParameter("desde"); }
if (request.getParameter("hasta") != null) { hasta=request.getParameter("hasta"); }

/// Parametros de Consulta
String nombreTabla           = "direccion_excepcionada";
String nombreDataset         = "une_st";
String nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type";
String valoresCamposConsulta = "{INEXISTENTE;INCOMPLETA}|RESUELTA PROVEEDOR";
String nombreCamposResultado = "codigo_solicitud|codigo_sistema|direccion_texto_libre|direccion_normalizada|estado_geor_type|fecha_ingreso_direccion|fecha_envio_proveedor|fecha_respuesta_proveedor|estado_excepcion_type|estado_gestion_type|comentario|comentario_proveedor";

/// Control de Respuesta del Servicio de Consulta
ArrayList arrayrespuesta = new ArrayList();
String[] respuesta   = new String[4];
int codigoRespuesta  = 0;
int descripcionError = 1;
int registros        = 2;
int totalResultados  = 3;
String privilegio = null;

try {
	// Filtro //
	if(filtro != null) {
		if( filtro.equals("0") ) {
		    nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type|codigo_solicitud";
		    valoresCamposConsulta = "{INEXISTENTE;INCOMPLETA}|RESUELTA PROVEEDOR|" + codigo;
		}
		
		String fecha = "[" + desde + " 00:00:00;" + hasta + " 23:59:59]" ;
		
		if( filtro.equals("1") ) {
			nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type|fecha_ingreso_direccion";
			valoresCamposConsulta = "{INEXISTENTE;INCOMPLETA}|RESUELTA PROVEEDOR|" + fecha ;
		}
		if( filtro.equals("2") ) {
			nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type|fecha_envio_proveedor";
			valoresCamposConsulta = "{INEXISTENTE;INCOMPLETA}|RESUELTA PROVEEDOR|" + fecha ;
		}
		if( filtro.equals("3") ) {
			nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type|fecha_respuesta_proveedor";
			valoresCamposConsulta = "{INEXISTENTE;INCOMPLETA}|RESUELTA PROVEEDOR|" + fecha ;
		}
		if( filtro.equals("4") ) {
			nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type|fecha_envio_sistema";
			valoresCamposConsulta = "{INEXISTENTE;INCOMPLETA}|RESUELTA PROVEEDOR|" + fecha ;
		}
		//System.err.println("La fecha es: " + fecha);
	} else { // Sin Filtro //
		nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type";
		valoresCamposConsulta = "{INEXISTENTE;INCOMPLETA}|RESUELTA PROVEEDOR";
	}
	
  //Llamado a la WS WServiciosURN.consultartabla_URN 
  respuesta        = WServiciosURN.consultartabla_URN(tamano_pagina,pagina,nombreTabla,nombreDataset,nombresCamposConsulta,valoresCamposConsulta, nombreCamposResultado);
  arrayrespuesta   = WServicios.lista_registros( respuesta[registros] );
  cantidad_paginas = WServiciosURN.catidad_paginas(Integer.parseInt(respuesta[totalResultados]), tamano_pagina); 
 
} catch (Exception e) {
	 System.err.println(e.toString());
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta http-equiv="cache-control" content="max-age=0" /> 
<meta http-equiv="cache-control" content="no-cache" /> 
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<title>Gestión Manual</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
    <link href="../Estilo/jquery-ui.css" rel="stylesheet">

</head>
<body>

<div id="Lista" style="width: 1200px;  height: auto;">
<% if (respuesta[codigoRespuesta].equals("KO")) { %>
   <h2>Error: <%= respuesta[descripcionError] %></h2>
<% } else { %>
	<% if (respuesta[registros].equals("0")) { %>
				<h2>No se encontraron registros</h2>
	<% }  %>
	<form method="post" id="FichaRegistros" name="FichaRegistros" >
		<table id="Tabla">
			<thead>
				<tr>
					<th colspan="12" align="left" id="tt" style="text-align:left;">Detalle Dirección Excepcionada</th>
				</tr>
				<tr>
					<th class="ccheck">
						<input type="checkbox"  id="selectall" name="selectall" title="Seleccionar todos los proyectos para su procesamiento">
					</th>
					<th style="width: 120px" id="tt">Código Solicitud</th>
					<th style="width: 180px" id="tt">Dirección Natural</th>
					<th style="width: 180px" id="tt">Dirección Normalizada</th>
					<th style="width: 180px" id="tt">Estado Georreferenciación</th>
					<th style="width: 180px" id="tt">Fecha Ingreso Dirección</th>
	                <th style="width: 180px" id="tt">Fecha Envío Proveedor</th>
	                <th style="width: 140px" id="tt">Fecha Respuesta Proveedor</th>
	                <th style="width: 140px" id="tt">Estado Excepción</th>
	                <th style="width: 100px" id="tt">Estado Gestión</th>
	                <th style="width: 150px" id="tt">Comentario</th>
		            <th style="width: 150px" id="tt">Comentario Proveedor</th>
				</tr>
			</thead>
			
			
			  <%   if (!respuesta[registros].equals("0") )  { %>
				<%  for (int j=0;j<arrayrespuesta.size();j++){ 
		        	   String rows = arrayrespuesta.get(j).toString();
		               String[]  values = rows.split("\\;", -1);
		               %>
		              
		               <tr id="tr">
						<td class="ccheck">
							<input type="checkbox" name="codigo_solicitud[]" value="<%= values[0] %>" class="codigo_solicitud" >
						</td>
						<td style="width: 110px"><%= values[0] %></td>
						<td style="width: 176px"><%= values[2] %></td>
						<td style="width: 230px"><%= values[3] %></td>
						<td style="width: 112px; text-align: center;"><%= values[4] %></td>
						<td><%= values[5] %></td>
			            <td><%= values[6] %></td>
			            <td><%= values[7] %></td>
			            <td><%= values[8] %></td>
			            <td><%= values[9] %></td>
			            <td><%= values[10] %></td>
			            <td><%= values[11] %></td>
					    </tr>
			    <% } %>
			   <% } %>
	        <tr>
					<td style="text-align: center; background-color:#FFF; border:#FFF;" colspan="12">
					<%  
					int ultima_pagina =0;
					    if (cantidad_paginas>1){ 
					    	if( cantidad_paginas>10 && bloque>0 ) { %>
                               <a href="GDirExec4.jsp?tipo_filtro=<%= (filtro) %>&pagina=<%= ((bloque*10)-1) %>&bloque=<%= (bloque-1) %>&desde=<%= (desde) %>&hasta=<%= (hasta) %>&codigo=<%= (codigo) %> "> Anterior </a> &nbsp;&nbsp;	 
                            <% 
                            }
						     for (int j=0;j<cantidad_paginas;j++){ 
		                         if( cantidad_paginas>10 ) { 
		                        	 if ( j>=bloque*10 && j <(bloque*10)+10 )  { 
		                                  if ( j==pagina ) { %>
		                        	 		 <b><%= (j+1) %></b> &nbsp;&nbsp;	 
		                                 <% } else  { %> 
		                        	   		 <a href="GDirExec4.jsp?tipo_filtro=<%= (filtro) %>&pagina=<%= (j) %>&bloque=<%= (bloque) %>&desde=<%= (desde) %>&hasta=<%= (hasta) %>&codigo=<%= (codigo) %> " > <%= (j+1) %> </a> &nbsp;&nbsp;	 
		                                 <% } %>
		                                 <% ultima_pagina = j;
		                              }
		                         } else {
		                              if ( j==pagina ) { %>
		                        	 		 <b><%= (j+1) %></b> &nbsp;&nbsp;	 
		                                 <% } else  { %> 
		                        	   		 <a href="GDirExec4.jsp?tipo_filtro=<%= (filtro) %>&pagina=<%= (j) %>&desde=<%= (desde) %>&hasta=<%= (hasta) %>&codigo=<%= (codigo) %> "> <%= (j+1) %> </a> &nbsp;&nbsp;		 
		                              <% } %>	 
		                         <%}%>
		                  <% }
						     if( cantidad_paginas>10 ) { 
							     if ( (cantidad_paginas - ultima_pagina) > 10 ) { %>
		                                 <a href="GDirExec4.jsp?tipo_filtro=<%= (filtro) %>&pagina=<%= (ultima_pagina+1) %>&bloque=<%= (bloque+1) %>&desde=<%= (desde) %>&hasta=<%= (hasta) %>&codigo=<%= (codigo) %> "> Siguiente </a> &nbsp;&nbsp;	 
		                          <% 
		                         }
						     }
					     } else {%>
					      &nbsp;
	                  <% } %>   
	                </td>
			</tr>
	        <tr>
					<td style="text-align: right; background-color:#FFF; border:#FFF;" colspan="12">
						
						<% privilegio = (String)session.getAttribute("session_privilege");
						   if( (Integer.parseInt(  session.getAttribute("session_nivel").toString() )  < 3) && (!privilegio.equals("consultas")) )  {%>
							<input  type="hidden" name ="enviar_publicar" value="enviar_publicar">
							<input  id="enviar" type="button"  value="Enviar CRM" <% if (respuesta[registros].equals("0")) {   out.println("disabled");  }%>/>
							<input  id="editar" type="button"  value="Editar Direccion" <% if (respuesta[registros].equals("0")) {   out.println("disabled");  }%>/>
					   <% } %>  
					   
						<input  id="filtro" type="button"  value="Filtrar" <% if (respuesta[registros].equals("0")) {   out.println("disabled");  }%>/>
						<input  id="actualizar" type="button"  value="Actualizar" />
	                
					</td>
			</tr>
	        
		</table>
		
	</form>
	
<% } %> 
</div>

<div id="dialogo-guardar" title="Guardar">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;">
  </span>¿Está seguro de guardar los cambios realizados?</p>
</div>



<div id="dialogo-editar" title="Editar Direccion">
 <div id="jspformulario" >
	 
 </div>
</div>

<div id="dialogo_filtrar" title="Filtrar">
<form action="GDirExec4.jsp" method="post" name="form_filtro" id="form_filtro">
  <p>Categoria</p>
     <select name="tipo_filtro" id="tipo_filtro"  onClick="seleccionar_filtro()">
        
         <option value="0">Código Solicitud</option>      
         <option value="1">Fecha ingreso Dirección </option>
         <option value="2">Fecha Envío Proveedor </option>
         <option value="3">Fecha Respuesta Proveedor </option>
         <!-- option value="4">Fecha Envío Sistema </option-->   
    </select>
    
    <div id="filtro_codigo">
    <p>Codigo</p>
    <input name="codigo" id="codigo" type="text">
    </div>
    <div id="filtro_fecha" style="display:none;">
      <p>&nbsp;</p>
      <table width="100%" border="0">
      <tr>
        <td>Desde <input name="desde" id="desde" type="text"></td>
        <td>Hasta <input name="hasta" id="hasta"  type="text"></td>
      </tr>
    </table>
    </div>
 </form>   
</div>


<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>
<script type="text/javascript" src="../Script/funciones_comunes.js"></script>
<script type="text/javascript" src="../Script/GDirExec4.js"></script>

</body>
</html>