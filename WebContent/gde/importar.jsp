<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.*" %>
<%@ page import="java.io.*" %> 

<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.util.Date" %>

<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.*" %>
<%
String usuario =request.getRemoteUser();

Date fecha_hora= new Date();
SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
String nombreArchivo= usuario+"_"+format.format(fecha_hora)+".csv";

String error = "NO";

String destination = "/archivos_importados";
String destinationRealPath = application.getRealPath( destination );

DiskFileItemFactory factory = new DiskFileItemFactory();
factory.setSizeThreshold( 1024 );

factory.setRepository( new File( destinationRealPath ) );

ServletFileUpload uploader = new ServletFileUpload( factory );

try
{
        List items = uploader.parseRequest( request );
        Iterator iterator = items.iterator();
       
        while( iterator.hasNext() )
        {
                FileItem item = (FileItem) iterator.next();
               // File file = new File( destinationRealPath, item.getName() );
                File file = new File( destinationRealPath, nombreArchivo );
                item.write( file );
        }
}
catch( FileUploadException e )
{
       // out.write( "<p>FileUploadException was thrown..." + e.getMessage() + "</p>" );
        error = "<p>Error : <b>" + e.getMessage() + "</b></p>";
}

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Importar</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="Lista" style="width: 1200px; height: 500px;">
	<h1>IMPORTAR</h1>
	<% if(error.equals("NO"))  { %>
		<p>&nbsp;</p>
		<h2>El proceso importar fue generado de forma exitosa</h2>
		<p>&nbsp;</p>
		<p>Hacer click en el siguiente link para descargar el archivo importado ( <a href="../archivos_importados/<%= nombreArchivo %>"><%= nombreArchivo %></a> )</p>
		<p>&nbsp;</p>
	<%  } else { %>
	    <p>&nbsp;</p>
		<h2><%= error %></h2>
		<p>&nbsp;</p>
		
	<%  }%>
	<a href="GDirExec2.jsp">Volver</a> 
</div>
</body>
<!-- Javascripts para manejo del Gestor -->
<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>

</html>