 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="func.unes.gde.*" %>

<%
    //Codigo del registro actual
    String codigo_solicitud = request.getParameter("codigo_solicitud");
    //Array que almacenara la respuesta del WS       
	ArrayList arrayrespuesta = new ArrayList();	
    String respuesta[] = new String[3];	int codigoRespuesta = 0;	int descripcionError = 1;	int registros = 2;
	
	// Leer tabla  ds_cd_periodicidad_direcciones
	try {  
		// Llamada al WS Consultar Tabla
		respuesta = WServicios.consultartabla_cadena("direccion_excepcionada","une_st","codigo_solicitud",codigo_solicitud,"direccion_texto_libre|contacto|telefonos|comentario|estado_excepcion_type|estado_gestion_type");
		
	} catch (Exception e) {
		 System.err.println(e.toString());
	}
	respuesta[registros] = respuesta[registros].replace("[", "");
	respuesta[registros] = respuesta[registros].replace("]", "");
   	String[]  values = respuesta[registros].split("\\;", -1);
%>
 
 

 <form action="" method="post" name="form_editar" id="form_editar">
 <table width="100%" border="0">
  <tr>
    <td width="264">
        Direccion Natural</br>
        <input type="hidden" name="codigo_solicitud" value="<%= codigo_solicitud %>">
        <input name="direccion_texto_libre" type="text" id="direccion_texto_libre" size="35" value="<%= values[0] %>">
      </td>
    <td width="245">
        Estado Gestión</br>
        <select name="estado_gestion_type">
          <option value="RESUELTA PROVEEDOR" <%  if ( values[5].equals("RESUELTA PROVEEDOR") ) { out.println(" selected ");}%>>RESUELTA PROVEEDOR</option>
          <option value="PENDIENTE"          <%  if ( values[5].equals("PENDIENTE")          ) { out.println(" selected ");}%>>PENDIENTE</option>
        </select>
    </td>
  </tr>
  <tr>
    <td>
        Contacto </br>
        <input name="contacto" type="text" id="contacto" size="35" value="<%= values[1] %>">
    </td>
    <td>
        Estado Excepcion</br>
        <select name="estado_excepcion_type">
          <option value="INEXISTENTE" <%  if ( values[4].equals("INEXISTENTE") ) { out.println(" selected ");}%>>INEXISTENTE</option>
          <option value="INCOMPLETA"  <%  if ( values[4].equals("INCOMPLETA")  ) { out.println(" selected ");}%>>INCOMPLETA</option>
          <option value="REPORTADA"   <%  if ( values[4].equals("REPORTADA")   ) { out.println(" selected ");}%>>REPORTADA</option>
        </select>
    </td>
  </tr>
  <tr>
    <td>
        Telefono Contacto</br>
        <input name="telefonos" type="text" id="telefonos" size="35" value="<%= values[2] %>">
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
        Comentario</br>
        <textarea name="comentario" id="comentario" cols="35" rows="4"><%= values[3] %></textarea>
      </td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>