<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="org.jboss.axis.AxisFault" %>
<%@ page import="co.net.une.www.svc.WSConsultarTablasGDEStub" %>
<%@ page import="co.net.une.www.svc.WSInsertarTablasGDEStub" %>
<%@ page import="co.net.une.www.svc.WSActualizarTablasGDEStub" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>

<%@page import="java.io.File"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.FileReader"%>
<%@page import="java.io.BufferedReader"%>

<%@ page import="func.unes.gde.*" %>

<%
String session_usuario = (String)session.getAttribute("session_usuario");
boolean procesado = Rurales.Procesar(session_usuario, session.getServletContext().getRealPath("/archivos_importados/"));
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Procesar</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
    <link href="../Estilo/jquery-ui.css" rel="stylesheet">
   
</head>
<body>

<div id="Lista" style="width: 1200px; height: 500px;">

<% if (procesado) { %>
	<h1>PROCESAR</h1>
	<p>&nbsp;</p>
	<h2>El proceso fue generado de forma exitosa.</h2>
	<p>&nbsp;</p>
	<a href="GDirExec2.jsp">Volver</a> 
<% } else {    %>
	<h1>PROCESAR</h1>
	<p>&nbsp;</p>
	<h2>El proceso de direcciones rurales falló.</h2>
	<p>&nbsp;</p>
	<a href="GDirExec2.jsp">Volver</a> 
<% } %>

</div>

<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>

</body>
</html>