<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<%@ page session="true"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory,javax.xml.parsers.DocumentBuilder,org.w3c.dom.*,javax.servlet.*" %>

<%@ page import="java.rmi.*" %>
<%@ page import="java.rmi.RemoteException" %>
<%@ page import="org.jboss.axis.AxisFault" %>
<%@ page import="co.net.une.www.svc.WSConsultarTablasGDEStub" %>
<%@ page import="co.net.une.www.svc.WSInsertarTablasGDEStub" %>
<%@ page import="co.net.une.www.svc.WSActualizarTablasGDEStub" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.util.Date" %>
<%@ page import="func.unes.gde.*" %>
<%

/// Parametros de Consulta
String nombreTabla           = "direccion_excepcionada";
String nombreDataset         = "une_st";
String nombresCamposConsulta = "estado_excepcion_type|estado_gestion_type";
String valoresCamposConsulta = "RURAL|RESUELTA PROVEEDOR";
String nombreCamposResultado = "codigo_solicitud|departamento|municipio|direccion_texto_libre|contacto|telefonos|detalle_direccion|estado_geor_type";
//Array que almacenara la respuesta del WS       
ArrayList arrayrespuesta = new ArrayList();
String[] respuesta   = new String[4];
int codigoRespuesta  = 0;
int descripcionError = 1;
int registros        = 2;
int totalResultados  = 3;

Date fecha_hora = new Date();
SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

try {
	//Llamado a la WS WServiciosURN.consultartabla_URN 
	respuesta        = WServiciosURN.consultartabla_URN(0,0,nombreTabla,nombreDataset,nombresCamposConsulta,valoresCamposConsulta, nombreCamposResultado);
	String[] parts =  respuesta[registros].split("\\|", -1);
	String resp = "";
	String sep = "";
	for (int p=0; p<(parts.length); p++ ) {
		String[] p1 =parts[p].split("\\;", -1);
		if ("E".equals(p1[7])||" ".equals(p1[6])){
			resp += sep+ parts[p];
			sep = "|";
		}
	}
	
	if ("".equals(resp))
		resp="0";
	
	respuesta[registros]=resp;
	
	arrayrespuesta   = WServicios.lista_registros( respuesta[registros] );
	
} catch (Exception e) {
	 System.err.println(e.toString());
}			
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Exportar</title>
  <!-- Hoja de estilos del Gestor -->
	<link href="../Estilo/FormaReportes.css" rel="stylesheet" type="text/css" />
    <link href="../Estilo/jquery-ui.css" rel="stylesheet">

</head>
<body>

<div id="Lista" style="width: 1200px; height: 500px;">

<% if (respuesta[codigoRespuesta].equals("KO")) { %>
	<h1>EXPORTAR</h1>
	<p>&nbsp;</p>
   	<h2>Error: <%= respuesta[descripcionError] %></h2>
<% } else { %>
	<% if (respuesta[registros].equals("0")) { %>
				<h1>EXPORTAR</h1>
				<p>&nbsp;</p>
				<h2>No se encontraron registros</h2>
	<% } else { 
		
               String nombreArchivo= "Exportado_"+(String)session.getAttribute("session_usuario")+"_"+format.format(fecha_hora)+".csv"; // Aqui se le asigna el nombre y
               FileWriter fw = null; // la extension al archivo
               try {
            	   fw = new FileWriter(getServletContext().getRealPath(nombreArchivo),true);
	               //FileWriter fw = new FileWriter("./" + outfile,true);
	               BufferedWriter bw = new BufferedWriter(fw);
	               PrintWriter salArch = new PrintWriter(bw);
	        
	               salArch.print("Codigo_Solicitud[|]Departamento[|]Municipio[|]Direccion_Natural[|]Nombre_Contacto[|]Telefono_Contacto[|]Latitud[|]Longitud");
	               salArch.println();
	               for (int j=0;j<arrayrespuesta.size();j++){ 
			        	   String rows = arrayrespuesta.get(j).toString();
			               String[]  values = rows.split("\\;", -1);
			               
			               salArch.print(values[0]+"[|]"+values[1]+"[|]"+values[2]+"[|]"+values[3]+"[|]"+values[4]+"[|]"+values[5]);
			               salArch.println();
			       }
	               salArch.close();
	               
	               %>
	                <h1>EXPORTAR</h1>
					<p>&nbsp;</p>
					<h2>El proceso exportar fue generado de forma exitosa</h2>
					<p>&nbsp;</p>
					<p>Hacer click en el siguiente link para descargar el archivo generado ( <a href="../<%= nombreArchivo %>"><%= nombreArchivo %></a> )</p>
					<p>&nbsp;</p>
					<a href="GDirExec2.jsp">Volver</a> 
	               <%
               }
               catch (IOException ex) { %>
               		<h1>EXPORTAR</h1>
					<p>&nbsp;</p>
            	    <h2>Error: <%= ex.getMessage() %></h2>
            	    <p>&nbsp;</p>
					<a href="GDirExec2.jsp">Volver</a> 
            <% } 
	}

} %> 
</div>


<script src="../Script/jquery.js"></script>
<script src="../Script/jquery-ui.js"></script>

</body>
</html>