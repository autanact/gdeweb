<%@page language="java" contentType="text/html; charset=UTF-8"%>  
<%request.getSession().invalidate();%>  
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="ms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>		
		<ms:setBundle basename="properties.mensajes" />

		<meta http-equiv="pragma" content="no-cache"/>
		<meta http-equiv="cache-control" content="no-cache"/>
		<meta http-equiv="expires" content="0"/>
  	<!--  	<meta http-equiv="Refresh" content= "1; URL=./control.jsp?ctrl=Inicio"/>  -->
  	
		<meta http-equiv="Refresh" content= "0; URL=./inicio.jsp?ctrl=Inicio"/> 

		<title>
			<ms:message key="Titulo_Sistema" /> 
		</title>		    
	</head>
<body>
<h1 style="text-align: center"><ms:message key="Bienvenida" /> </h1>
<h3 style="text-align: center"><ms:message key="Espere" />
	<img alt='<ms:message key="Espere" />' src="./Imagen/Esperar.gif" height=24 width=24>
</h3>
</body>
</html>